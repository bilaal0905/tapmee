

import UIKit
import Appz
import FirebaseStorage
import CropViewController

class InsertLinkVC: BaseClass {
    
    @IBOutlet weak var outerView: UIView! {
        didSet {
            if indexPath != 2 {
                addShadow(view: outerView)
                outerView.layer.cornerRadius = outerView.layer.frame.height/2
            }
        }
    }
    @IBOutlet weak var mainVu: UIView!
    @IBOutlet weak var topLbl: UILabel! {
        didSet {
            addShadow(view: topLbl)
        }
    }
    
    @IBOutlet weak var editLinkNameField: UITextField!
    @IBOutlet weak var editLinkNameBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var imgVu: UIImageView!
    @IBOutlet weak var editLinkLogoImgVu: UIImageView!
    
    @IBOutlet weak var openBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var isViewOnly = false
    var selectedLink:Links?
    var platform = ""
    var text = ""
    var delegate: LinkAdded?
    var deleteDelegate : LinkDeleted?
    let storage = Storage.storage()
    var img = ""
    var indexPath = 0
    var customLinkImageUrl = ""
    var imagePicked = false
    
    //MARK: View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addShadow(view: userNameField)
        addShadow(view: openBtn)
        addShadow(view: deleteBtn)
        addShadow(view: saveBtn)
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initialViewSetup()

    }
    //MARK: View setup
    private func initialViewSetup() {
        userNameField.text = text
        if selectedLink?.linkID == 4 {
            editLinkNameBtn.isHidden = false
            editLinkNameField.isHidden = false
            topLbl.isHidden = true
            editLinkNameField.text = platform.capitalized
        }
        else {
            topLbl.text = platform.capitalized
        }
        imageViewSetup()

        switch platform.lowercased() {
        case "whatsapp":
            userNameField.placeholder = "Phone Number"
        case "linkedin":
            userNameField.placeholder = "LinkedIn Profile Link"
        case "twitter","twitch","telegram","instagram":
            userNameField.placeholder = "Username"
        case "youtube":
            userNameField.placeholder = "Youtube profile link"
        case "facebook":
            userNameField.placeholder = "Facebook profile link"
            break
        case "pinterest":
            userNameField.placeholder = "Pintrest profile link"
            break
        case "vimeo":
            userNameField.placeholder = "link"
        case "reddit","snapchat","tiktok":
            userNameField.placeholder = "Username"
        case "email":
            userNameField.placeholder = "Email"
        case "spotify":
            userNameField.placeholder = "Album or Artist"
        case "paypal":
            userNameField.placeholder = "PayPal.me link"
        case "custom link","website":
            userNameField.placeholder = "https://www.example.com/"
        case "contact card","text":
            userNameField.placeholder = "Phone Number"
        case "paylah":
            userNameField.placeholder = "Paylah Profile Url"
        case "calendly":
            userNameField.placeholder = "Calendly Url"
            
        default:
            userNameField.placeholder = platform
        }
        
        if isViewOnly {
            userNameField.isUserInteractionEnabled = false
            deleteBtn.isHidden = true
            saveBtn.setTitle("Close", for: .normal)
        }
    }
    
    //MARK: Custom logoImage view Setup
    private func imageViewSetup() {
        if selectedLink?.linkID == 4 {
            customLinkImageUrl = selectedLink?.image ?? ""
            editLinkLogoImgVu.isHidden = false
            if selectedLink?.value != "" && selectedLink?.value != nil, let img = UIImage(contentsOfFile: self.customLogoImgUrl.path) {
                self.imgVu.contentMode = .scaleToFill
                self.imgVu.image = img
            }
            else {
                self.imgVu.loadGif(name: "1-Custom-Link")
            }
            //addShadow(view: self.imgVu)
            imgVu.layer.cornerRadius = imgVu.layer.frame.height/2
            
            let addLogoGesture = UITapGestureRecognizer(target: self, action: #selector(self.imgVuTapped))
            imgVu.isUserInteractionEnabled = true
            addLogoGesture.numberOfTapsRequired = 1
            addLogoGesture.delaysTouchesBegan = true
            imgVu.addGestureRecognizer(addLogoGesture)
        }
        else {
            self.imgVu.image = UIImage(named: platform.lowercased())
        }
    }
    @objc func imgVuTapped (sender: UITapGestureRecognizer){
        
        resignFirstResponder()
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
//            let imagePicker1 = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.dismiss(animated: true) { [self] in
                //self.openImageCropper(image: image)
                self.imgVu.contentMode = .scaleToFill
                self.imgVu.image = image
                self.imagePicked = true
                var storageRef = self.storage.reference()
                storageRef = self.storage.reference().child("customLogoImg:\(self.getUserId()).png")
                self.customLinkImageUrl = "\(storageRef)"
                if let uploadData = image.jpegData(compressionQuality: 0.5) {
                    storageRef.putData(uploadData, metadata: nil) { [self] (metadata, error) in
                        if error != nil {
                            //self.stopLoading()
                            self.showAlert(title: AlertConstants.Error, message: error?.localizedDescription ?? AlertConstants.SomeThingWrong)
                            print("error")
                        }
                        else {
                            print("customLogoImage Updated successfully")
                        }
                    }
                }
            }
        }
    }
    
    override func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.dismiss(animated: true)
    }

    
    
    override func viewWillLayoutSubviews() {
        mainVu.roundCorners(corners: [.topLeft,.topRight], radius: 30.0)
    }
    
    //MARK: Alerts work
    func showAlertMessage(_ message: String) {
            self.showAlert(title: AlertConstants.Alert, message: message)
    }
    
    func showAlert() {
        
        switch platform.lowercased() {
        
        case "whatsapp":
            showAlertMessage(AlertConstants.whatsappMessage)
            break
        case "linkedin":
            showAlertMessage(AlertConstants.linkedInAlertMessage)
            break
        case "twitter":
            showAlertMessage(AlertConstants.twitterMessage)
            break
        case "twitch":
            showAlertMessage(AlertConstants.twitchMessage)
            break
        case "telegram":
            showAlertMessage(AlertConstants.telegramMessage)
            break
        case "instagram":
            showAlertMessage(AlertConstants.instaGramMessage)
        case "youtube":
            showAlertMessage(AlertConstants.youtubeMessage)
            break
        case "facebook":
            showAlertMessage(AlertConstants.fbAlertMessage)
            break
        case "pinterest":
            showAlertMessage(AlertConstants.pintrestMessage)
            break
        case "vimeo":
            showAlertMessage(AlertConstants.vimeoMessage)
            break
        case "reddit":
             showAlertMessage(AlertConstants.redditMessage)
        case "snapchat":
            showAlertMessage(AlertConstants.snapchatMessage)
        case "tiktok":
            showAlertMessage(AlertConstants.tiktokMessage)
            break
        case "paypal":
            showAlertMessage(AlertConstants.paypalMessage)
            break
        case "paylah":
            showAlertMessage(AlertConstants.paylahMessage)
        case "calendly":
            showAlertMessage(AlertConstants.calendlyMessage)
        case "email":
            showAlertMessage(AlertConstants.email)
            break
        case "spotify":
            showAlertMessage(AlertConstants.spotify)
            break
        case "custom link","website":
            showAlertMessage(AlertConstants.customLink)
            break
        case "contact card","text":
            showAlertMessage(AlertConstants.text)
            break
        default:
            break
        }
    }
    
    
    //MARK: Button Actions
    @IBAction func editLinkNameBtn(_ sender: UIButton) {
        editLinkNameField.becomeFirstResponder()
    }
    
    @IBAction func helpButtonPressed(_ sender: UIButton) {
        showAlert()
    }
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        if sender.title(for: .normal) == "Save"  {
            let t = userNameField.text

            guard let text = t?.removeWhitespace() ,!text.isEmpty else {
                showAlert(title: "Alert", message: "This field can not be empty")
                return
            }
            if selectedLink?.linkID == 4 {
                platform = editLinkNameField.text ?? "Custom Link"
                if imagePicked {
                    self.saveLogoImageInDocumentDirectory(image: imgVu.image)
                }
            }
            delegate?.linkReceived(text: text,platform: platform,imageUrl: customLinkImageUrl)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func openBtnPressed(_ sender: Any) {
        
        guard let n = userNameField.text, !n.isEmpty else {
            showAlert(title: AlertConstants.Alert, message: "This value can not be empty.")
            return
        }
        
        let app = UIApplication.shared
        switch platform.lowercased() {
        
        case "twitch":
            let twitchURL = NSURL(string: n)
            if (UIApplication.shared.canOpenURL(twitchURL! as URL)) {
                // The Twitch app is installed, do whatever logic you need, and call -openURL:
                UIApplication.shared.open(URL(string: "\(n)")!)
            } else {
                // The Twitch app is not installed. Prompt the user to install it!
            }
        case "whatsapp":
            app.open(Applications.WhatsApp(), action: .send(abid: n, text: ""))
        case "instagram":
            app.open(Applications.Instagram(),action: .username(n))
        case "facebook":
            app.open(Applications.Facebook(),action: .profile)
        case "youtube":
            app.open(Applications.Youtube(),action: .open)
        case "twitter":
            app.open(Applications.Twitter(),action: .userHandle(n))
        
        case "linkedin":
            let webURL = URL(string: "\(n)")!
            
            UIApplication.shared.open(webURL, options: [:], completionHandler: nil)

//            let appURL = URL(string: "linkedin://profile/\(n)")!
//
//
//            if UIApplication.shared.canOpenURL(appURL) {
//                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
//            }
//
        case "snapchat":
            let username = n
            let appURL = URL(string: "snapchat://add/\(username)")!
            let application = UIApplication.shared
            
            if application.canOpenURL(appURL) {
                application.open(appURL)
                
            } else {
                // if Snapchat app is not installed, open URL inside Safari
                let webURL = URL(string: "https://www.snapchat.com/add/\(username)")!
                application.open(webURL)
            }
        case "vimeo":
            app.open(Applications.Vimeo(),action: .open)
        case "tiktok":
            let installed = UIApplication.shared.canOpenURL(URL(string: "snssdk1233://user/profile/@\(n)")! as URL)
            if installed {
                UIApplication.shared.open(URL(string: "snssdk1233://user/profile/@\(n)")!)
            } else {
                UIApplication.shared.open(URL(string: "www.tiktok.com/\(n)")! as URL)
            }
            break
        case "telegram":
            if let url = URL(string: "tg://resolve?domain=\(n)") {
                UIApplication.shared.open(url)
            }
        case "reddit":
            
            let installed = UIApplication.shared.canOpenURL(URL(string: "reddit:")! as URL)
            if installed {
                UIApplication.shared.open(URL(string: "reddit://r/\(n)")!)
            } else {
                UIApplication.shared.open(URL(string: "https://apps.apple.com/us/app/reddit/id1064216828")! as URL)
            }
            
        case "pinterest":
            app.open(Applications.Pinterest(),action: .user(name: n))
        case "email":
            self.sendEmail(n)
//            app.open(Applications.Mail(),action: .compose(email: .init(recipient: n, subject: "", body: "")))
//
        case "spotify":
            let installed = UIApplication.shared.canOpenURL(URL(string: "spotify:")! as URL)
            if installed {
                UIApplication.shared.open(URL(string: "\(n)")!)
            } else {
                UIApplication.shared.open(URL(string: "https://apps.apple.com/app/spotify-music/id324684580?mt=8")! as URL)
            }
        case "paypal":
            if let url = NSURL(string:n)?.absoluteURL,
               UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.open(URL(string: "https://apps.apple.com/us/app/paypal-mobile-cash/id283646709")! as URL)
            }
            
        case "apple pay":
            app.open(Applications.AppStore(),action: .app(id: "1160481993"))
            UIApplication.shared.open(URL(string: "shoebox://")! as URL)
        case "custom link":
            let validUrlString = n.hasPrefix("https") ? n : n.hasPrefix("http") ? n : "https://\(n)"
            guard let url = URL(string: validUrlString) else {
                showAlert(title: AlertConstants.Alert, message: "Invalid Url")
                return
            }
            UIApplication.shared.open(url)
        case "website":
            let validUrlString = n.hasPrefix("https") ? n : "https://\(n)"
            guard let url = URL(string: validUrlString) else {
                showAlert(title: AlertConstants.Alert, message: "Invalid Url")
                return
            }
            UIApplication.shared.open(url)
        case "contact card":
            if let url = URL(string: "tel://\(n)") {
                UIApplication.shared.open(url)
            }
        case "apple":
            app.open(Applications.Music(),action: .open)
        case "text":
            app.open(Applications.Messages(),action: .sms(phone: n))
        case "calendly":
            let validUrlString = n.hasPrefix("https") ? n : "https://\(n)"
            guard let url = URL(string: validUrlString) else {
                showAlert(title: AlertConstants.Alert, message: "Invalid Url")
                return
            }
            UIApplication.shared.open(url)
        default:
            if let url = URL(string: "https://www.google.com/search?q=\(n)") {
                UIApplication.shared.open(url)
            }
        }
    }
    
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        
        showTwoBtnAlert(title: AlertConstants.Alert, message: "Are you sure to delete link.", yesBtn: "Delete", noBtn: "Cancel") { [self] (delete) in
            if delete {
                deleteLogoImageFromDocumentDirectory()
                deleteDelegate?.deleteLink(platform: self.platform)
                self.dismiss(animated: true, completion: nil)
            }
            
        }
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func addShadow(view: UIView) {
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 3.0)
        view.layer.shadowRadius = 2.0
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 8
    }
    
}
