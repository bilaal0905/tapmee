

import UIKit

class CompleteProfileVC: BaseClass {
   

    @IBOutlet weak var tblVu: UITableView!
    
    let animationNames = ["socialA","profileA","handsA"]
    let titles = ["Add atleast one link","Add profile picture","Add atleast one link",]
    override func viewDidLoad() {
        super.viewDidLoad()

        tblVu.delegate = self
        tblVu.dataSource = self
        // Do any additional setup after loading the view.
    }
}
extension CompleteProfileVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return animationNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompleteProfileCell", for: indexPath) as! CompleteProfileCell
//        cell.animatedView.animation = .named(animationNames[indexPath.row])
//        cell.animatedView.play()
        cell.titleLbl.text = titles[indexPath.row]
        return cell
    }
    
}
