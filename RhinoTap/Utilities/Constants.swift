
import Foundation
import Firebase
import FirebaseDatabase
import UIKit

var userLinks : [Links]? = []
let imgChache = NSCache<AnyObject, AnyObject>()
var userAllLinks : [Links]? = []
var baseUrl = "https://user.tapmee.it/"
var openProfile = false
var profileId = ""

struct Constants {
    
    static let firebaseServerKey = "AAAAxiO_m88:APA91bHf3Ydj4VA2PZCPh2A-J7f007vVz5qYqKdNqmlNt2gPhuCL95i-0tuOLKunXARXuoGmYlOsetZeWjB5wh21FV_5OCK_02BB3eY2eCkbDsi34dhaJVxn3pF8JaXh5Pixpd946cBd"
    static let status = "userStatus"
    static let googleClientID = "414061742670-aetiepbvejum450jmgog7slndee70bjh.apps.googleusercontent.com"
    static let profileUpdateNotif = Notification.Name("profileUpdate")

    static let customer = "userData"
    static let customFontRegular = UIFont(name: "Montserrat-Regular", size: 13.0)
    static let customFontBold = UIFont(name: "Montserrat-Bold", size: 14.0)
    struct refs {
        static let databaseRoot = Database.database().reference()
        static let databaseUser = databaseRoot.child("User")
        static let allLinks = databaseRoot.child("SocialLink")
        static let databaseContacts = databaseRoot.child("Contact")
        static let databaseBaseUrl = databaseRoot.child("BaseUrl")
        static let databaseTag = databaseRoot.child("Tag")
    }
    
}

internal struct AlertConstants {
    static let scanAlert = "Scan TapMee to keep connected".localize()
    static let accountLockedAlert = "Your Account is locked, contact to admin".localize()
    static let userCancelLoginAlert = "User cancelled the login-Flow".localize()
    static let profileAlreadyUpdatedAlert = "Profile is already updated.".localize()
    static let emptyFieldAlert = "This field can not be empty".localize()
    static let removeAlert = "Do you want to remove?".localize()
    static let deleteAlert = "Do you want to delete?".localize()
    static let Error = "Error!".localize()
    static let Alert = "Alert".localize()
    static let DeviceType = "ios".localize()
    static let Ok = "Ok".localize()
    static let EmailNotValid = "Email is not valid.".localize()
    static let PhoneNotValid = "Phone number is not valid.".localize()
    static let EmailEmpty = "Email is empty.".localize()
    static let PhoneEmpty = "Phone number is empty".localize()
    static let FirstNameEmpty = "First name is empty".localize()
    static let LastNameEmpty = "Last name is empty".localize()
    static let NameEmpty = "Name is empty".localize()
    static let Empty = " is empty".localize()
    static let PasswordsMisMatch = "Make sure your passwords match".localize()
    static let LoginSuccess = "Login successful".localize()
    static let SignUpSuccess = "Signup successful".localize()
    static let emailPasswordInvalid = "Email or password is not valid".localize()
    static let PasswordEmpty = "Password is empty".localize()
    static let shortPassword = "Password must be atleast 6 digits".localize()
    static let Success = "Success".localize()
    static let InternetNotReachable = "Your phone does not appear to be connected to the internet. Please connect and try again".localize()
    static let UserNameEmpty = "Username is empty".localize()
    static let TermsAndCondition = "Terms and conditions have not been accepted".localize()
    static let AllFieldNotFilled = "Make sure all fields are filled".localize()
    static let fieldCanBeEmpty = "This field can not be empty".localize()

    static let SomeThingWrong = "Some thing went wrong".localize()
    static let SelectFromDropDown = "Please select value from Dropdown".localize()
    
    /*
     alerts for hints
     */
    static let whatsappMessage = "Add your phone number including your country code (e.g. +6581234567)".localize()
    
    static let fbAlertMessage = """
    1. Open up your Facebook app and log into your Facebook account.\n
    2. From the home page, click on the menu icon at the bottom right corner (It looks like three horizontal lines.)\n
    3. Click on your profile picture to go to your profile page.\n
    4. Click on the Profile settings tab (three dots).\n
    5. Click on Copy Link to copy your full Facebook profile link.\n
    6. Paste the link into the Facebook URL field. (e.g. www.facebook.com/your_fb_id).\n
    """
    
    static let linkedInAlertMessage = """
    Linkedin:-\n
    1. Open your LinkedIn app and log into your account.\n
    2. Go to your profile page.\n
    3. Click on the three dots beside "add section".\n
    4. Select Share via...\n
    5. Select Copy.\n
    6. Paste the copied link into the LinkedIn URL field.
    """
    
    static let instaGramMessage = """
    1. Open up your Instagram app and log into your account.\n
    2. Click on your profile picture at the bottom right corner.\n
    3. Your username will be shown at the very top of your profile (above your profile picture).\n
    4. Paste your username into the Instagram URL field.\n
    """
    
    static let twitterMessage = """
    1. Open your Twitter app and log into your account.\n
    2. Click on your profile picture located at the top left corner.\n
    3. Your username will be shown under your profile picture.\n
    4. Copy and paste your username into the Twitter URL field.\n
    """
    
    static let twitchMessage = """
    If you are using the twitch app on a mobile phone:\n
    1: Go to twitch app and log into your account.
    2: Click your profile picture in the top left corner, then under profile picture.
    3: \nYour username will be shown under the profile picture.
    """
    
    static let telegramMessage = """
        1. Open your Telegram app and log into your account.\n
        2. Click on settings located at the bottom right corner.\n
        3. Your username will be shown under your contact number.\n
        4. Copy and paste your username into the Telegram URL field.\n
    """

    static let youtubeMessage = """
        1. Sign in to your YouTube Studio account.\n
        2. From the Menu, select Customisation Basic Info.\n
        3. Click into the Channel URL and copy the link.\n
        4. Paste the copied link into the YouTube URL field.\n
    """
    
    static let pintrestMessage = """
    1. Open your Pinterest app and log into your account.\n
    2. Click onto your profile picture located at the bottom right corner.\n
    3. Click into the three dots menu located at the top right corner.\n
    4. Select copy profile link.\n
    5. Paste the copied link into the Pinterest URL field.\n
    """
    
    static let snapchatMessage = """
    1. Open your Snapchat app and log into your account.\n
    2. Tap into your profile icon at the top left corner of the screen.\n
    3. Your username is shown next to your Snapchat score.\n
    4. Copy and paste the username into the Snapchat URL field.\n
    """
    
    static let tiktokMessage = """
        1. Open your TikTok app and log into your account.\n
        2. Click on profile located at the bottom right corner.\n
        3. Your username will be shown under your profile picture.\n
        4. Copy and paste your username into the TikTok URL field.\n
        """
    
    static let paypalMessage = """
         1. Go to www.paypal.com and log in.\n
         2. Under your profile, select Get Paypal.me\n
         3. Create a Paypal.me profile.\n
         4. Copy your created Paypal.me link and paste it into the Paypal URL field.\n
        """
    
    static let vimeoMessage = "1. Visit www.vimeo.com and log into your account.\n\n2. Click into settings.\n\n3. Go to your profile page and copy the Vimeo URL located at the bottom.\n\n4. Paste the copied link into the Vimeo URL field."
    
    static let redditMessage = "1. Open your Reddit app and log into your account.\n2. Click on the top left corner profile icon.\n3. Copy the username under the profile avatar (e.g. u/username).\n4. Paste username into the Reddit URL field.\n"
    
    static let paylahMessage = "1. Open up your Paylah! app and log into your account.\n2. Go to My QR.\n3. Click on Share via.\n4. Select copy and paste the copied link into your phone notes.\n5. Select the Paylah link portion, copy and paste into the Paylah! URL field."
    
    static let calendlyMessage = "1. Open up your Calendly app and log into your account.\n\n2. Copy the URL link below your name.\n\n3. Paste the copied link into the Calendly URL field."
    
    static let customLink = "Input a website link/ URL of your choice."
    
    static let contact = "Add your phone number including your country code (eg: +6581234567)"

    static let email = "Input your email address."
    
    static let text = "Add your phone number including your country code (eg: +6581234567)"
    
    static let spotify = "1. Search for your favourite Artist or Albums.\n\n2. Click on the three dots menu selection and select share.\n\n3. Select Copy Link.\n\n4. Paste the copied link into the Spotify URL Field."
    
}

enum connectBtnTitle : String {
    case Connected = "Connected"
    case Invited = "Invited"
    case Accept = "Accept"
    case Connect = "Connect"
}

internal struct APIUrl {
    
    
}
struct FirebaseConst {
    struct refs {
        static let databaseRoot = Database.database().reference()
        static let databaseChats = databaseRoot.child("chatThreads")
        static let databaseUser = databaseRoot.child("UsersList")
    }
}

enum Storyboards {
    case Main
    var id: String {
        return String(describing: self)
    }
}
