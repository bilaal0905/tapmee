
import UIKit
import LGSideMenuController
import FirebaseAuth
import FirebaseStorage
import Localize

class SideMenuController: BaseClass {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var tblVu: UITableView!
    @IBOutlet weak var imgVu: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var segment: UISegmentedControl!
    
    var nameArray = ["Buy TapMee".localize(),
                     "Edit the profile".localize(),
                     "Activate TapMee".localize(),
                     "TapMee Guide".localize(),
                     "Delete Account".localize(),
                     "Log Out".localize()]
    
    var imagesArray = ["buy",
                       "editProfile",
                       "qr",
                       "help",
                       "deleteAccount",
                       "logouticon",
                       "ambassador",
                       "device",
                       "profile"]
    
    var selectedColorHex = "#CFCFCF"
    var selectedIndex = 0
    let storage = Storage.storage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblVu.delegate = self
        tblVu.dataSource = self
        setUpLayout()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Constants.profileUpdateNotif, object: nil)
        
        let normalAttribute: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Montserrat-Regular", size: 12.0)!, .foregroundColor: UIColor.gray]
        segment.setTitleTextAttributes(normalAttribute, for: .normal)
        
        let selectedAttribute: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Montserrat-Bold", size: 13.0)!, .foregroundColor: UIColor.black]
        segment.setTitleTextAttributes(selectedAttribute, for: .selected)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .clear
        self.view.addBackground()
    }
    func setUpLayout() {
        
        let u = getProfileUrl()
        if u != "" {
            let ref = storage.reference(forURL: u)
            
            if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                imgVu.image = cacheImg
            } else {
                ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                        // Uh-oh, an error occurred!
                    } else {
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                            self.imgVu.image = img
                        }
                    }
                }
            }
        } else {
            imgVu.image = UIImage(named: "user_black")
        }
        
        imgVu.layer.borderWidth = 1
        imgVu.layer.masksToBounds = false
        imgVu.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        imgVu.layer.cornerRadius = imgVu.frame.height/2
        imgVu.clipsToBounds = true
        
//        outerView.layer.cornerRadius = 10
//        outerView.layer.borderWidth = 3
//        outerView.layer.borderColor = UIColor.white.cgColor
        let user = readUserData()
        emailLbl.text = user?.name ?? (user?.username)!
        segment.selectedSegmentIndex = user?.profileOn ?? 1
        
    }
    
    @IBAction func segmentChanged(_ sender: Any) {
        var dict = ["profileOn":0,
                    "id":getUserId()] as [String : Any]
        
        switch segment.selectedSegmentIndex
        {
            case 0:
                dict["profileOn"] = 0
                APIManager.updateUserProfile(dict) { success in
                    if success {
                        self.showAlert(title: "Profile status changed", message: "Now your profile will not be visible to others")
                    }
                }
            case 1:
                dict["profileOn"] = 1
                APIManager.updateUserProfile(dict) { success in
                    if success {
                        self.showAlert(title: "Profile status changed", message: "Now your profile will be visible to others")
                    }
                }
            default:
                break;
        }
    }
    
    
    @objc func onNotification(notification:Notification) {
        
        let newName = notification.userInfo?["name"]
        let u = (notification.userInfo?["url"] ?? "") as! String
        //        let email = notification.userInfo?["email"]
        
        if u != "" {
            let ref = storage.reference(forURL: u)
            
            if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                imgVu.image = cacheImg
            } else {
                ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                        // Uh-oh, an error occurred!
                    } else {
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                            self.imgVu.image = img
                        }
                    }
                }
            }
        } else {
            imgVu.image = UIImage(named: "user_black")
        }
        print("Notification Recived:",newName as Any)
        if newName != nil {
            emailLbl.text = newName as? String
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
}

extension SideMenuController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuCell
        cell.nameLbl.text = nameArray[indexPath.row].localize()
        cell.imgVu.image = UIImage(named: imagesArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
            case 0:
            if let url = URL(string: "https://www.tapmee.it/") {
                UIApplication.shared.open(url)
            }
            self.hideLeftView(self)
            case 1:
                NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"EditProfileVC"])
                self.toggleLeftViewAnimated(self)
            case 2:
            NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"TabBarController", "tabbarIndex": "1"])
                self.toggleLeftViewAnimated(self)
            case 3:
            if let url = URL(string: "https://www.tapmee.it/") {
                UIApplication.shared.open(url)
            }
                self.toggleLeftViewAnimated(self)
                break                
            case 4:
                self.showTwoBtnAlert(title: AlertConstants.Alert,
                                     message: "Do you want to delete your account?".localize(),
                                     yesBtn: "Yes".localize(),
                                     noBtn: "No".localize()) { status in
                    
                    if status == true {
                        print("delete")
                        self.deleteUserAccount()
                    }
                }

            case 5:
                self.logout()
            default:
                self.toggleLeftViewAnimated(self)
                print("nothing")
        }
        
    }
    
    @IBAction func logOutBtnTpd(_ sender: Any) {
        
        logout()
    }
    
    func logout() {
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        
        startLoading()
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            stopLoading()
            showAlert(title: AlertConstants.Error, message: signOutError.localizedDescription )
            print("Error signing out: %@", signOutError)
            return
        }
        
        Constants.refs.databaseUser.child(getUserId()).updateChildValues([
            "fcmToken":""
        ])
        
        deleteUserModel()
        stopLoading()
        
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyboard.instantiateViewController(withIdentifier: "SignInNavController") as! SignInNavController
        self.view.window?.rootViewController = rootVC
    }
    
    func deleteUserAccount() {
        
        let user = Auth.auth().currentUser
        user?.delete(completion: { error in
            if let error = error {
                // An error happened.
                print(error.localizedDescription)
              } else {
                // Account deleted.
                  self.deleteUserModel()
                  
                  self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                  self.navigationController?.popToRootViewController(animated: true)
                  
                  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let rootVC = storyboard.instantiateViewController(withIdentifier: "SignInNavController") as! SignInNavController
                  self.view.window?.rootViewController = rootVC
              }
        })
        
        
    }
}

