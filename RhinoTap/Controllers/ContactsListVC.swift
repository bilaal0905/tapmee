

import UIKit
import FirebaseStorage



class tblCell: UITableViewCell {
    

    @IBOutlet weak var imgView: UIImageView! {
        didSet {
            imgView.layer.cornerRadius = imgView.bounds.height/2
        }
    }
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emaillbl: UILabel!
    @IBOutlet weak var phonelbl: UILabel!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var messagelbl: UILabel!

}


class ContactsListVC: BaseClass,UISearchBarDelegate {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblVu: UITableView!
    
    var contacts : [Contacts]? = []
    var filteredContacts : [Contacts]? = []
    var isSearching = false
    let refreshControl = UIRefreshControl()
    lazy var emptyStateMessage: UILabel = {
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Montserrat-Regular", size: 17)
        messageLabel.sizeToFit()
        return messageLabel
    }()
    
    let storage = Storage.storage()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblVu.delegate = self
        tblVu.dataSource = self
        
//        self.tblVu.estimatedRowHeight = 200
        self.tblVu.rowHeight = UITableView.automaticDimension
        
        searchBar.delegate = self
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tblVu.addSubview(refreshControl)
        
        self.tblVu.tableFooterView = UIView()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        getContactsList()
    }
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        getContactsList()
    }
    
    //MARK: Get Contact list
    func getContactsList() {
        
        self.refreshControl.endRefreshing()
        startLoading()

        APIManager.getContactsList(id: getUserId()) { contacts in
            
            if contacts?.count == 0 || contacts == nil {
                self.showEmptyState(message: "No contacts found")
                self.stopLoading()
            } else {
                self.hideEmptyState()
            }
            
            self.contacts = contacts
            DispatchQueue.main.async {
                self.stopLoading()
                self.tblVu.reloadData()
            }
        }
    }
        
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        isSearching = false
        searchBar.resignFirstResponder()
        self.tblVu.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        isSearching = true
    }
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       print(searchText)
        
        let result = contacts?.filter{ $0.name?.range(of: searchText, options: .caseInsensitive) != nil }
        filteredContacts = result
        if searchText.count == 0 || searchText == "" {
            filteredContacts = contacts
        }

        isSearching = true
        self.tblVu.reloadData()

    }
}

extension ContactsListVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = (isSearching ? filteredContacts?.count : contacts?.count) ?? 0
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tblCell", for: indexPath) as! tblCell
       
        let contact = isSearching ? filteredContacts?[indexPath.row] : contacts?[indexPath.row]
                
        cell.nameLbl.text = contact?.name
        cell.nameLbl.font = UIFont(name: "Montserrat-Bold", size: 20)
        cell.imgView.image = #imageLiteral(resourceName: "profile")
        
        cell.emaillbl.text = "Email: \(contact?.email ?? "")"
        cell.phonelbl.text  = "Phone: \(contact?.phone ?? "")"
        

        cell.jobTitle.text = "Job: \(contact?.job ?? "")"
        cell.company.text = contact?.company ?? "" == "" ? "" :
        "Company: " + (contact?.company!)!
        
        cell.messagelbl.text = contact?.message ?? "" == "" ? "" :
        "Note: " + (contact?.message!)!

        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 200
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = sb.instantiateViewController(withIdentifier: "ViewContactVC") as! ViewContactVC
//
//        if (contacts?[indexPath.row].id != nil) {
//            vc.uid = (contacts?[indexPath.row].id)!
//            vc.nameInContacts = (contacts?[indexPath.row].name ?? "")
//            vc.isRequestedByMe = (contacts?[indexPath.row].isReqByMe ?? false)
//            vc.isRequestedByOther = (contacts?[indexPath.row].isReqByOther ?? false)
//            vc.userDetails = contacts?[indexPath.row]
//            navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    func showEmptyState(message: String) {
        emptyStateMessage.text = message
          tblVu.addSubview(emptyStateMessage)
        emptyStateMessage.centerXAnchor.constraint(equalTo: tblVu.centerXAnchor).isActive = true
        emptyStateMessage.centerYAnchor.constraint(equalTo: tblVu.centerYAnchor).isActive = true
      }

      func hideEmptyState() {
          emptyStateMessage.removeFromSuperview()
      }
}
