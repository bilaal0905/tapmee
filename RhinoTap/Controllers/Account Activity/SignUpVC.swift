
import UIKit
import FirebaseAuth
import Firebase
class SignUpVC: BaseClass {
    
    //@IBOutlet weak var pwLengthlbl: UILabel!
    //@IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField! {
        didSet {
            let blackPlaceholderText = NSAttributedString(string: "Username",
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            userNameTF.attributedPlaceholder = blackPlaceholderText
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: userNameTF.frame.height))
            userNameTF.leftView = paddingView
            userNameTF.leftViewMode = .always
        }
    }
    @IBOutlet weak var emailTF: UITextField! {
        didSet {
            let blackPlaceholderText = NSAttributedString(string: "Email",
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            emailTF.attributedPlaceholder = blackPlaceholderText
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: emailTF.frame.height))
            emailTF.leftView = paddingView
            emailTF.leftViewMode = .always
        }
    }
    @IBOutlet weak var pwTF: UITextField! {
        didSet {
            let blackPlaceholderText = NSAttributedString(string: "Password",
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            pwTF.attributedPlaceholder = blackPlaceholderText
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: pwTF.frame.height))
            pwTF.leftView = paddingView
            pwTF.leftViewMode = .always
        }
    }
    //  @IBOutlet weak var privacyPolicyLbl: UILabel!
    
    var credential : AuthCredential? = nil
    let storage = Storage.storage()
    
    var isSocialLogin = false
    var isFromAppleLogin = false
    
    var uid = ""
    var email = ""
    var name = ""
    var profileImgUrl = ""
    var profileImg = UIImage(named: "")
    var timestamp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.timestamp = format.string(from: date)
        
        //        let regularFont = UIFont(name: "Montserrat-Regular", size: 12.0)
        //        let boldFont = UIFont(name: "Montserrat-SemiBold", size: 12.0)
        //        let attributedString = NSMutableAttributedString(string: "By signing up, you agree to our ".localize(), attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 17)])
        //        let attributedString1 = NSMutableAttributedString(string: " and ".localize(), attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 17)])
        //        let boldString = NSMutableAttributedString(string: "Terms".localize(), attributes: [NSAttributedString.Key.font: boldFont ?? UIFont.systemFont(ofSize: 17)])
        //        let boldString1 = NSMutableAttributedString(string: "Privacy Policy".localize(), attributes: [NSAttributedString.Key.font: boldFont ?? UIFont.systemFont(ofSize: 17)])
        //        let aa:NSAttributedString = attributedString.append(boldString)
        //        privacyPolicyLbl.attributedText = (\(attributedString" + boldString + attributedString1 + boldString1)
        //        privacyPolicyLbl.attributedText = boldenParts(string: "By signing up, you agree to our ".localize() + "Terms".localize() + "and".localize() + "Privacy Policy".localize(), boldCharactersRanges: [[32,36], [42,55]], regularFont: UIFont(name: "Montserrat-Regular", size: 12.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 12.0))
        //
        //        self.privacyPolicyLbl.isUserInteractionEnabled = true
        //        let tapgesture = RangeGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        //        tapgesture.numberOfTapsRequired = 1
        //        self.privacyPolicyLbl.addGestureRecognizer(tapgesture)
        
        
        
        
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
        //setNeedsStatusBarAppearanceUpdate()
    }
    
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        .lightContent
    //    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.view.addBackground()
        
        if isSocialLogin {
            pwTF.isHidden = true
            // pwLengthlbl.isHidden = true
            if profileImgUrl != "" {
                let url = URL(string: profileImgUrl)
                let data = try? Data(contentsOf: url!)
                if data != nil {
                    profileImg = UIImage(data: data!)
                }
            }
        } else {
            pwTF.isHidden = false
            // pwLengthlbl.isHidden = false
        }
        
        // nameTF.placeholder = "Full Name"
        emailTF.text = email
        //nameTF.text = name
    }
    //    //MARK: tappedOnLabel
    //        @objc func tappedOnLabel(_ gesture: RangeGestureRecognizer) {
    //            guard let text = self.privacyPolicyLbl.text else { return }
    //            let privacyPolicyRange = (text as NSString).range(of: "Privacy Policy")
    //            let termsAndConditionRange = (text as NSString).range(of: "Terms")
    //
    //            if gesture.didTapAttributedTextInLabel(label: self.privacyPolicyLbl, inRange: privacyPolicyRange) {
    //                print("user tapped on privacy policy text")
    //            } else if gesture.didTapAttributedTextInLabel(label: self.privacyPolicyLbl, inRange: termsAndConditionRange){
    //                print("user tapped on terms and conditions text")
    //            }
    //        }
    //    //MARK: Bolden Parts
    //    func boldenParts(string: String, boldCharactersRanges: [[Int]], regularFont: UIFont?, boldFont: UIFont?) -> NSAttributedString {
    //        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 17)])
    //        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont ?? UIFont.boldSystemFont(ofSize: regularFont?.pointSize ?? UIFont.systemFontSize)]
    //        for range in boldCharactersRanges {
    //            let currentRange = NSRange(location: range[0], length: range[1]-range[0]+1)
    //            attributedString.addAttributes(boldFontAttribute, range: currentRange)
    //            }
    //        return attributedString
    //    }
    //MARK: Button Actions
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signinBtn(_ sender: UIButton) {
        pushController(controller: SignInVC.id, storyboard: "Main")
        //self.navigationController?.popViewController(animated: false)
        
    }
    
    
    @IBAction func createAccountPressed(_ sender: Any) {
        
        resignFirstResponder()
        //        guard let name = nameTF.text, !name.isEmpty,
        guard let email = emailTF.text, !email.isEmpty,
              let userName = userNameTF.text, !userName.isEmpty else {
                  showAlert(title: AlertConstants.Error, message: AlertConstants.AllFieldNotFilled)
                  return
              }
        
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        
        startLoading()
        
        var param : [String:Any] = [
            "username": userName.lowercased(),
            "name": name,
            "email": email.lowercased()
        ]
        
        if isSocialLogin {
            
            guard uid != "" else {
                stopLoading()
                showAlert(title: AlertConstants.Error, message: "Sign Up failed. Please try again.".localize())
                return
            }
            
            if (isFromAppleLogin == true) {
                // User is signed in
              
                let user = User(id: uid, name: param["name"] as? String,
                                email: param["email"] as? String,
                                fcmToken: getFCM(),
                                profileUrl: "",
                                bio: "",
                                username: param["username"] as? String,
                                phone: "",
                                gender: "",
                                dob: "",
                                address: "",
                                coverUrl:"",
                                profileOn: 1,
                                isDeleted: false,
                                platform: "ios",
                                timestamp: self.timestamp)
                
                param.removeAll()
                param = user.asDictionary
                
                UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey: Constants.customer)
                UserDefaults.standard.synchronize()
                
                Constants.refs.databaseUser.child(uid).setValue(param) {
                    (error:Error?, ref:DatabaseReference) in
                    self.stopLoading()
                    if let error = error {
                        self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                        return
                    } else {
                        print("Data saved successfully!")
                        UserDefaults.standard.setValue(true, forKey: Constants.status)
                        
                        self.performSegue(withIdentifier: "Main", sender: self)
                        
                    }
                }
                
                
                
            } else { // Login with facebook or google
                
                let storageRef = storage.reference().child("profilePic:\(uid).png")
                
                if profileImg != nil {
                    profileImgUrl = "\(storageRef)"
                }
                else {
                    profileImgUrl = ""
            }
                let user = User(id: uid, name: param["name"] as? String, email: param["email"] as? String, fcmToken: getFCM(), profileUrl: profileImgUrl, bio: "Bio", username: param["username"] as? String, phone: "Phone", gender: "Gender", dob: "Date of Birth", address: "Address", coverUrl:"", profileOn: 1, isDeleted: false, platform: "ios",verifyUser: false, tagUid: "", timestamp: self.timestamp)
                
                param.removeAll()
                param = user.asDictionary
                
                UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey: Constants.customer)
                UserDefaults.standard.synchronize()
                
                startLoading()
                Constants.refs.databaseUser.child(uid).setValue(param) {
                    (error:Error?, ref:DatabaseReference) in
                    
                    self.stopLoading()
                    if let error = error {
                        self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                        return
                    } else {
                        print("Data saved successfully!")
                        UserDefaults.standard.setValue(true, forKey: Constants.status)
                        
                        self.performSegue(withIdentifier: "Main", sender: self)
                        
                    }
                }
                
                if let uploadData = profileImg?.jpegData(compressionQuality: 0.5) {
                    storageRef.putData(uploadData, metadata: nil) { [self] (metadata, error) in
                        if error != nil {
                            self.showAlert(title: AlertConstants.Error, message: error?.localizedDescription ?? "Sign Up failed. Please try again.".localize())
                            print("error")
                        } else {
                            
                        }
                    }
                } else {
                    stopLoading()
                }
            }
            
        } else {
            
            guard let password = pwTF.text, !password.isEmpty,
                  password.count > 5 else {
                      self.stopLoading()
                      showAlert(title: AlertConstants.Error, message: AlertConstants.shortPassword)
                      return
                  }
            
            APIManager.signUp(params: param,password: password) { (status,id,err)  in
                self.stopLoading()
                if status == false { // User name is already taken
                    print("Error Occured",err)
                    self.showAlert(title: AlertConstants.Alert, message: err)
                } else {
                    self.showAlert(title: AlertConstants.Success, message: AlertConstants.SignUpSuccess) {
                        UserDefaults.standard.setValue(true, forKey: Constants.status)
                        
                        self.performSegue(withIdentifier: "Main", sender: self)
                        
                    }
                }
            }
        returnedError: { (err) in
            self.stopLoading()
            self.showAlert(title: AlertConstants.Error, message: err)
        }
        }
    }
    
    @IBAction func tappedPrivacyPolicyBtn(_ sender: Any) {
        if let url = URL(string: "https://www.tapmee.it/privacy-policy/") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func tappedTermsUseBtn(_ sender: Any) {
        if let url = URL(string: "https://www.tapmee.it/privacy-policy/") {
            UIApplication.shared.open(url)
        }
    }
    
}

