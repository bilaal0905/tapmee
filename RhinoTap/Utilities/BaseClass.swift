


import Foundation
import UIKit
import CropViewController
import NVActivityIndicatorView
import Network
import MessageUI


class BaseClass: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate,CropViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    var loaderIndicator: NVActivityIndicatorView? = nil
    var imagePicker = UIImagePickerController()
    var customer: User?
    var customLogoImg: UIImage?
    var customLogoImgUrl: URL {
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documents.appendingPathComponent("customLogoImg.png")
    }

   
    override func viewDidLoad() {
        super.viewDidLoad()
        loaderinit()
//        whiteLoaderinit()
    }
    
    @available(iOS 12.0, *)
    func isNetworkAvailable() -> Bool {
        var internet = false
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                if path.usesInterfaceType(.cellular) {
                    print("celullar")
                } else if path.usesInterfaceType(.wifi) {
                    print("It's WiFi!")
                }
                DispatchQueue.main.async {
                    
                    print("There is internet")
                }
                internet = true
            } else {
                DispatchQueue.main.async {
                    self.showAlert(title: AlertConstants.InternetNotReachable,message: "")
                    print("There is no internet")
//                    self.backgroundVu.isHidden = false
//                    self.noNet.isHidden = false
//                    self.retryBtn.isHidden = false
//                    self.activityIndicator.isHidden = true
                }
                internet = false

            }
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        return internet
    }
    
    func saveUserToDefaults(_ value: User) {
        UserDefaults.standard.setValue(true, forKey: Constants.status)
        
        var timestamp = ""
        
        if value.timestamp == ""  {
            let date = Date()
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            timestamp = format.string(from: date)
        }
        
    
        let dict: User = User(id: value.id, name: value.name, email: value.email, fcmToken: value.fcmToken, profileUrl: value.profileUrl, bio: value.bio, username: value.username, phone: value.phone, gender: value.gender, dob: value.dob, address: value.address,coverUrl: value.coverUrl,isDeleted: value.isDeleted, platform: value.platform != "ios" ? "ios" : "ios",
                              verifyUser:value.verifyUser, tagUid: value.tagUid, timestamp: value.timestamp == "" ? timestamp : value.timestamp)
        
        UserDefaults.standard.set(try? PropertyListEncoder().encode(dict), forKey: Constants.customer)
        UserDefaults.standard.synchronize()
    }
    
    
    func readUserData() -> User? {
        
        var customer: User?
        if let data = UserDefaults.standard.value(forKey:Constants.customer) as? Data {
            customer = try! PropertyListDecoder().decode(User.self, from: data)
        }
        
        return customer
    }
    
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func getProfileUrl() -> String {
                
        let user = readUserData()
        return user?.profileUrl ?? ""
    }
    
    func saveFCM (_ fcm: String) {
        UserDefaults.standard.setValue(fcm, forKey: "FCMToken")
        UserDefaults.standard.synchronize()
    }

    func getFCM() -> String {
        return UserDefaults.standard.value(forKey: "FCMToken") as? String ?? ""
    }
    
    func getUsername() -> String {
                
        let user = readUserData()
        return user?.username ?? ""
    }
    
    func getName() -> String {
        let user = readUserData()
        return user?.name ?? ""
    }
    
    func showPermissionAlert(title: String, message: String) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)

    }
    
    func getBio() -> String {
        let user = readUserData()
        return user?.bio ?? "Bio"
    }
    
    func getUserId() -> String {
        let user = readUserData()
        return user?.id ?? ""
    }
    
    func getEmail() -> String {
        let user = readUserData()
        return user?.email ?? ""
    }
    
    func deleteUserModel() {
        let user = User()
        saveUserToDefaults(user)
        UserDefaults.standard.setValue(false, forKey: Constants.status)

        print("user data deleted")
    }

    func loaderinit() {
        loaderIndicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.width/2 - 25, y: self.view.frame.height/2 , width: 50, height: 50), type: .ballScaleRippleMultiple, color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), padding: 1)
        view.addSubview(loaderIndicator!)
    }

    func startLoading() {
        loaderIndicator?.startAnimating()
    }
    
    func stopLoading() {
        loaderIndicator?.stopAnimating()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.dismiss(animated: true) {
                self.openImageCropper(image: image)
            }
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }

    func openImageCropper(image: UIImage) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.toolbar.clampButtonHidden = true
        cropViewController.doneButtonColor = UIColor.white
        cropViewController.cancelButtonColor = UIColor.red
        cropViewController.aspectRatioPreset = .presetSquare
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.resetAspectRatioEnabled = false 
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        print("Image Cropped!")
        self.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .`default`, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func showAlert(title: String, message: String, onSuccess closure: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .`default`, handler: { _ in
            closure()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showTwoBtnAlert (title: String, message: String,yesBtn:String,noBtn:String, onSuccess success: @escaping (Bool) -> Void) {
        
        let dialogMessage = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: yesBtn, style: .destructive, handler: { (action) -> Void in
            
            print("Yes button click...")
            success(true)
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: noBtn, style: .cancel) { (action) -> Void in
            print("Cancel button click...")
            success(false)
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    func pushController(controller toPush: String, storyboard: String) {
        let controller = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: toPush)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func getControllerRef(controller toPush: String, storyboard: String) -> UIViewController {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: toPush)
    }
        
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func sendEmail(_ email: String) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            
            mail.title = ""
            mail.setToRecipients([email])
            
            mail.setSubject("")
            
            mail.setMessageBody("", isHTML: false)
            
            self.present(mail, animated: true, completion: nil)
            
        } else {
            // show failure alert
            showMailServiceErrorAlert()
            return
        }
    }
    
    func showMailServiceErrorAlert(){
        self.showAlert(title: "Mail services are not available",message: "")
    }
       
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {

        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue :
            print("Cancelled")

        case MFMailComposeResult.failed.rawValue :
            print("Failed")

        case MFMailComposeResult.saved.rawValue :
            print("Saved")

        case MFMailComposeResult.sent.rawValue :
            print("Sent")

        default: break
        
        }

        self.dismiss(animated: true, completion: nil)

   }
    
    //MARK: Save logo image in document directory
    func saveLogoImageInDocumentDirectory(image: UIImage?)
    {
        if let data = image?.pngData() {
            do {
                try data.write(to: customLogoImgUrl)
            } catch {
                print("Unable to Write Data to Disk (\(error))")
            }
        }
    }
    
    //MARK: Delete logo Image from document directory
    func deleteLogoImageFromDocumentDirectory() {
        do {
            try FileManager.default.removeItem(atPath: customLogoImgUrl.path)
        }
        catch {
            print("Could not clear temp folder: \(error)")
        }
    }
}
