

import UIKit
import CodableFirebase
import Firebase
import FirebaseStorage
import Localize
class ProfileCell: UICollectionViewCell {
    
    @IBOutlet weak var imgVu: UIImageView!
    @IBOutlet weak var outerVu: UIView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var deleteLinkBtn: UIButton!
    @IBOutlet weak var editLinkBtn: UIButton!
    
}


class DashBoardVC: BaseClass, LinkDeleted, LinkAdded {

    @IBOutlet weak var frontVu: UIView!
    @IBOutlet weak var collectionVu: UICollectionView!
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    
    var isLeadModeOn =  false
    var isDirectModeOn = false
    var isMenuRearrangementDone = false
    
    var linksDict = [String:AnyObject]()
    var index = -1
    var userDetails : User?
    var headerIndex : IndexPath? = nil
    let storage = Storage.storage()
    
    lazy var emptyStateMessage: UILabel = {
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 17)
        messageLabel.sizeToFit()
        return messageLabel
    }()
    var profilePicture = UIImage(named: "user_black")
    var coverPicture = UIImage(named: "user_white")
    
    var directOnIndex : Int = 0
    
    //MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //print("Custom Link".localize())
        collectionVu.delegate = self
        collectionVu.dataSource = self
        
        collectionVu.isUserInteractionEnabled = true
        collectionVu.dragInteractionEnabled = true
        
        userDetails = self.readUserData()
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        collectionVu.addGestureRecognizer(longPressGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.addBackground()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        
        hidelayout()
        getUserLinks()
        //isDirectModeOn = UserDefaults.standard.value(forKey: "direct") as? Bool ?? false
    }
    
    // MARK: Views setup
    func showlayout() {
        self.frontVu.isHidden = true
        collectionVu.isHidden = false
    }
    
    func hidelayout() {
        self.frontVu.isHidden = false
        collectionVu.isHidden = true
    }
    
    //MARK: Gesture Work
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = collectionVu.indexPathForItem(at: gesture.location(in: collectionVu)) else {
                break
            }
            collectionVu.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            collectionVu.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            collectionVu.endInteractiveMovement()
        default:
            collectionVu.cancelInteractiveMovement()
        }
    }
    //MARK:  Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addLink" {
            let vc = segue.destination as? InsertLinkVC
            
            let x = userLinks?[index].name?.capitalized
           // if userLinks?[index].image ==
            
            let storageRef = self.storage.reference().child("customLogoImg:\(self.getUserId()).png")
            if userLinks?[index].image == "\(storageRef)" {
                vc?.indexPath = 2
            }
            else {
                vc?.indexPath = 0
            }
            vc?.platform = x ?? "No Name"
            vc?.text = userLinks?[index].value ?? ""
            vc?.img = userLinks?[index].image ?? ""
            vc?.selectedLink = userLinks?[index]
            
            vc?.deleteDelegate = self
            vc?.delegate = self
        }
    }
    
    //MARK: Button Actions
    
    @IBAction func leadInfoButton(_ sender: UIButton) {
        if (isLeadModeOn){
            self.showAlert(title: "Alert!", message: "The lead capture is on")
        } else{
            self.showAlert(title: "Alert!", message: "The lead capture is off")
        }
    }
    @IBAction func directInfoButton(_ sender: UIButton) {
        if (isDirectModeOn){
            isDirectModeOn = true
            self.showAlert(title: "Alert!", message: "The direct mode is on")
        } else{
            self.showAlert(title: "Alert!", message: "The direct mode is off")
        }
    }
    
    @objc func editProfilePressed() {
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC

        self.navigationController?.pushViewController(editProfileVC, animated: true)
        
       // NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"EditProfileVC"])
    }
    
    @objc func deleteLinkPressed(_ sender: UIButton) {
        index = sender.tag
        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.deleteAlert) {
            self.deleteLink(platform: "platform")
        }

    }
    @objc func editLinkPressed(_ sender: UIButton) {
        index = sender.tag
        
        let addLinksVC = self.storyboard?.instantiateViewController(withIdentifier: "EditLinkVC") as! EditLinkVC
        addLinksVC.linkSelected = userLinks?[index]
        addLinksVC.delegate = self
        self.navigationController?.pushViewController(addLinksVC, animated: true)
    }
    //MARK: Switch Actions
    @objc func leadCaptureSwitchBtn(_ sender: UISwitch) {
        //startLoading()
        APIManager.updateUserLeadMode(leadMode: sender.isOn) { status in
            DispatchQueue.main.async {
                //self.stopLoading()
                if status == true {
                    self.collectionVu.reloadData()
                    print("Lead mode is now updated \(sender.isOn)")
                }
                else {
                    print("Lead mode updating error")
                }
            }

        }
        if (sender.isOn){
            isLeadModeOn = true
            //self.showAlert(title: "Alert!", message: "The lead capture is on")
        } else{
            isLeadModeOn = false
            //self.showAlert(title: "Alert!", message: "The lead capture is off")
        }
    }
    

    @objc func directSwitchBtn(_ sender: UISwitch) {
        
        if (sender.isOn){
            isDirectModeOn = true
            //            self.showAlert(title: "Alert!", message: "The direct mode is on")
        } else{
            isDirectModeOn = false
            //self.showAlert(title: "Alert!", message: "The direct mode is off")
        }
        
        //startLoading()
        APIManager.updateUserDirectMode(directMode: sender.isOn) { status in
            DispatchQueue.main.async {
                
                if sender.isOn {
                    APIManager.updateUserDirectLink(directLink:userLinks?[self.directOnIndex]) { directLinkStatus in
                        DispatchQueue.main.async {
                            //self.stopLoading()
                            self.collectionVu.reloadData()
                            if status == true {
                                print("Direct mode is now updated \(sender.isOn)")
                            }
                            else {
                                print("Direct mode off/updating error")
                            }
                        }
                    }
                }
                else {
                    //self.stopLoading()
                    self.collectionVu.reloadData()
                }
            }
        }
    }
    
    //MARK: Firebase Work
    func getUserLinks() {
        
        startLoading()
        
        APIManager.getUserData(id: getUserId()) { [self] (data) in
            
            guard data != nil else {
                showAlert(title: AlertConstants.Error, message: "User data not found, Please sign in again.")
                self.stopLoading()
                self.deleteUserModel()
                
                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                self.navigationController?.popToRootViewController(animated: true)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let rootVC = storyboard.instantiateViewController(withIdentifier: "SignInNavController") as! SignInNavController
                self.view.window?.rootViewController = rootVC
                
                return
            }
            
            userDetails = data
            saveUserToDefaults(userDetails!)
            
            userLinks = userDetails?.links
            //Get all social links and if name of any social icon is changed. it will also change name in user links            
            if let profileUrl = userDetails?.profileUrl, profileUrl != "" {
                downloadImage(from: profileUrl)
            }
            
            if let coverUrl = userDetails?.coverUrl, coverUrl != "" {
                setlogo(from: coverUrl)
            }
            if let directMode = userDetails?.directMode {
                self.isDirectModeOn = directMode
            }
            let linkCount = userDetails?.links?.count ?? 0
            if linkCount > 0 {
                for i in 0...(linkCount - 1) {
                    if userDetails?.links![i].name == userDetails?.direct?.name {
                        directOnIndex = i
                    }
                }
            }

            userDetails?.links?.forEach({ link in
                let storageRef = self.storage.reference().child("customLogoImg:\(self.getUserId()).png")
                if link.image == "\(storageRef)" {
                    DispatchQueue.main.async() {
                        storageRef.getData(maxSize: 1 * 1024 * 8024) { data, error in
                            
                            if let error = error {
                                print(error.localizedDescription)
                                // Uh-oh, an error occurred!
                            } else {
                                if let img = UIImage(data: data!) {
                                    customLogoImg = img
                                    self.saveLogoImageInDocumentDirectory(image: img)
                                    collectionVu.reloadData()
                                }
                            }
                        }
                    }
                }
            })
            
            let addNew = Links.init(value: "Add New", image: "gs://linkpod-331b0.appspot.com/socialImages/ic_add_new.png", name: "Add New", shareable: false)
            userLinks?.append(addNew)
            
            if userLinks?.count == 0 || userLinks == nil {
                userLinks = []
                userLinks?.append(addNew)
                
            } else {
                self.hideEmptyState()
            }
            
            print(userLinks as Any)
            DispatchQueue.main.async {
                self.showlayout()
                stopLoading()
                collectionVu.reloadData()
            }
            
            
        } error: { [self] err in
            stopLoading()
            showAlert(title: AlertConstants.Error, message: err)
        }
    }
    
    func deleteLink(platform: String) {
        
        startLoading()
        userLinks?.remove(at: index)
        
        APIManager.updateUserLinks() { [self] (success) in
            if success {
                let path = IndexPath(item: index, section: 0)
                collectionVu.deleteItems(at: [path])
                stopLoading()
                self.showAlert(title: AlertConstants.Success, message: "Link deleted.".localize())
                
            } else {
                stopLoading()
                self.showAlert(title: AlertConstants.Error, message: "Link could not be deleted.")
                
            }
            
        }
    }
    
    //MARK: Update userlinks
    func updateUserLinks(showAlert: Bool) {
        APIManager.updateUserLinks() { (success) in
            if success, showAlert{
                self.showAlert(title: AlertConstants.Success, message: "User Link Saved")
            }
            
        }
    }
    //MARK: Delegate Methods
    func linkReceived(text: String, platform: String, imageUrl: String?) {
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        collectionVu.reloadData()

        userLinks?[index].value = text
        userLinks?[index].name = platform
        if let imageUrl = imageUrl, imageUrl != "" {
            userLinks?[index].image = imageUrl
        }
        
        collectionVu.reloadData()
        //updateUserLinks(showAlert: true)
    }
    
    
    func updateSocialIconsArrangement() {
        //TODO: Update the array order if user re-arrange it
        if userLinks?.count ?? 0 > 0 {
            APIManager.updateUserLinks() { [self] (success) in
                self.stopLoading()
                if !success {
                    collectionVu.reloadData()
                    return
                }
            }
        }
    }
    
    func downloadImage(from url: String) {
        
        let ref = storage.reference(forURL: url)
        
        DispatchQueue.main.async() { [weak self] in
            
            let header = self?.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self?.headerIndex)!) as? CollectionHeaderView
            
            if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                header?.imgVu.image = cacheImg
                self?.profilePicture = cacheImg
            } else {
                ref.getData(maxSize: 1 * 1024 * 8024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                        // Uh-oh, an error occurred!
                    } else {
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                            header?.imgVu.image = img
                            self?.profilePicture = img
                        }
                    }
                }
            }
        }
    }
    
    func setlogo(from url: String) {
        
        let ref = storage.reference(forURL: url)
        
        DispatchQueue.main.async() { [weak self] in
            
            let header = self?.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self?.headerIndex)!) as? CollectionHeaderView
            
            ref.getData(maxSize: 1 * 1024 * 8024) { data, error in
                if let error = error {
                    print(error.localizedDescription)
                    // Uh-oh, an error occurred!
                } else {
                    if let img = UIImage(data: data!) {
                        imgChache.setObject(img, forKey: ref)
                        header?.coverImgVu.image = img
                        self?.coverPicture = img
                    }
                }
            }
        }
    }
}
extension DashBoardVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: Collectionview bottom cells
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        if indexPath.item == userLinks?.count {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        //TODO: To check is it done or not, to stop the icons update on firebase
        self.isMenuRearrangementDone = true
        
        print("Starting Index: \(sourceIndexPath.item)")
        print("Ending Index: \(destinationIndexPath.item)")
        
        let item = userLinks?[sourceIndexPath.item]
        userLinks?.remove(at: sourceIndexPath.item)
        userLinks?.insert(item!, at: destinationIndexPath.item)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        if userLinks?[indexPath.row].name == "Add New" {
            cell.deleteLinkBtn.isHidden = true
            cell.editLinkBtn.isHidden = true
        }
        else {
            cell.deleteLinkBtn.isHidden = true
            cell.editLinkBtn.isHidden = true
            cell.deleteLinkBtn.tag = indexPath.row
            cell.editLinkBtn.tag = indexPath.row
        }
        
        cell.deleteLinkBtn.addTarget(self, action: #selector(deleteLinkPressed(_:)), for: .touchUpInside)
        cell.editLinkBtn.addTarget(self, action: #selector(editLinkPressed), for: .touchUpInside)
        
        cell.contentView.frame = cell.bounds
        cell.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cell.outerVu.frame = cell.contentView.frame
        cell.imgVu.frame.size.width = cell.outerVu.frame.size.width
        cell.imgVu.frame.size.height = cell.outerVu.frame.size.height - 20
        cell.lbl.frame.origin.y = cell.imgVu.frame.size.height
        
        if isDirectModeOn {
            if directOnIndex == indexPath.row
            {
                cell.isEnabled(on: true)
            } else {
                cell.isEnabled(on: false)
            }
        } else  {
            cell.isEnabled(on: true)
        }
        
        cell.lbl.text = ""
        
        cell.imgVu.image = nil
        cell.imgVu.image = nil
        
        cell.imgVu.layer.cornerRadius = cell.imgVu.frame.height/2

        let link = userLinks?[indexPath.row]
        cell.imgVu.image = UIImage(named: link?.name?.lowercased() ?? "")

//        if link?.linkID == 4 {  //For custom Logo Link
//            if let img = UIImage(contentsOfFile: self.customLogoImgUrl.path)
//            {
//                cell.imgVu.image = img
//            }
//            else if let customLogoImg = customLogoImg {
//                cell.imgVu.image = customLogoImg
//            }
//            else {
//                cell.imgVu.loadGif(name: "1-Custom-Link")
//            }
//            cell.imgVu.contentMode = .scaleToFill
//            cell.imgVu.layer.masksToBounds = true
//        }
//
//        else {
//            cell.imgVu.image = UIImage(named: link?.name?.lowercased() ?? "")
//        }
        cell.lbl.text = link?.name!.localize()
        if userLinks?[indexPath.row].name == "Add New" {
            cell.lbl.text = "Add".localize()
        }
        cell.lbl.font = Constants.customFontRegular
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.bounds
        let width = (size.width / 4) - 20
        let height = width + 20
        return CGSize(width: width, height: height) // viewcollectionView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: CGFloat(430))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //TODO: Send data on firebase to update the sorting order of social icons
        if !isDirectModeOn {
            self.updateSocialIconsArrangement()
            
            if userLinks?[indexPath.row].name == "Add New" {
                //NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"AddLinksVC"])
                
                let addLinksVC = self.storyboard?.instantiateViewController(withIdentifier: "AddLinksVC") as! AddLinksVC
                self.navigationController?.pushViewController(addLinksVC, animated: true)
                
            } else {
                index = indexPath.row
//                performSegue(withIdentifier: "addLink", sender: self)
                let addLinksVC = self.storyboard?.instantiateViewController(withIdentifier: "EditLinkVC") as! EditLinkVC
                addLinksVC.linkSelected = userLinks?[index]
                addLinksVC.delegate = self
                self.navigationController?.pushViewController(addLinksVC, animated: true)
            }
        }
        
        else {
            directOnIndex = indexPath.row
            //startLoading()
            APIManager.updateUserDirectLink(directLink:userLinks?[self.directOnIndex]) { directLinkStatus in
                DispatchQueue.main.async {
                    //self.stopLoading()
                    self.collectionVu.reloadData()
                    if directLinkStatus {
                        print("Direct Link updated")
                    }
                    else {
                        print("Direct link updating error")
                    }
                }
            }
            collectionVu.reloadData()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isDirectModeOn {
            let cell = collectionView.cellForItem(at: [indexPath.row])
            cell?.isEnabled(on: false)
        }
    }
    
    func showEmptyState(message: String) {
        emptyStateMessage.text = message
        collectionVu.addSubview(emptyStateMessage)
        emptyStateMessage.centerXAnchor.constraint(equalTo: collectionVu.centerXAnchor).isActive = true
        emptyStateMessage.centerYAnchor.constraint(equalTo: collectionVu.centerYAnchor,constant: 50).isActive = true
    }
    
    func hideEmptyState() {
        emptyStateMessage.removeFromSuperview()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isDirectModeOn &&  (userLinks?.count ?? 0) > 0{
            return (userLinks?.count)! - 1
        }
        else{
            return userLinks?.count ?? 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // MARK: Collectionview Header cell
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionHeaderView", for: indexPath) as? CollectionHeaderView else {
                fatalError("Invalid view type")
            }
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            //headerView.nameLbl.text = userDetails?.name ?? ""
            headerView.userNameLbl.text = userDetails?.name ?? (userDetails?.username)!
            //headerView.emailLbl.text = userDetails?.email ?? ""
            headerView.imgVu.image = profilePicture
            headerView.coverImgVu.image = coverPicture
            
            //headerView.tickIcon.image = userDetails?.verifyUser == true ?  UIImage(named: "tick-.png") : nil
            headerIndex = indexPath
//            let numberOfPostsViewSelector : Selector = #selector(self.didSelectHeader)
//            let viewPostsViewGesture = UITapGestureRecognizer(target: self, action: numberOfPostsViewSelector)
//            viewPostsViewGesture.numberOfTapsRequired = 1
//            viewPostsViewGesture.delaysTouchesBegan = true
//            headerView.addGestureRecognizer(viewPostsViewGesture)
            
           // headerView.leadCapture_toggleBtn.isOn = isLeadModeOn
           // headerView.leadCapture_toggleBtn.addTarget(self, action: #selector(leadCaptureSwitchBtn(_:)), for: .valueChanged)
            
            headerView.direct_toggleBtn.isOn = isDirectModeOn
            headerView.direct_toggleBtn.addTarget(self, action: #selector(directSwitchBtn(_:)), for: .valueChanged)
            headerView.editProfileBtn.addTarget(self, action: #selector(editProfilePressed), for: .touchUpInside)
            
        default:
            assert(false, "Invalid element type")
        }
        
        return headerView
    }
    
    @objc func didSelectHeader(sender: UITapGestureRecognizer){
        print("HErE")
        //TODO: Menu re-arrangement then hit the fireabse udpate otherwise not
        if isMenuRearrangementDone == true {
            self.updateSocialIconsArrangement()
        }
        
        NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"AddLinksVC"])
    }
}

extension UICollectionViewCell {
    func isEnabled (on: Bool) {
//        self.isUserInteractionEnabled = on
        for view in contentView.subviews {
//            self.isUserInteractionEnabled = on
            view.alpha = on ? 1 : 0.5
        }
    }
}
