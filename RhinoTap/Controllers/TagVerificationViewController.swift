//
//  TagVerificationViewController.swift
//  RhinoTap
//
//  Created by Muhammad Abdullah on 22/02/2022.
//

import UIKit
import FirebaseAuth
import Firebase
import NFCReaderWriter


class TagVerificationViewController: BaseClass {
    
    @IBOutlet weak var clickOnLbl: UILabel!
    @IBOutlet weak var holdYourLbl: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    @IBOutlet weak var activateBtn: UIButton!
    
    let readerWriter = NFCReaderWriter.sharedInstance()
    var userId = ""
    var idInPayload = ""
    var payload = ""
    var isVarified: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var boldRange = ("To active your TapMee, Click Active TapMee and follow the instructions.".localize() as NSString).range(of: "TapMee".localize())
        
//        clickOnLbl.attributedText = boldenParts(string: "To active your TapMee, Click Active TapMee and follow the instructions.".localize(), boldCharactersRanges: [[Int(boldRange.location),Int(boldRange.length + boldRange.location)]], regularFont: UIFont(name: "Montserrat-Regular", size: 16.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 16.0))
        
        //starting from 0 and included space as a character
        clickOnLbl.attributedText = boldenParts(string: "Per attivare il tuo TapMee, fai clic su Attiva TapMee e segui le istruzioni", boldCharactersRanges: [[20,26],[40,53]], regularFont:UIFont(name: "Montserrat-Regular", size: 16.0) , boldFont: UIFont(name: "Montserrat-SemiBold", size: 16.0))
      
        boldRange = ("Once activated, place your TapMee tag at the bottom of your phone or any other surface.".localize() as NSString).range(of: "TapMee")
        holdYourLbl.attributedText = boldenParts(string: "Once activated, place your TapMee tag at the bottom of your phone or any other surface.".localize(), boldCharactersRanges: [[Int(boldRange.location),Int(boldRange.length + boldRange.location)]], regularFont: UIFont(name: "Montserrat-Regular", size: 16.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 16.0))
        
        boldRange = ("If you haven't ordered your TapMee yet, you can pick yours from our official website: www.TapMee.com".localize() as NSString).range(of: "TapMee")
        bottomLbl.attributedText = boldenParts(string: "If you haven't ordered your TapMee yet, you can pick yours from our official website: www.TapMee.com".localize(), boldCharactersRanges: [[Int(boldRange.location),Int(boldRange.length + boldRange.location)]], regularFont: UIFont(name: "Montserrat-Regular", size: 17.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 17.0))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.addBackground()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        //If user comes back without activation then logout the user
        //        guard APIManager.isInternetAvailable() else {
        //            showAlert(title: AlertConstants.InternetNotReachable, message: "")
        //            return
        //        }
        //
        //        let firebaseAuth = Auth.auth()
        //        do {
        //            try firebaseAuth.signOut()
        //        } catch let signOutError as NSError {
        //            print("Error signing out: %@", signOutError)
        //            return
        //        }
        //
        //        Constants.refs.databaseUser.child(getUserId()).updateChildValues([
        //            "fcmToken":""
        //        ])
        
        
        //deleteUserModel()
//        UserDefaults.standard.setValue(false, forKey: Constants.status)
//        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"TabBarController"])

        
    }
    
    func boldenParts(string: String, boldCharactersRanges: [[Int]], regularFont: UIFont?, boldFont: UIFont?) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 17)])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont ?? UIFont.boldSystemFont(ofSize: regularFont?.pointSize ?? UIFont.systemFontSize)]
        for range in boldCharactersRanges {
            let currentRange = NSRange(location: range[0], length: range[1]-range[0])
            attributedString.addAttributes(boldFontAttribute, range: currentRange)
        }
        return attributedString
    }
    
    @IBAction func tapToActivatePressed(_ sender: Any) {
        writeTag()
    }
    
    func writeTag(){
        readerWriter.newWriterSession(with: self, isLegacy: true, invalidateAfterFirstRead: true, alertMessage: "Ready to scan - hold the TapMee tag close to your smartphone to set up your tag".localize())
        readerWriter.begin()
        self.readerWriter.detectedMessage = "Write data successfully".localize()
    }
    
    func readTag() {
        guard NFCReaderSession.readingAvailable else {
            self.showAlert(title: AlertConstants.Error, message: "Hold your iPhone near to write the data".localize())
            return
        }
        
        readerWriter.newWriterSession(with: self, isLegacy: false, invalidateAfterFirstRead: true, alertMessage: AlertConstants.scanAlert.localize())
        readerWriter.begin()
        readerWriter.detectedMessage = "Detected Tag info".localize()
        return
    }
    
    // MARK: - Utilities
    func contentsForMessages(_ messages: [NFCNDEFMessage]) -> String {
        var recordInfos = ""
        
        if let record = messages.first?.records.first {
            if let string = String(data: record.payload, encoding: .ascii) {
                recordInfos = "\(string)"
            }
        }
        
        return recordInfos
    }
    
    func getTagInfos(_ tag: __NFCTag) -> [String: Any] {
        var infos: [String: Any] = [:]
        
        switch tag.type {
            case .miFare:
                if #available(iOS 13.0, *) {
                    if let miFareTag = tag.asNFCMiFareTag() {
                        switch miFareTag.mifareFamily {                                
                            case .desfire:
                                infos["TagType"] = "MiFare DESFire"
                            case .ultralight:
                                infos["TagType"] = "MiFare Ultralight"
                            case .plus:
                                infos["TagType"] = "MiFare Plus"
                            case .unknown:
                                infos["TagType"] = "MiFare compatible ISO14443 Type A"
                            @unknown default:
                                infos["TagType"] = "MiFare unknown"
                        }
                        if let bytes = miFareTag.historicalBytes {
                            infos["HistoricalBytes"] = bytes.hexadecimal
                        }
                        infos["Identifier"] = miFareTag.identifier.hexadecimal
                    }
                } else {
                    // Fallback on earlier versions
                }
            case .iso7816Compatible:
                if #available(iOS 13.0, *) {
                    if let compatibleTag = tag.asNFCISO7816Tag() {
                        infos["TagType"] = "ISO7816"
                        infos["InitialSelectedAID"] = compatibleTag.initialSelectedAID
                        infos["Identifier"] = compatibleTag.identifier.hexadecimal
                        if let bytes = compatibleTag.historicalBytes {
                            infos["HistoricalBytes"] = bytes.hexadecimal
                        }
                        if let data = compatibleTag.applicationData {
                            infos["ApplicationData"] = data.hexadecimal
                        }
                        infos["OroprietaryApplicationDataCoding"] = compatibleTag.proprietaryApplicationDataCoding
                    }
                } else {
                    // Fallback on earlier versions
                }
            case .ISO15693:
                if #available(iOS 13.0, *) {
                    if let iso15693Tag = tag.asNFCISO15693Tag() {
                        infos["TagType"] = "ISO15693"
                        infos["Identifier"] = iso15693Tag.identifier
                        infos["ICSerialNumber"] = iso15693Tag.icSerialNumber.hexadecimal
                        infos["ICManufacturerCode"] = iso15693Tag.icManufacturerCode
                    }
                } else {
                    // Fallback on earlier versions
                }
                
            case .feliCa:
                if #available(iOS 13.0, *) {
                    if let feliCaTag = tag.asNFCFeliCaTag() {
                        infos["TagType"] = "FeliCa"
                        infos["Identifier"] = feliCaTag.currentIDm
                        infos["SystemCode"] = feliCaTag.currentSystemCode.hexadecimal
                    }
                } else {
                    // Fallback on earlier versions
                }
            default:
                break
        }
        return infos
    }
    
}

@available(iOS 11.0, *)
extension TagVerificationViewController: NFCReaderDelegate {
    
    func readerDidBecomeActive(_ session: NFCReader) {
        print("Reader did become")
    }
    
    func reader(_ session: NFCReader, didInvalidateWithError error: Error) {
        print("ERROR:\(error)")
        readerWriter.end()
    }
    
    /// -----------------------------
    // MARK: 2. NFC Writer(iOS 13):
    /// -----------------------------
    @available(iOS 13.0, *)
    func reader(_ session: NFCReader, didDetect tags: [NFCNDEFTag]) {
        print("did detect tags")

        let uriPayloadFromURL = NFCNDEFPayload.wellKnownTypeURIPayload(
                url: URL(string: baseUrl+getUserId())!
            
            )!
        

        let message = NFCNDEFMessage(records: [uriPayloadFromURL])

        readerWriter.write(message, to: tags.first!) { (error) in
            if let err = error {
                print("ERR:\(err)")
            } else {
                print("write success")
            }
            self.readerWriter.end()
        }
    }
    /// --------------------------------
    // MARK: 3. NFC Tag Reader(iOS 13)
    /// --------------------------------
    func reader(_ session: NFCReader, didDetect tag: __NFCTag, didDetectNDEF message: NFCNDEFMessage) {
  
        _ = readerWriter.tagIdentifier(with: tag)
        let payload = contentsForMessages([message])
        let tagInfos = getTagInfos(tag)
        var tagInfosDetail = ""
        
        tagInfos.forEach { (item) in
            tagInfosDetail = tagInfosDetail + "\(item.key): \(item.value)\n"
        }
        
        DispatchQueue.main.async {
            
            print("Tag Data:",payload)
            guard !payload.isEmpty else {
//                self.showAlert(title: AlertConstants.Alert, message: "No data found in Pod.")
                self.showAlertController(with: "")
                return
            }
            
            self.payload = payload
            
           /*
            //TODO: To add the tag activation part
            self.readTagByPayload(payload)
            */
            
            
        }
        
        self.readerWriter.alertMessage = "NFC Tag Info detected".localize()
        self.readerWriter.end()
    }
    
    func readTagByPayload(_ payload: String) {
        
        //sample: https://user.https://avicennaenterprise.com/12345
        if payload.contains("user.tapmee.it") {
            
            let strArr = payload.components(separatedBy: "/")
            
            if strArr.count > 0 {
                
                let uid =  strArr[1]//"521456"
                //TODO: Call api and check the status of the NFC tag
                
                Constants.refs.databaseTag.queryOrdered(byChild: "tagId").queryEqual(toValue: uid).observeSingleEvent(of: .value) { snapshot in
                    
                    if snapshot.exists() {
                        
                        for tagsSnapshot in snapshot.children {
                            
                            let tagSnap = tagsSnapshot as! DataSnapshot
                            
                            let response = tagSnap.value as! [String: Any]
                            _ = response["id"] as? String ?? "100"
                            _ = response["isDeleted"] as? Bool ?? false
                            _ = response["tagId"] as? String ?? "100"
                            
                            if let status = response["status"] as? Bool, status == false {
                                
                                //Update status of tag: means it's varified now
                                APIManager.tagStatusUpdate(tagSnap.key, self.getUsername()) { status in
                                    
                                    if status {
                                        print("success")
                                        //TODO: show alert of sucess message
                                        
                                        let controller = self.getControllerRef(controller: ShowAlertViewController.id, storyboard: Storyboards.Main.id) as! ShowAlertViewController
                                        controller.isVerified = true
                                        controller.delegate = self
                                        controller.tagID = uid
                                        self.present(controller, animated: true)
                                    }
                                }
                                
                            } else {
                                self.showAlert(title: AlertConstants.Alert, message: "This TapMee Tag is already used by someone's, please use another tag to activate your profile".localize())
                            }
                        }
                    } else {
                        //TODO: Show invalid tag here
                        let controller = self.getControllerRef(controller: ShowAlertViewController.id, storyboard: Storyboards.Main.id) as! ShowAlertViewController
                        controller.notListed = true
                        controller.delegate = self
                        self.present(controller, animated: true)

                    }
                }
            }
        } else {
            //TODO: show alert of sucess message
            let controller = self.getControllerRef(controller: ShowAlertViewController.id, storyboard: Storyboards.Main.id) as! ShowAlertViewController
            controller.isVerified = false
            controller.delegate = self
            self.present(controller, animated: true) {}
        }
    }
    
    func showAlertController(with uid: String) {
        let controller = self.getControllerRef(controller: ShowAlertViewController.id, storyboard: Storyboards.Main.id) as! ShowAlertViewController
        controller.isVerified = false
        controller.delegate = self
        controller.tagID = uid
        self.present(controller, animated: true)
    }
}
extension TagVerificationViewController : verifyTag {
        func tagVerification(_ status: Bool) {
            print(#function)
            if status == true {
                self.showAlert(title: AlertConstants.Alert, message: "Your profile is active now".localize()) {
                }
        }
    }
}
