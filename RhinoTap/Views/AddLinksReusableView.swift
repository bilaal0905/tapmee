

import UIKit
import iOSDropDown
class AddLinksReusableView: UICollectionReusableView {
    
    
    @IBOutlet weak var addLogoBtn: UIButton!
    @IBOutlet weak var addProfileImg: UIButton!
    @IBOutlet weak var removeLogoBtn: UIButton!
    @IBOutlet weak var removeProfileImg: UIButton!
    @IBOutlet weak var logoBtn: UIButton!
    @IBOutlet weak var editUsernameBtn: UIButton! {
        didSet {
            editUsernameBtn.tag = 1
        }
    }
    @IBOutlet weak var phoneVu: UIView!
    @IBOutlet weak var addressVu: UIView!
    
    @IBOutlet weak var editPhoneBtn: UIButton! {
        didSet {
            editPhoneBtn.tag = 2
        }
    }
    @IBOutlet weak var editBioBtn: UIButton! {
        didSet {
            editBioBtn.tag = 4
        }
    }
    
    @IBOutlet weak var editAddressBtn: UIButton! {
        didSet {
            editAddressBtn.tag = 3
        }
    }
    
    
    @IBOutlet weak var nameTF: UITextField! {
        didSet {
            nameTF.tag = 11
        }
    }
    @IBOutlet weak var phoneNumberTF: UITextField!{
        didSet {
            phoneNumberTF.tag = 22
        }
    }
    @IBOutlet weak var addressTF: UITextField!{
        didSet {
            addressTF.tag = 33

        }
    }
    
    
    @IBOutlet weak var genderDropDown: DropDown!
    @IBOutlet weak var profileImg: UIImageView! {
        didSet {
            profileImg.layer.cornerRadius = profileImg.frame.height/2
        }
    }
    
    
    @IBOutlet weak var dobBtn: UIButton!
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var userNameVu: UIView!
    
    @IBOutlet weak var outerVu: UIView!
    {
        didSet {
            outerVu.layer.cornerRadius = 25
            layer.backgroundColor = UIColor.clear.cgColor
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOffset = CGSize(width: 2.0, height: 0.0)
            layer.shadowOpacity = 0.2
            layer.shadowRadius = 2.0
            
            outerVu.layer.masksToBounds = true
            addSubview(outerVu)
            outerVu.translatesAutoresizingMaskIntoConstraints = false
            
            // pin the containerView to the edges to the view
            outerVu.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            outerVu.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            outerVu.topAnchor.constraint(equalTo: topAnchor).isActive = true
            outerVu.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
    }
    
    
}
