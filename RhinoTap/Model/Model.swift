

import Foundation

struct User: Codable {
    
    var id: String?
    var name: String?
    var email: String?
    var fcmToken:String?
    var phone: String?
    var gender: String?
    var username: String?
    var profileUrl: String?
    var bio: String?
    var dob: String?
    var address: String?
    var coverUrl: String?
    var isReqByMe: Bool?
    var isReqByOther: Bool?
    var profileOn: Int?
    var links : [Links]?
    var isDeleted: Bool?
    var platform: String?
    var verifyUser: Bool?
    var tagUid: String?
    var leadMode: Bool?
    var directMode: Bool?
    var direct: Links?
    var timestamp: String?
    
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
    
    private init() {}
    static let shared = User()
    
    init(id: String? = nil,
         name: String? = nil,
         email: String? = nil,
         fcmToken: String? = nil,
         profileUrl: String? = nil,
         bio: String? = nil,
         username: String? = nil,
         phone: String? = nil,
         gender: String? = nil,
         dob: String? = nil,
         address: String? = nil,
         coverUrl: String? = nil,
         profileOn: Int? = nil,
         isDeleted: Bool? = nil,
         platform: String? = nil,
         verifyUser: Bool? = nil,
         tagUid: String? = nil,
         timestamp: String? = nil
    ) {
        
        self.id = id
        self.email = email
        self.name = name
        self.fcmToken = fcmToken
        self.username = username
        self.profileUrl = profileUrl
        self.phone = phone
        self.gender = gender
        self.bio = bio
        self.dob = dob
        self.address = address
        self.coverUrl = coverUrl
        self.profileOn = profileOn
        self.isDeleted = isDeleted
        self.platform = platform
        self.verifyUser = verifyUser
        self.tagUid = tagUid
        self.timestamp = timestamp
    }
    
    func getUserId() -> String {
        return self.id ?? ""
    }
}

struct Links : Codable {
    var linkID : Int?
    var image : String?
    var shareable : Bool?
    var name : String?
    var value : String?
    enum CodingKeys: String, CodingKey {
        case linkID = "linkID"
        case image = "image"
        case shareable = "shareable"
        case name = "name"
        case value = "value"
    }
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }

    init (
        linkID: Int? = nil,
        value : String? = nil,
        image : String? = nil,
        name : String? = nil,
        shareable : Bool? = nil
    ) {
        self.linkID = linkID
        self.value = value
        self.image = image
        self.name = name
        self.shareable = shareable
     }
}
struct Contacts : Codable {
   
    var id : String?
    var email : String?
    var phone: String?
    var message: String?
    var name: String?
    var userid: String?
    var company: String?
    var job: String?

    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
}

struct Tag : Codable {
    var id : String?
    var isDeleted : Bool?
    var status : Bool?
    var tagId : String?
    
    enum CodingKeys: String, CodingKey {
    case id = "id"
    case isDeleted = "isDeleted"
    case status = "status"
    case tagId = "tagId"
}
    init(
        id: String? = nil,
        isDeleted: Bool? = false,
        status: Bool? = nil,
        tagId: String? = nil
    )
    {
        self.id = id
        self.isDeleted = isDeleted
        self.status = status
        self.tagId = tagId
    }
}
