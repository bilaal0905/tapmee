
import UIKit
import Firebase
import UserNotifications
import FirebaseMessaging
import FirebaseDatabase

class PushNotificationManager: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    func registerForPushNotifications() {
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        if #available(iOS 14, *) {
                            
                        } else {
                            
                        }
                       }
                })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
      //  updateTokenIfNeeded()
        
    }
    
    func updateTokenIfNeeded() {
        
        let status = UserDefaults.standard.bool(forKey: Constants.status)
        let vc = BaseClass()
        if let token = Messaging.messaging().fcmToken {
            print("Token updated FCM ",token)
            if status {
                if token != vc.getFCM() {
                    Constants.refs.databaseUser.child(vc.getUserId()).updateChildValues([
                        "fcmToken" : token
                    ]) { (error:Error?, ref:DatabaseReference) in
                        
                        if let error = error {
                            print("Data could not be saved: \(error).")
                        } else {
                            UserDefaults.standard.setValue(token, forKey: "FCMToken")
                            print("FCM Token updated successfully!")
                       }
                    }
                }
            }
        } else {
            print("Token is null")
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        print("FCM Token ",fcmToken!)
        updateTokenIfNeeded()
    }
    
    // This function is called when notification arrives in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Notification is about to present")
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        completionHandler([.alert, .sound])
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      
        print("Notification Received")
        if let stringData = userInfo["userId"] as? String {
                  print("String Data: \(stringData)")
              }
        
        print(userInfo)
       

    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.hexString
        print("Device token :",deviceTokenString)
        //        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    
    // This method is called when user clicked on the notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response)
        
        let userInfo = response.notification.request.content.userInfo
        print("InfO: ", userInfo)
        
        guard let id = userInfo["userId"] as? String else { return }
        
        moveToContactScreen(userId: id)
        
        completionHandler()
        
    }
    
    private func moveToContactScreen(userId: String) {
        
        guard let window = UIApplication.shared.keyWindow else { return }
        
        // instantiate the view controller from storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            let vc = storyboard.instantiateViewController(identifier: "ViewContactVC") as! ViewContactVC
            let navController = UINavigationController(rootViewController: vc)
            navController.navigationBar.isHidden = true
            navController.modalPresentationStyle = .fullScreen
            vc.uid = userId
            vc.didTapNotification = true
            window.rootViewController = vc
            window.makeKeyAndVisible()
            UIView.transition(with: window,
                              duration: 0.3,
                              options: .transitionFlipFromLeft,
                              animations: nil,
                              completion: nil)
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    
}
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
