

import UIKit
import Firebase
import Contacts
import ContactsUI
import Appz
import SDWebImage


class ViewContactVC: BaseClass, CNContactViewControllerDelegate {
    
    @IBOutlet weak var collectionVu: UICollectionView!
    @IBOutlet weak var cvTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurEffect: UIVisualEffectView! {didSet{
        blurEffect.frame.size = view.safeAreaLayoutGuide.layoutFrame.size
    }}
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var lockImageVu: UIImageView!
    
    @IBOutlet weak var profileOffLbl: UILabel! {didSet{
        profileOffLbl.text = "This profile is private\n(Ask for your consent to receive your info)"
    }}
    
    var didTapNotification = false
    
    var contactLinks : [Links]?
    var uid = ""
    var nameInContacts = ""
    var isRequestedByMe = false
    var isRequestedByOther = false
    var tagScanned = false
    var index = -1
    var headerIndex : IndexPath? = IndexPath(item: 0, section: 0)
    var profilePicture = UIImage(named: "user_black")
    var logoImg = UIImage(named: "img")
    var headerHeight : CGFloat = 410.0
    var headerTotalHeight : CGFloat = 410.0
    var lblHeight : CGFloat = 0.0
    var bioText = "Bio"
    var dob = "Date of Birth"
    var userDetails : User? = nil
    let storage = Storage.storage()
    
    
    lazy var emptyStateMessage: UILabel = {
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 17)
        messageLabel.sizeToFit()
        return messageLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionVu.delegate = self
        collectionVu.dataSource = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if tagScanned {
            cvTopConstraint.constant = 15
        } else {
            cvTopConstraint.constant = 0
        }
        
        getContactsData()
    }
    
    func showPrivateProfileVu() {
        //        self.blurEffect.isHidden = false
        self.view.addSubview(blurEffect)
        self.lockImageVu.image = #imageLiteral(resourceName: "icons8-lock")
        self.label1.addBlurrEffect()
        self.label2.addBlurrEffect()
        self.label3.addBlurrEffect()
    }
    
    func hidePrivateProfileVu() {
        self.lockImageVu.image = UIImage(named: "icons8-unlock")
        showEmptyState(message: "Connect with user to show links")
        blurEffect.removeFromSuperview()
    }
    
    @IBAction func tappedBackBtnOfBlurEffect(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func connectBackPressed(_ button:UIButton) {
        let title = button.title(for: .normal)
        let myUser = readUserData()
        
        guard let myId = myUser?.id else {
            return
        }
        guard let id = userDetails?.id, myId != id else {
            return
        }
        var msg = ""
        var newTitle = connectBtnTitle.Accept.rawValue
        var dict : [String : Any] = [:]
        
        if title == "Accept" {
            dict = [
                "\(myId)/\(id)/isReqByMe": true,
                "\(myId)/\(id)/isReqByOther": true,
                "\(id)/\(myId)/isReqByMe": true,
                "\(id)/\(myId)/isReqByOther": true,
            ]
            isRequestedByMe = true
            isRequestedByOther = true
            msg = "Request Accepted"
            newTitle = connectBtnTitle.Connected.rawValue
            
            APIManager.updateConnectUserValue(contactId: id, dict, completion: { [self] success in
                self.stopLoading()
                if success {
                    self.showAlert(title: AlertConstants.Success, message: msg)
                    button.setTitle(newTitle, for: .normal)
                    let notif = PushNotificationSender()
                    if let fcm = userDetails?.fcmToken {
                        notif.sendPushNotification(fcmToken: fcm, title: "Request Accepted", body: "\(getName()) has accepted your request.")
                    }
                    collectionVu.reloadData()
                } else {
                    self.showAlert(title: AlertConstants.Error, message: "Could not update status")
                }
            })
        } else if title == "Connect" {
            
            startLoading()
            dict = [
                "\(myId)/\(id)/isReqByMe": true,
                "\(id)/\(myId)/isReqByOther": true,
                "\(id)/\(myId)/isReqByMe": false,
                "\(myId)/\(id)/isReqByOther": false,
                "\(id)/\(myId)/name": myUser?.name as Any,
                "\(id)/\(myId)/id" : myId,
                "\(id)/\(myId)/profileUrl" : myUser?.profileUrl as Any ,
                "\(id)/\(myId)/username": myUser?.username as Any,
            ]
            isRequestedByMe = true
            isRequestedByOther = false
            newTitle = connectBtnTitle.Invited.rawValue
            msg = "Invitation sent for connection"
            
            let notif = PushNotificationSender()
            if let fcm = userDetails?.fcmToken {
                notif.sendPushNotification(fcmToken: fcm, title: "Connection Request", body: "Connection request from \(getName()). Accept to share links.")
            }
            
            APIManager.updateConnectUserValue(contactId: id, dict, completion: { success in
                self.stopLoading()
                if success {
                    self.showAlert(title: AlertConstants.Success, message: msg)
                    button.setTitle(newTitle, for: .normal)
                    
                } else {
                    self.showAlert(title: AlertConstants.Error, message: "Could not update status")
                }
            })
        } else {
            dict = [
                "\(myId)/\(id)/isReqByMe": false,
                "\(id)/\(myId)/isReqByOther": false,
                "\(myId)/\(id)/isReqByOther": false,
                "\(id)/\(myId)/isReqByMe": false,
                "\(id)/\(myId)/name": myUser?.name as Any,
                "\(id)/\(myId)/id" : myId,
                "\(id)/\(myId)/profileUrl" : myUser?.profileUrl as Any ,
                "\(id)/\(myId)/username": myUser?.username as Any
            ]
            
            isRequestedByMe = false
            isRequestedByOther = false
            newTitle = connectBtnTitle.Connect.rawValue
            msg = "Are sure to remove "
            
            showTwoBtnAlert(title: "Confirm", message: msg + (userDetails?.name ?? "this user"), yesBtn: "Confirm", noBtn: "Cancel") { yes in
                if yes {
                    self.startLoading()
                    
                    APIManager.updateConnectUserValue(contactId: id, dict, completion: { success in
                       
                        if success {
                            button.setTitle(newTitle, for: .normal)
                            button.backgroundColor = UIColor.white
                            button.setTitleColor(UIColor.black, for: .normal)
                            self.collectionVu.reloadData()
                            self.stopLoading()
                        } else {
                            self.stopLoading()
                            self.showAlert(title: AlertConstants.Error, message: "Could not remove this connection.")
                        }
                    })
                }
            }
            
        }
        
    }
    
    @objc func addToContacts(_ button:UIButton) {
        checkContactsAuthorizationStatus()
    }
    func checkContactsAuthorizationStatus() {
        let store = CNContactStore()
        
        switch CNContactStore.authorizationStatus(for: .contacts){
        case .authorized:
            createContact()
        case .notDetermined:
            store.requestAccess(for: .contacts){succeeded, err in
                guard err == nil && succeeded else{
                    return
                }
                self.createContact()
            }
        case .denied,.restricted:
            showPermissionAlert(title: "Contacts Permission Required", message: "Please enable contacts permissions in settings.")
        default:
            print("Not handled")
            
        }
        
        
    }
    
    
    func createContact() {
        _ = CNContactStore()
        
        guard let name = userDetails?.name, !name.isEmpty else {
            showAlert(title: AlertConstants.Error, message: "Name is empty, so contacts could not be saved.")
            return
        }
        let contact = CNMutableContact()
        contact.givenName = name
        
        //    profile photo
        if profilePicture != UIImage(named: "user_black") {
            if let img = profilePicture,
               let data = img.pngData() {
                contact.imageData = data
            }
        }
        
        // Address
        if let text = userDetails?.address {
            let address = CNMutablePostalAddress()
            address.city = text
            let home = CNLabeledValue<CNPostalAddress>(label:CNLabelHome, value:address)
            contact.postalAddresses = [home]
        }
        
        
        if let phone = userDetails?.phone {
            //  set the phone numbers
            let homePhone = CNLabeledValue(label: CNLabelHome,
                                           value: CNPhoneNumber(stringValue: phone))
            contact.phoneNumbers = [homePhone]
        }
        
        if let work = userDetails?.email {
            let workEmail = CNLabeledValue(label:"Work Email",
                                           value: (work) as NSString)
            
            contact.emailAddresses = [workEmail]
        }
        
        DispatchQueue.main.async {
            
            let contactViewController = CNContactViewController(forNewContact: contact)
            
            
            
            contactViewController.delegate = self
            let navigationController = UINavigationController(rootViewController: contactViewController)
            
            navigationController.modalPresentationStyle = .formSheet
            
            self.present(navigationController, animated: true, completion: nil)
            
        }
        //
        //          let request = CNSaveRequest()
        //        request.add(contact, toContainerWithIdentifier: nil)
        //          do{
        //            try store.execute(request)
        //            showAlert(title: AlertConstants.Alert, message: "Thus contact has bee saved to your phone.")
        //          } catch let err{
        //            print("Failed to save the contact. \(err)")
        //          }
    }
    
    @objc func goBack(_ button:UIButton){
        
        if didTapNotification {
            guard let window = UIApplication.shared.keyWindow else { return }
            let rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationHome") as! UINavigationController
            
            window.rootViewController = rootViewController
            window.makeKeyAndVisible()
            UIView.transition(with: window,
                              duration: 0.3,
                              options: .transitionCrossDissolve,
                              animations: nil,
                              completion: nil)
            
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    
    @objc func dismiss(_ button:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewLink" {
            if let vc = segue.destination as? InsertLinkVC {
                let x = self.contactLinks?[index].name?.capitalized
                vc.platform = x ?? "No Name"
                vc.text = self.contactLinks?[index].value ?? ""
                vc.img = self.contactLinks?[index].image ?? ""
                vc.isViewOnly = true
            }
        }
    }
    
    
    //MARK: Get Contacts Data
    
    func getContactsData() {
        guard !uid.isEmpty else {
            showAlert(title: AlertConstants.Error, message: "No data found in tag")
            return
        }
        print(uid)
        startLoading()
        
        /*
         Get User details against the uid
         */
        
        APIManager.getUserData(id: uid) { [self] (user) in
            guard user != nil else {
                self.stopLoading()
                self.showAlert(title: AlertConstants.Error, message: "Could not find user data")
                return
            }
            
            print("User Data found:",user as Any)
            
            let fetchedUser = user
            self.userDetails = user
            contactLinks = []
            contactLinks = user?.links
            
            if tagScanned || didTapNotification {
                addToContacts(user: fetchedUser!)
            } else {
                if let name = fetchedUser?.name,
                   let id = fetchedUser?.id {
                    /*
                     check name is in tableview ist or not
                     */
                    if name != nameInContacts {
                        let myId = getUserId()
                        let param = [
                            "\(myId)/\(id)/name": name,
                            "\(myId)/\(id)/id": id
                        ]
                        
                        APIManager.updateConnectUserValue(contactId: id, param) { success in
                            
                            if success {
                                print("User's name updated")
                            }
                        }
                    }
                }
            }
            
                        
            let header = self.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self.headerIndex)!) as? ContactDetailsView
            
            
            if let url = fetchedUser?.profileUrl, url != "" {
                let reference = storage.reference(forURL: url)
                reference.downloadURL { url, error in
                    guard let url = url else { return }
                    print(url)
                    header?.profileImg?.sd_setImage(with: url, placeholderImage: UIImage(named: "img"), completed: { (image, error, cacheType, storageRef) in
                        if image != nil && error != nil {
                            UIView.animate(withDuration: 0.3) {
                                header?.profileImg.alpha = 1
                            }
                        }
                    })
                }
            }
            
            if let url = fetchedUser?.coverUrl, url != "" {
                let reference = storage.reference(forURL: url)
                reference.downloadURL { url, error in
                    guard let url = url else { return }
                    print(url)
                    header?.logoImg?.sd_setImage(with: url, placeholderImage: UIImage(named: "img"), completed: { (image, error, cacheType, storageRef) in
                        if image != nil && error != nil {
                            UIView.animate(withDuration: 0.3) {
                                header?.logoImg.alpha = 1
                            }
                        }
                    })
                }
            }

            
            header?.bioLbl.text = fetchedUser?.bio
            header?.phoneLbl.text = fetchedUser?.phone
            header?.phoneLbl.addBlurrEffect()
            header?.emailLbl.text = fetchedUser?.email
            header?.emailLbl.addBlurrEffect()
            header?.addressLbl.text = fetchedUser?.address
            
            /*
             Bring back button to front
             */
            
            if userDetails?.bio == "" {
                userDetails?.bio = "Bio"
            }
            
            if userDetails?.address == "" {
                userDetails?.address = "Address"
            }
            
            /*
             Increase height based on text
             */
            let height1 = (userDetails?.bio ?? "Bio").getStringHeight(constraintedWidth: collectionVu.frame.width - 20, font: UIFont.systemFont(ofSize: 20.0))
            
            let height2 = (userDetails?.address ?? "Address").getStringHeight(constraintedWidth: collectionVu.frame.width - 20, font: UIFont.systemFont(ofSize: 17.0))
            print(headerTotalHeight)
            print(height1)
            print(height2)
            headerTotalHeight = headerHeight + height1 + height2
            print(headerTotalHeight)
            
            DispatchQueue.main.async {
                stopLoading()
                self.collectionVu.reloadData()
            }
            
        } error: { (desc) in
            self.stopLoading()
            self.showAlert(title: AlertConstants.Error, message: desc)
        }
    }
    
    func addToContacts(user: User) {
        let dict = [
            "id" : user.id!,
            "name" : user.name!,
            "profileUrl" : user.profileUrl ?? "",
            "email": user.email!,
            "isReqByMe": false,
            "isReqByOther": false
        ] as [String : Any]
        
        APIManager.addNewContactToFirebase(myId: getUserId(), dict as APIManager.dictionaryParameter) { success,error in
            if success {
                self.showAlert(title: AlertConstants.Alert, message: "User added to your in app contacts")
            } else {
                self.showAlert(title: AlertConstants.Alert, message: error)
            }
        }
    }
}




extension ViewContactVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditProfileCell", for: indexPath) as! EditProfileCell
        
        let ref = storage.reference(forURL: contactLinks?[indexPath.row].image ?? "")
        
        let imgCache = NSCache<AnyObject, AnyObject>()
        if let cacheImg = imgCache.object(forKey: ref) as? UIImage {
            cell.imgVu.image = cacheImg
        } else {
            ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error.localizedDescription,indexPath.row)
                } else {
                    
                    if let img = UIImage(data: data!) {
                        imgCache.setObject(img, forKey: ref)
                        cell.imgVu.image = img
                    }
                }
            }
        }
        
        cell.lbl.text = contactLinks?[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.bounds
        let width = (size.width / 4) - 10
        let height = width + 20
        
        return CGSize(width: width, height: height) // viewcollectionView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        
        performSegue(withIdentifier: "viewLink", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var rows = 0
        
        if isRequestedByMe && isRequestedByOther && (userDetails?.profileOn ?? 1) == 1 {
            rows = contactLinks?.count ?? 0
        }
        
        if rows == 0 {
            if isRequestedByMe && isRequestedByOther && (userDetails?.profileOn ?? 1) == 1 {
                showEmptyState(message: "No links were added by user")
            }
            else if (userDetails?.profileOn == nil || userDetails?.profileOn == 0) {
                self.showPrivateProfileVu()
                print("----called---self.showPrivateProfileVu()")
            } else if (userDetails?.profileOn == 1) {
                self.hidePrivateProfileVu()
                print("----called---self.hidePrivateProfileVu()")
            }
        } else {
            hideEmptyState()
        }
        return rows
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: CGFloat(headerTotalHeight))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ContactDetailsView", for: indexPath) as? ContactDetailsView else {
                fatalError("Invalid view type")
            }
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            if tagScanned {
                headerView.backBtn.isHidden = true
            } else {
                headerView.dismissBtn.isHidden = true
            }
            //TODO: Show the pictures to logo and profile
//            if let url = self.userDetails?.profileUrl, url != "" {
//
//                let reference = storage.reference(withPath: url)
//                reference.downloadURL { url, error in
//                  guard let url = url else { return }
//
//                    headerView.profileImg?.sd_setImage(with: url, placeholderImage: UIImage(named: "user_black"), completed: { (image, error, cacheType, storageRef) in
//                    if image != nil && error != nil {
//                      UIView.animate(withDuration: 0.3) {
//                          headerView.profileImg.alpha = 1
//                      }
//                    }
//                  })
//                }
//            }
//
//            if let url = self.userDetails?.logoUrl, url != "" {
//
//                let reference = storage.reference(withPath: url)
//                reference.downloadURL { url, error in
//                  guard let url = url else { return }
//
//                    headerView.logoImg?.sd_setImage(with: url, placeholderImage: UIImage(named: "user_black"), completed: { (image, error, cacheType, storageRef) in
//                    if image != nil && error != nil {
//                      UIView.animate(withDuration: 0.3) {
//                          headerView.logoImg.alpha = 1
//                      }
//                    }
//                  })
//                }
//            }

            headerView.nameLbl.text = userDetails?.name ?? "Name not found"
            
            if userDetails?.profileOn == 1 && isRequestedByOther && isRequestedByMe {
                
                headerView.phoneLbl.removeBlurEffect()
                headerView.emailLbl.removeBlurEffect()
                headerView.addressLbl.removeBlurEffect()
                
                
                if userDetails?.bio == "" {
                    headerView.bioLbl.text = "Bio"
                } else {
                    headerView.bioLbl.text = userDetails?.bio ?? "Bio"
                }
                if userDetails?.phone == "" {
                    headerView.phoneLbl.text = "Phone"
                } else {
                    headerView.phoneLbl.text = userDetails?.phone ?? "Phone"
                }
                if userDetails?.address == "" {
                    headerView.addressLbl.text = "Address"
                } else {
                    headerView.addressLbl.text = userDetails?.address ?? "Address"
                }
                
                headerView.emailLbl.text = userDetails?.email ?? "Email not provided"
                headerView.phoneVu.isUserInteractionEnabled = true
                headerView.emailVu.isUserInteractionEnabled = true
                headerView.addressLbl.isUserInteractionEnabled = true
                headerView.addToContactsBtn.isUserInteractionEnabled = true
                
            } else {
                if userDetails?.profileOn == 0 {
                    //                    headerView.bioLbl.text = "Profile is off"
                    //                    headerView.bioLbl.addBlurrEffect()
                    
                    headerView.phoneLbl.text = "Profile is off"
                    headerView.phoneLbl.addBlurrEffect()
                    
                    headerView.emailLbl.text = "Profile is off"
                    headerView.emailLbl.addBlurrEffect()
                    
                    headerView.addressLbl.text = "Profile is off"
                    headerView.addressLbl.addBlurrEffect()
                } else {
                    //                    headerView.bioLbl.text = "User not connected"
                    //                    headerView.bioLbl.addBlurrEffect()
                    
                    headerView.phoneLbl.text = "User not connected"
                    headerView.phoneLbl.addBlurrEffect()
                    
                    headerView.emailLbl.text = "User not connected"
                    headerView.emailLbl.addBlurrEffect()
                    
                    headerView.addressLbl.text = "User not connected"
                    headerView.addressLbl.addBlurrEffect()
                }
                
                headerView.addToContactsBtn.isUserInteractionEnabled = false
                headerView.phoneVu.isUserInteractionEnabled = false
                headerView.emailVu.isUserInteractionEnabled = false
                headerView.addressLbl.isUserInteractionEnabled = false
            }
            
            
            var title = connectBtnTitle.Connect.rawValue
            var txtColor = UIColor.black
            var bgColor = UIColor.white
            
            if isRequestedByMe && isRequestedByOther {
                title = connectBtnTitle.Connected.rawValue
                txtColor = UIColor.white
                bgColor = UIColor.black
            } else if isRequestedByMe && !isRequestedByOther {
                title = connectBtnTitle.Invited.rawValue
                //                txtColor = UIColor.white
                //                bgColor = UIColor.black
            } else if !isRequestedByMe && isRequestedByOther {
                title = connectBtnTitle.Accept.rawValue
                txtColor = UIColor.white
                bgColor = UIColor.black
            }
            
            
            headerView.connectBackBtn.backgroundColor = bgColor
            headerView.connectBackBtn.setTitleColor(txtColor, for: .normal)
            headerView.connectBackBtn.setTitle(title, for: .normal)
            

            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.addressTapped(_:)))
            headerView.addressLbl.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.phoneTapped(_:)))
            headerView.phoneVu.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.emailTapped(_:)))
            headerView.emailVu.addGestureRecognizer(tap3)
            
            headerView.connectBackBtn.addTarget(self, action: #selector(connectBackPressed(_:)), for: .touchUpInside)
            
            
            headerView.addToContactsBtn.addTarget(self, action: #selector(addToContacts(_:)), for: .touchUpInside)
            
            headerView.backBtn.addTarget(self, action: #selector(goBack(_:)), for: .touchUpInside)
            headerView.dismissBtn.addTarget(self, action: #selector(dismiss(_:)), for: .touchUpInside)
            
        default:
            assert(false, "Invalid element type")
        }
        return headerView
        
    }
    
    @objc func emailTapped(_ sender: UITapGestureRecognizer) {
        print("emailTapped")
        
        guard let txt = userDetails?.email, txt.lowercased() != "address".lowercased(), isValidEmail(txt) else {
            showAlert(title: AlertConstants.Alert, message: "Email not valid")
            return
        }
        
        if let url = URL(string: "mailto:\(txt)") {
            UIApplication.shared.open(url)
        }
        
    }
    
    @objc func addressTapped(_ sender: UITapGestureRecognizer) {
        print("addressTapped")
        guard let txt = userDetails?.address, txt.lowercased() != "address".lowercased() else {
            showAlert(title: AlertConstants.Alert, message: "Address not valid")
            return
        }
        
        UIApplication.shared.open(URL(string: "\(txt)")!)
    }
    
    @objc func phoneTapped(_ sender: UITapGestureRecognizer) {
        
        guard let phone = userDetails?.email, phone.lowercased() != "phone".lowercased() else {
            showAlert(title: AlertConstants.Alert, message: "Phone number not valid")
            return
        }
        
        let app = UIApplication.shared
        app.open(Applications.Phone(),action: .open(number: phone))
        
    }
    
    func showEmptyState(message: String) {
        emptyStateMessage.text = message
        collectionVu.addSubview(emptyStateMessage)
        emptyStateMessage.centerXAnchor.constraint(equalTo: collectionVu.centerXAnchor).isActive = true
        emptyStateMessage.centerYAnchor.constraint(equalTo: collectionVu.centerYAnchor,constant: 100).isActive = true
    }
    
    func hideEmptyState() {
        emptyStateMessage.removeFromSuperview()
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.dismiss(animated: true) {
            //            self.showAlert(title: AlertConstants.Success, message: "Contact saved.")
        }
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: {})
    }
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
}

func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}

extension UILabel {
    func addBlurrEffect() {
        let blurEffectView = ASBlurEffectView() // creating a blur effect view
        blurEffectView.intensity = 1 // setting blur intensity from 0.1 to 10
        self.addSubview(blurEffectView) // adding blur effect view as a subview to your view in which you want to use
    }
    
    func removeBlurEffect() {
        for subview in self.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
}
