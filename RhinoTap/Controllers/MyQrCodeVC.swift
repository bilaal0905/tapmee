

import UIKit
import FirebaseStorage
import EFQRCode

class MyQrCodeVC: BaseClass {
    
    var userName = ""
    var userId  = ""
    let storage = Storage.storage()

//    @IBOutlet weak var profileImg: UIImageView! {
//        didSet {
//            profileImg.layer.cornerRadius = profileImg.bounds.height/2
//        }
//    }
    @IBOutlet weak var linkLbl: UILabel!
    @IBOutlet weak var qrImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getbaseUrl()
    }
    
    func getbaseUrl () {

        APIManager.getBaseUrl(){ url in
            if url != "" {
                
                baseUrl = url!
                self.userId = self.getUserId()
                
                let link = baseUrl + self.userId
                self.linkLbl.text = link
//                self.qrImage.image = self.generateQRCode(from: link)
                
                if let image = EFQRCode.generate(
                    for: link,
                    watermark: UIImage(named: "icon2")?.cgImage
                ) {
                    print("Create QRCode image success \(image)")
                    self.qrImage.image = UIImage(cgImage: image)
                } else {
                    print("Create QRCode image failed!")
                }


            }
        } err: { error in
            print(error)
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        self.view.addBackground()
        
        var ref = storage.reference()
        let link = getProfileUrl()
        if link != ""  {
            ref = storage.reference(forURL: link)

            if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                //profileImg.image = cacheImg
            } else {
                ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    if error != nil {
                        // Uh-oh, an error occurred!
                    } else {
                        
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                          //  self.profileImg.image = img
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func shareProfileLink(_ sender: Any) {
        
        let shareText = URL(string: baseUrl + userId)
        
        let vc = UIActivityViewController(activityItems: [shareText!], applicationActivities: [])
        present(vc, animated: true, completion: nil)
    }
}
