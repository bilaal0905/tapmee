//
//  EditProfileVC.swift
//  Tapmee
//
//  Created by Apple on 09/04/2022.
//

import UIKit
import CropViewController

import FirebaseStorage

class EditProfileVC: BaseClass {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var coverImgVu: UIImageView!
    @IBOutlet weak var userNameTF: UITextField!{
        didSet {
            let blackPlaceholderText = NSAttributedString(string: "Username".localize(),
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            userNameTF.attributedPlaceholder = blackPlaceholderText
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: userNameTF.frame.height))
            userNameTF.leftView = paddingView
            userNameTF.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var bioTextView: UITextView!
    {
        didSet {
            //            let blackPlaceholderText = NSAttributedString(string: "Bio",
            //                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            //            bioTextView.attributedPlaceholder = blackPlaceholderText
            
            // let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: bioTextView.frame.height))
            bioTextView.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
            // bioTextView.leftViewMode = .always
        }
    }
    

    @IBOutlet weak var removeProfilePicBtn: UIButton!
    
    @IBOutlet weak var removeCoverBtn: UIButton! {
        didSet {
            removeCoverBtn.contentMode = .scaleAspectFit
        }
    }
    
    var userDetails: User?
    let storage = Storage.storage()
    var profilePicture = UIImage(named: "addImage-placeholder")
    var coverPicture = UIImage()
    var isCover = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bioTextView.delegate = self
        self.view.addBackground()
        self.initialViewSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: Initial View Setup
    func initialViewSetup() {
        userDetails = readUserData()
        // text field setup
        self.userNameTF.text = userDetails?.name
        if let bio = userDetails?.bio, bio != "", bio != " " {
            self.bioTextView.text = bio
        }
        else {
            self.bioTextView.text = "Bio"
        }
        
        //Remove Buttons
        removeProfilePicBtn.isHidden = true
        //removeCoverBtn.isHidden = true
        removeCoverBtn.setImage(UIImage(named: "addImage-placeholder"), for: .normal)
        removeCoverBtn.backgroundColor = .clear
        removeCoverBtn.layer.borderColor = UIColor.clear.cgColor
        //Profile image setup
        if let profileUrl = userDetails?.profileUrl, profileUrl != "" {
            downloadImage(from: profileUrl)
        }
        
        let addProfileGesture = UITapGestureRecognizer(target: self, action: #selector(self.profileImageVuTapped))
        profileImageView.isUserInteractionEnabled = true
        addProfileGesture.numberOfTapsRequired = 1
        addProfileGesture.delaysTouchesBegan = true
        profileImageView.addGestureRecognizer(addProfileGesture)
        
        //Cover Photo setup
        let addcoverGesture = UITapGestureRecognizer(target: self, action: #selector(self.coverImageVuTapped))
        coverImgVu.isUserInteractionEnabled = true
        addcoverGesture.numberOfTapsRequired = 1
        addcoverGesture.delaysTouchesBegan = true
        coverImgVu.addGestureRecognizer(addcoverGesture)

        let coverUrl = userDetails?.coverUrl ?? ""
        
        DispatchQueue.main.async { [self] in
            var ref = storage.reference()
            
            if coverUrl != "" {
                ref = storage.reference(forURL: coverUrl)
                ref.getData(maxSize: 1 * 1024 * 8024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                        // Uh-oh, an error occurred!
                    } else {
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                            self.coverImgVu.image = img
                           // self.removeCoverBtn.isHidden = false
                            removeCoverBtn.setImage(UIImage(named: "minus-button"), for: .normal)
                            removeCoverBtn.backgroundColor = .black
                            removeCoverBtn.layer.borderColor = UIColor.black.cgColor

                        }
                    }
                }
            }
        }
    }
    
    func downloadImage(from url: String) {
        
        let ref = storage.reference(forURL: url)
        
        DispatchQueue.main.async() { [weak self] in
            
            if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                self!.profileImageView.image = cacheImg
                self?.profilePicture = cacheImg
                self!.removeProfilePicBtn.isHidden = false
            } else {
                ref.getData(maxSize: 1 * 1024 * 8024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                            self!.profileImageView.image = img
                            self!.removeProfilePicBtn.isHidden = false

                        }
                    }
                }
            }
        }
    }
    
    //MARK: Crop View Controller
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            picker.dismiss(animated: true) {
                self.openImageCropper(image: image)
            }
        }
    }
    override func openImageCropper(image: UIImage) {
        if isCover {
            let cropBackGroundImageViewCircular = CropViewController(image: image)
            cropBackGroundImageViewCircular.aspectRatioPreset = .preset16x9
            cropBackGroundImageViewCircular.toolbar.clampButtonHidden = true
            cropBackGroundImageViewCircular.doneButtonColor = UIColor.white
            cropBackGroundImageViewCircular.cancelButtonColor = UIColor.red
            cropBackGroundImageViewCircular.aspectRatioLockEnabled = true
            cropBackGroundImageViewCircular.resetAspectRatioEnabled = false
            cropBackGroundImageViewCircular.delegate = self
            present(cropBackGroundImageViewCircular, animated: true, completion: nil)

        } else {
            let cropViewController = CropViewController(image: image)
            cropViewController.aspectRatioPreset = .presetSquare
            cropViewController.toolbar.clampButtonHidden = true
            cropViewController.doneButtonColor = UIColor.white
            cropViewController.cancelButtonColor = UIColor.red
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.resetAspectRatioEnabled = false
            cropViewController.delegate = self
            present(cropViewController, animated: true, completion: nil)
        }
    }
    
    override func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        var storageRef = storage.reference()
        if isCover {
            coverImgVu.image = image
            removeCoverBtn.setImage(UIImage(named: "minus-button"), for: .normal)
            removeCoverBtn.backgroundColor = .black
            removeCoverBtn.layer.borderColor = UIColor.black.cgColor
            storageRef = storage.reference().child("coverPic:\(getUserId()).png")
            
        } else {
            profileImageView.image = image
            self.removeProfilePicBtn.isHidden = false

            storageRef = storage.reference().child("profilePic:\(getUserId()).png")
            imgChache.setObject(image, forKey: storageRef)
        }
 
        startLoading()
        self.coverImgVu.isUserInteractionEnabled = false
        self.profileImageView.isUserInteractionEnabled = false
        if let uploadData = image.jpegData(compressionQuality: 0.5) {
            storageRef.putData(uploadData, metadata: nil) { [self] (metadata, error) in
                if error != nil {
                    self.coverImgVu.isUserInteractionEnabled = true
                    self.profileImageView.isUserInteractionEnabled = true
                    self.stopLoading()
                    self.showAlert(title: AlertConstants.Error, message: error?.localizedDescription ?? AlertConstants.SomeThingWrong)
                    print("error")
                } else {
                    var dict = [String:String]()
                    
                    if isCover {
                        let coverUrl = "\(storageRef)"
                        userDetails?.coverUrl = coverUrl
                        dict = ["id":getUserId(),
                                "coverUrl":coverUrl]
                    } else {
                        let profileUrl = "\(storageRef)"
                        userDetails?.profileUrl = profileUrl
                        dict = ["id":getUserId(),
                                "profileUrl":profileUrl]
                        NotificationCenter.default.post(name: Constants.profileUpdateNotif, object: nil, userInfo: ["url":profileUrl])
                    }
                    
                    APIManager.updateUserProfile(dict) { [self] (success) in
                        saveUserToDefaults(userDetails!)
                        self.coverImgVu.isUserInteractionEnabled = true
                        self.profileImageView.isUserInteractionEnabled = true
                        stopLoading()
                        print("Saved profile with url",dict)
                    }
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Imageviews tap methods
    @objc func profileImageVuTapped() {
        resignFirstResponder()
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            isCover = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func coverImageVuTapped(){
        
        resignFirstResponder()
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            isCover = true
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK: Update Profile
    func updateUserProfile() {
        
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        
        if userNameTF.text == userDetails?.name && bioTextView.text == userDetails?.bio {
            showAlert(title: AlertConstants.Alert, message: AlertConstants.profileAlreadyUpdatedAlert)
            return
        }
        
        if userNameTF.text == "" || bioTextView.text == "" {
            showAlert(title: AlertConstants.Alert, message: AlertConstants.AllFieldNotFilled)
            return
        }
        
        startLoading()
        
        let param : [String:String?] = [
            "id" : getUserId(),
            "name": userNameTF.text,
            "bio": bioTextView.text,
        ]
        
        APIManager.updateUserProfile(param as APIManager.dictionaryParameter) { [self] (success) in
            self.stopLoading()
            if success {
                userDetails = User(id: userDetails?.id,
                                   name:  userNameTF.text,
                                   email: getEmail(),
                                   fcmToken: getFCM(),
                                   profileUrl: userDetails?.profileUrl,
                                   bio: bioTextView.text,
                                   username: userDetails?.username,
                                   phone: userDetails?.phone,
                                   gender: userDetails?.gender,
                                   dob: userDetails?.dob,
                                   address: userDetails?.address,
                                   platform: userDetails?.platform,
                                   timestamp: userDetails?.timestamp)
                
                NotificationCenter.default.post(name: Constants.profileUpdateNotif, object: nil, userInfo: ["name": userDetails?.name ?? (userDetails?.username)!,"url":userDetails?.profileUrl ?? ""])
                
                self.saveUserToDefaults(userDetails!)
                
                self.showAlert(title: AlertConstants.Success, message: "Profile updated successfully") {
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.showAlert(title: AlertConstants.Error, message: "Profile could not be updated")
            }
        }
    }
    
    //MARK: Remove Cover Photo
    func removeCoverPhoto() {
        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.removeAlert) { [self] in
            
            self.coverImgVu.image = UIImage()
            //self.removeCoverBtn.isHidden = true
            removeCoverBtn.setImage(UIImage(named: "addImage-placeholder"), for: .normal)
            removeCoverBtn.backgroundColor = .clear
            removeCoverBtn.layer.borderColor = UIColor.clear.cgColor
            userDetails?.coverUrl = ""
            let dict = ["id":getUserId(),
                    "coverUrl":""]
            startLoading()
            APIManager.updateUserProfile(dict) { [self] (success) in
                saveUserToDefaults(userDetails!)
                stopLoading()
                print("Saved cover image with url",dict)
            }
        }
    }
    //MARK: Button Actions
    @IBAction func saveBtn(_ sender: UIButton) {
        updateUserProfile()
    }
    
    @IBAction func closeBtn(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"TabBarController"])
    }
    
    @IBAction func removeProfilePicBtn(_ sender: Any) {
        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.removeAlert) { [self] in
            
            self.profileImageView.image = UIImage(named: "addImage-placeholder")
            self.removeProfilePicBtn.isHidden = true
            
            var storageRef = storage.reference()
            storageRef = storage.reference().child("logoImg:\(getUserId()).png")
            //imgChache.setObject(logoPic!, forKey: storageRef)
            imgChache.removeObject(forKey: storageRef)
            
            userDetails?.profileUrl = ""
            let dict = ["id":getUserId(),
                    "profileUrl":""]
            startLoading()
            APIManager.updateUserProfile(dict) { [self] (success) in
                saveUserToDefaults(userDetails!)
                stopLoading()
                print("Saved profile pic with url",dict)
            }
        }
    }
    
    @IBAction func removeCoverBtn(_ sender: UIButton) {
        
        if removeCoverBtn.image(for: .normal) == UIImage(named: "minus-button")
        {
            self.removeCoverPhoto()
        }
        else {
            self.coverImageVuTapped()
        }
    }
}

extension EditProfileVC: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Bio" {
            textView.text = ""
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Bio"
        }
    }
}
