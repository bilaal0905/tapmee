

import Foundation


struct AppleInfoModel {
    let userid:String
    let email:String
    let firstName:String
    let lastName:String
    let fullName:String
}
