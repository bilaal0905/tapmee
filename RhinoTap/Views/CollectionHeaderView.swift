

import UIKit

class CollectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var imgVu: UIImageView! {
        didSet {
            imgVu.layer.cornerRadius = imgVu.frame.height/2
        }
    }
    
    @IBOutlet weak var coverImgVu: UIImageView!
    
//    @IBOutlet weak var logoImgVu: UIImageView! {
//        didSet {
//            logoImgVu.layer.cornerRadius = logoImgVu.frame.height/2
//        }
//    }

    //@IBOutlet weak var leadCapture_toggleBtn: UISwitch!
    @IBOutlet weak var direct_toggleBtn: UISwitch!
    
    
   // @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
   // @IBOutlet weak var emailLbl: UILabel!
    //@IBOutlet weak var tickIcon: UIImageView!
    @IBOutlet weak var editProfileBtn: UIButton!
    
    
//    @IBOutlet weak var containerView: UIView! { didSet {
//        containerView.layer.cornerRadius = 25
//        layer.backgroundColor = UIColor.clear.cgColor
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
//        layer.shadowOpacity = 0.4
//        layer.shadowRadius = 4.0
//
//        containerView.layer.masksToBounds = true
//    }}
    
}

