//
//  EditLinkVC.swift
//  Tapmee
//
//  Created by Apple on 11/04/2022.
//

import UIKit
class EditLinkVC: BaseClass {
    
    @IBOutlet weak var linkImageVu: UIImageView!
    
    @IBOutlet weak var linkNameLbl: UILabel!
    @IBOutlet weak var linkValueTF: UITextField!{
        didSet {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: linkValueTF.frame.height))
            linkValueTF.leftView = paddingView
            linkValueTF.leftViewMode = .always
        }
    }
    @IBOutlet weak var linkHintLbl: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    var linkSelected: Links?
    var delegate: LinkAdded?
    var linkHintText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.addBackground()
        
        initialViewSetup()
        linkValueTF.delegate = self
        
    }
    
    func splitString(_ inputString: String, _ url: String) {
        let splits = inputString.components(separatedBy: url)
        if splits.count > 1 {
            self.linkValueTF.text = splits[1]
        } else {
            self.linkValueTF.text = ""
        }
    }
    
    //MARK: Initial View Setup
    func initialViewSetup() {
        
        linkNameLbl.text = linkSelected?.name!.localize()
        linkImageVu.image = UIImage(named: linkSelected?.name?.lowercased() ?? "")
        
        setLinkFieldPlaceholder()
        
        if let linkValue = linkSelected?.value, linkValue != "" {
            setLabelAndFieldForEditLink()
        }
        else {
            setLabelAndFieldForNewLink()
        }
    }
    func setLabelAndFieldForEditLink() {
        
        linkHintLbl.text = linkSelected?.value ?? ""
        linkValueTF.text = linkSelected?.value ?? ""
        switch linkSelected?.name!.lowercased() {
            case "facebook":
                self.splitString(linkSelected?.value ?? "", "https://facebook.com/")
            case "instagram":
                self.splitString(linkSelected?.value ?? "", "https://www.instagram.com/")
            case "linkedin":
                self.splitString(linkSelected?.value ?? "", "https://linkedin.com/in/")
            case "snapchat":
                self.splitString(linkSelected?.value ?? "", "https://www.snapchat.com/add/")
            case "telegram":
                self.splitString(linkSelected?.value ?? "", "https://t.me/")
            case "tiktok":
                self.splitString(linkSelected?.value ?? "", "https://www.tiktok.com/@")
            case "twitter":
                self.splitString(linkSelected?.value ?? "", "https://www.twitter.com/")
            case "vimeo":
                self.splitString(linkSelected?.value ?? "", "https://vimeo.com/")
            case "whatsapp":
                self.splitString(linkSelected?.value ?? "", "https://api.whatsapp.com/send?phone=")
                //case "youtube":
                //    self.splitString(linkSelected?.value ?? "", "https://www.youtube.com/")
            case "paypal":
                self.splitString(linkSelected?.value ?? "", "https://www.paypal.me/")
            case "pinterest":
                self.splitString(linkSelected?.value ?? "", "https://pinterest.com/")
            case "spotify":
                self.splitString(linkSelected?.value ?? "", "https://open.spotify.com/")
            case "email":
                self.splitString(linkSelected?.value ?? "", "mailto:")
                //linkHintLbl.text = "mailto:" + (linkSelected?.value)!
            case "contact card","phone":
                self.splitString(linkSelected?.value ?? "", "tel://")
                //linkHintLbl.text = "tel://" + (linkSelected?.value)!
            case "text":
                self.splitString(linkSelected?.value ?? "", "sms:")
                //linkHintLbl.text = "sms:" + (linkSelected?.value)!
            case "calendly", "website", "custom link", "custom link 1", "paylah", "reddit","messenger","menu", "catalogo","my pet","youtube":
                linkValueTF.text = linkSelected?.value!
                
            default:
                linkHintLbl.text = linkSelected?.value ?? ""
                linkValueTF.text = linkSelected?.value ?? ""
        }
        linkHintText = linkHintLbl.text ?? ""
    }
    
    func setLabelAndFieldForNewLink() {
        linkValueTF.text = ""
        switch linkSelected?.name!.lowercased() {
            case "contact card","phone":
                linkHintLbl.text = "tel://"
            case "facebook":
                linkHintLbl.text = "https://facebook.com/"
            case "instagram":
                linkHintLbl.text = "https://www.instagram.com/"
            case "linkedin":
                linkHintLbl.text = "https://linkedin.com/in/"
            case "snapchat":
                linkHintLbl.text = "https://www.snapchat.com/add/"
            case "telegram":
                linkHintLbl.text = "https://t.me/"
            case "text":
                linkHintLbl.text = "sms:"
            case "tiktok":
                linkHintLbl.text = "https://www.tiktok.com/@"
            case "twitter":
                linkHintLbl.text = "https://www.twitter.com/"
            case "vimeo":
                linkHintLbl.text = "https://vimeo.com/"
            case "whatsapp":
                linkHintLbl.text = "https://api.whatsapp.com/send?phone="
                //case "youtube":
                //  linkHintLbl.text = "https://www.youtube.com/"
            case "email":
                linkHintLbl.text = "mailto:"
            case "paypal":
                linkHintLbl.text = "https://www.paypal.me/"
            case "pinterest":
                linkHintLbl.text = "https://pinterest.com/"
            case "spotify":
                linkHintLbl.text = "https://open.spotify.com/"
            case "calendly","website", "custom link", "custom link 1", "paylah", "reddit", "messenger","menu", "catalogo","my pet","youtube":
                linkHintLbl.text = "https://www.site.com"
            default:
                linkHintLbl.text = ""
                
        }
        linkHintText = linkHintLbl.text ?? ""
    }
    
    func setLinkFieldPlaceholder() {
        switch linkSelected?.name?.lowercased() ?? "" {
            case "whatsapp":
                linkValueTF.placeholder = "Phone Number"
            case "linkedin":
                linkValueTF.placeholder = "LinkedIn Profile Link"
            case "twitter","twitch","telegram","instagram":
                linkValueTF.placeholder = "Username"
            case "youtube":
                linkValueTF.placeholder = "Youtube profile link"
            case "facebook":
                linkValueTF.placeholder = "Facebook profile link"
                break
            case "pinterest":
                linkValueTF.placeholder = "Pintrest profile link"
                break
            case "vimeo":
                linkValueTF.placeholder = "link"
            case "reddit","snapchat","tiktok":
                linkValueTF.placeholder = "Username"
            case "email":
                linkValueTF.placeholder = "Email"
            case "spotify":
                linkValueTF.placeholder = "Album or Artist"
            case "paypal":
                linkValueTF.placeholder = "PayPal.me link"
            case "custom link","website","custom link 1","menu", "catalogo","my pet":
                linkValueTF.placeholder = "https://www.example.com/"
            case "contact card","text","phone":
                linkValueTF.placeholder = "Phone Number"
            case "paylah":
                linkValueTF.placeholder = "Paylah Profile Url"
            case "calendly":
                linkValueTF.placeholder = "Calendly Url"
                
            default:
                linkValueTF.placeholder = linkSelected?.name
        }
    }
    
    func deleteLink() {
        
        startLoading()
        userLinks = userLinks?.filter {$0.name != linkSelected?.name}
        
        startLoading()
        APIManager.updateUserLinks() { [self] (success) in
            stopLoading()
            if success {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showAlert(title: AlertConstants.Error, message: "Link could not be deleted.")
            }
            
        }
    }
    //MARK: Button Actions
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func deleteBtn(_ sender: UIButton) {
        if let linkValue = linkSelected?.value, linkValue != "" {
            
            let deleteAlert = UIAlertController(title: AlertConstants.Alert, message: AlertConstants.deleteAlert, preferredStyle: UIAlertController.Style.alert)
            deleteAlert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { (action: UIAlertAction!) in
                self.deleteLink()
            }))
            
            deleteAlert.addAction(UIAlertAction(title: "No".localized, style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(deleteAlert, animated: true, completion: nil)
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func saveBtn(_ sender: UIButton) {
        
        //        guard sender.title(for: .normal) == "Save".localized  else {
        //            return
        //        }
        let t = linkValueTF.text
        guard let text = t?.removeWhitespace() ,!text.isEmpty else {
            showAlert(title: AlertConstants.Alert, message: AlertConstants.emptyFieldAlert)
            return
        }
        
        var found = false
        linkSelected?.value = linkHintLbl.text
        //        switch linkSelected?.name!.lowercased() {
        //        case     "whatsapp","linkedin","twitter","twitch","telegram","instagram","youtube","pinterest","facebook","vimeo","snapchat","tiktok","spotify","paypal":
        //            linkSelected?.value = linkHintLbl.text
        //
        //        case "email","text","contact card","calendly", "website", "custom link", "custom link 1", "paylah", "reddit","messenger" :
        //            linkSelected?.value = linkValueTF.text
        //        default:
        //            linkSelected?.value = linkHintLbl.text
        //        }
        for i in 0..<(userLinks ?? []).count {
            if userLinks?[i].name == linkSelected?.name {
                userLinks?[i] = linkSelected!
                found = true
                break
            }
        }
        
        if !found {
            userLinks?.append(linkSelected!)
        }
        startLoading()
        APIManager.updateUserLinks() { [self] (success) in
            self.stopLoading()
            if !success {
                self.showAlert(title: AlertConstants.Error, message: "User Link Could not be saved, please try again.")
                return
            }
            else {
                delegate?.linkReceived(text: (linkSelected?.value)!,platform: linkSelected?.name ?? "",imageUrl: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

//MARK: Textfield Delegates

extension EditLinkVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            switch linkSelected?.name!.lowercased() {
                case "calendly", "website", "custom link", "custom link 1", "paylah", "reddit", "messenger","menu", "catalogo","my pet","youtube":
                    if updatedText != "" {
                        self.linkHintLbl.text = updatedText
                    }
                    else {
                        self.linkHintLbl.text = "https://www.site.com"
                    }
                case "contact card","phone":
                    linkHintLbl.text = "tel://" + updatedText
                case "facebook":
                    linkHintLbl.text = "https://facebook.com/" + updatedText
                case "instagram":
                    linkHintLbl.text = "https://www.instagram.com/" + updatedText
                case "linkedin":
                    linkHintLbl.text = "https://linkedin.com/in/" + updatedText
                case "snapchat":
                    linkHintLbl.text = "https://www.snapchat.com/add/" + updatedText
                case "telegram":
                    linkHintLbl.text = "https://t.me/" + updatedText
                case "text":
                    linkHintLbl.text = "sms:" + updatedText
                case "tiktok":
                    linkHintLbl.text = "https://www.tiktok.com/@" + updatedText
                case "twitter":
                    linkHintLbl.text = "https://www.twitter.com/" + updatedText
                case "vimeo":
                    linkHintLbl.text = "https://vimeo.com/" + updatedText
                case "whatsapp":
                    linkHintLbl.text = "https://api.whatsapp.com/send?phone=" + updatedText
                    //            case "youtube":
                    //                linkHintLbl.text = /*"https://www.youtube.com/" + */ updatedText
                case "email":
                    linkHintLbl.text = "mailto:" + updatedText
                case "paypal":
                    linkHintLbl.text = "https://www.paypal.me/" + updatedText
                case "pinterest":
                    linkHintLbl.text = "https://pinterest.com/" + updatedText
                case "spotify":
                    linkHintLbl.text = "https://open.spotify.com/" + updatedText
                default:
                    if updatedText != "" {
                        self.linkHintLbl.text = updatedText
                    }
                    else {
                        self.linkHintLbl.text = ""
                    }
                    self.linkHintLbl.text = linkHintText + updatedText
            }
            
        }
        return true
    }
}
