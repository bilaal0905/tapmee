
import UIKit
import FirebaseAuth

class SignInVC: BaseClass {
    
    @IBOutlet weak var emailTF: UITextField! {
    didSet {
           let blackPlaceholderText = NSAttributedString(string: "Email",
                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
           
        emailTF.attributedPlaceholder = blackPlaceholderText
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: emailTF.frame.height))
        emailTF.leftView = paddingView
        emailTF.leftViewMode = .always
       }
    }
    @IBOutlet weak var pwTF: UITextField!
    {
    didSet {
           let blackPlaceholderText = NSAttributedString(string: "Password",
                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
           
        pwTF.attributedPlaceholder = blackPlaceholderText
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: pwTF.frame.height))
        pwTF.leftView = paddingView
        pwTF.leftViewMode = .always
       }
    }
    @IBOutlet weak var forgotPwBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0, weight: .medium),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    var attributedString = NSMutableAttributedString(string:"")
    
    
    var timestamp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.timestamp = format.string(from: date)
        
        self.view.addBackground()
    }

    //MARK: Button Actions
    @IBAction func forgotPwPressed(_ sender: Any) {

        pushController(controller: ForgotPwVC.id, storyboard: "Main")
    }
    
    
    
    @IBAction func signInPressed(_ sender: Any) {
        
        resignFirstResponder()
        
        guard let email = emailTF.text, !email.isEmpty,
              let password = pwTF.text, !password.isEmpty else {
            showAlert(title: AlertConstants.Error, message: AlertConstants.AllFieldNotFilled)
            return
        }
        
        startLoading()
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
            
            guard error == nil else {
                self?.stopLoading()
                self?.showAlert(title: AlertConstants.Error , message: error?.localizedDescription ?? AlertConstants.emailPasswordInvalid)
                return
            }
            
            APIManager.getUserData(id: (authResult?.user.uid)!) { [self] (user) in
                guard user != nil else {
                    self?.stopLoading()
                    self?.showAlert(title: AlertConstants.Error, message: "Could not find user data.".localize())
                    return
                }
                
                //TODO: if user isBlocked then we'll show alert and user will not move into app
                if user?.isDeleted == true {
                    self?.stopLoading()
                    self?.showAlert(title: AlertConstants.Alert, message: "Your Account is locked, contact to admin".localize())
                } else {
                    
//                    if user?.platform != "ios" {
//                        //MARK: update to firebase timestamp and platform
//                        if let _timestamp = self?.timestamp {
//                            
//                            let param : [String:String?] = [
//                                
//                                "timestamp": user?.timestamp == "" ? _timestamp : user?.timestamp,
//                                "platform": user?.platform != "ios" ? "ios" : "ios",
//                                "id" : self?.getUserId()
//                            ]
//                            
//                            APIManager.updateUserProfile(param as APIManager.dictionaryParameter) { [self] (success) in
//                            
//                                if success {
//                                    print("----Updated----")
//                                }
//                            }
//
//                        }
//                    }
                    
                    self?.saveUserToDefaults(user!)
                    self?.stopLoading()
                    
                    UserDefaults.standard.setValue(true, forKey: Constants.status)
                    print("Saved Data to user defaults",user as Any)
                    
                    self?.performSegue(withIdentifier: "Main", sender: self)

//                    //TODO: If tag id is present then move to next
//                    if user?.tagUid != "" && user?.tagUid != nil {
//                        self?.performSegue(withIdentifier: "Main", sender: self)
//                    } else {
//                        let controller = self?.getControllerRef(controller: TagVerificationViewController.id, storyboard: Storyboards.Main.id) as! TagVerificationViewController
//                        self?.navigationController?.pushViewController(controller, animated: true)
//                    }
                }
                
                
            } error: { (desc) in
                self?.stopLoading()
                self?.showAlert(title: AlertConstants.Error, message: desc)
            }
        }        
    }
    @IBAction func signUpBtnPressed(_ sender: Any) {
//        pushController(controller: SignUpVC.id, storyboard: "Main")
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension UIView {
    func addBackground() {
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = UIImage(named: "background")
        
        // you can change the content mode:
        imageViewBackground.contentMode = .scaleToFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
    }
}
