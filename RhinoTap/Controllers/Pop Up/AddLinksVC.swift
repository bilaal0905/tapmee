

import UIKit
import CropViewController
import iOSDropDown
import FirebaseStorage
import CodableFirebase
import SwiftGifOrigin
import Localize
class EditProfileCell: UICollectionViewCell {
    @IBOutlet weak var outerVu: UIView!
    @IBOutlet weak var imgVu: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var checkMark: UIImageView!{didSet{
        checkMark.image = UIImage(named: "checked")
    }}
}

class AddLinksVC: BaseClass,TextBackProtocol,getDate,LinkAdded,LinkDeleted {
    
    
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var collectionVu: UICollectionView!
    
    var linkChanged = false
    var isLogo = false
    
    var isLogoRemoved = false
    var isProfilePicRemoved = false
    
    var index = -1
    var headerIndex : IndexPath = IndexPath(item: 0, section: 0)
    var profilePicture = UIImage(named: "user_black")
    var logoPic = UIImage(named: "img")
    
    var headerHeight : CGFloat = 390.0
    var headerTotalHeight : CGFloat = 390.0
    var lblHeight : CGFloat = 00.0
    var bioText = "Bio"
    var profileUrl = ""
    var logoUrl = ""
    
    var nameChanged = false
    var newName = ""
    var bioChanged = false
    //    var newBioText = ""
    var profileChanged = false
    var newProfile = UIImage(named: "user_black")
    var dobChanged = false
    var genderChanged = false
    var newDob = "Date of Birth"
    var newGender = ""
    var phoneChanged = false
    var newPhone = ""
    var addressChanged = false
    var newAddress = ""
    
    var isProfilePic = false
    var isLogoPic = false
    
    var userDetails : User?
    let storage = Storage.storage()
    
    //==========================================
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionVu.delegate = self
        collectionVu.dataSource = self
        
        titlelbl.text = "Add Social Links".localize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setProfile()
        getAllLinks()
        self.view.addBackground()
    }
    
    // MARK: Delegates Received
    //==========================================
    
    func linkReceived(text: String,platform: String, imageUrl: String?) {
        
        if userLinks == nil {
            userLinks = []
        }
        userAllLinks?[index].value = text
        userAllLinks?[index].shareable = true
        userAllLinks?[index].name = platform
        if let imageUrl = imageUrl, imageUrl != "" {
            userAllLinks?[index].image = imageUrl
        }
//        APIManager.updateUserLinks() { (success) in
//            if success {
//                print("custom Logo Saved")
//            }
//
//        }
        var found = false
        for i in 0..<(userLinks ?? []).count {
            if userLinks?[i].name == userAllLinks?[index].name {
                userLinks?[i] = (userAllLinks?[index])!
                found = true
                break
            }
        }
        
        if !found {
            userLinks?.append((userAllLinks?[index])!)
        }
        linkChanged = true
        collectionVu.reloadData()
    }
    
    func deleteLink(platform: String) {
        
        for i in 0..<(userLinks ?? []).count {
            if userLinks?[i].name == userAllLinks?[index].name {
                userLinks?.remove(at: i)
                userAllLinks?[index].value = ""
                break
            }
        }
        
        print(userLinks as Any,"After Deletion")
        linkChanged = true
        collectionVu.reloadData()
    }
    
    func textReceived(text: String) {
        
        bioText = text
        bioChanged = true
        bioText = bioText.removingAllExtraNewLines
        let height = bioText.getStringHeight(constraintedWidth: collectionVu.frame.width - 20, font: UIFont.systemFont(ofSize: 17.0))
        
        headerTotalHeight = headerHeight + height
        
        collectionVu.reloadData()
    }
    
    func getSelectedDate(_ date: String) {
        newDob = date
        dobChanged = true
        collectionVu.reloadData()
        print(date)
    }
    
    // MARK: Prepare Segue
    //==========================================
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addText" {
            if let vc = segue.destination as? AddTextViewController {
                vc.delegate = self
                if bioChanged {
                    vc.bioText = bioText
                } else {
                    vc.bioText = userDetails?.bio ?? ""
                }
            }
        } else if segue.identifier == "showDate" {
            if let vc = segue.destination as? DateVC {
                vc.delegate = self
                if dobChanged {
                    vc.oldDate = newDob
                } else {
                    vc.oldDate = userDetails?.dob ?? ""
                }
            }
        } else if segue.identifier == "addNewLinks" {
            let vc = segue.destination as? InsertLinkVC
            
            let x = userAllLinks?[index].name?.capitalized
            vc?.text = userAllLinks?[index].value ?? ""
            vc?.img = userAllLinks?[index].image ?? ""
            vc?.indexPath = index
            if (index == 2) {
                vc?.platform = "Custom Link"
            }
            else{
                vc?.platform = x ?? "No Name"
            }
            
            vc?.deleteDelegate = self
            vc?.delegate = self
        }
    }
    
    // MARK: Ovveriden Methods
    //==========================================
    override func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        profileChanged = true
        var storageRef = storage.reference()
        if isLogo {
            logoPic = image
            storageRef = storage.reference().child("logoImg:\(getUserId()).png")
            //imgChache.setObject(image, forKey: storageRef)
            
        } else {
            profilePicture = image
            storageRef = storage.reference().child("profilePic:\(getUserId()).png")
            imgChache.setObject(image, forKey: storageRef)
        }
        
        startLoading()
        self.collectionVu.isUserInteractionEnabled = false
        if let uploadData = image.jpegData(compressionQuality: 0.5) {
            storageRef.putData(uploadData, metadata: nil) { [self] (metadata, error) in
                if error != nil {
                    self.collectionVu.isUserInteractionEnabled = true

                    self.stopLoading()
                    self.showAlert(title: AlertConstants.Error, message: error?.localizedDescription ?? AlertConstants.SomeThingWrong)
                    print("error")
                } else {
                    
                    var dict = [String:String]()
                    if isLogo {
                        logoUrl = "\(storageRef)"
                        userDetails?.coverUrl = logoUrl
                        dict = ["id":getUserId(),
                                "logoUrl":logoUrl]
                    } else {
                        profileUrl = "\(storageRef)"
                        userDetails?.profileUrl = profileUrl
                        dict = ["id":getUserId(),
                                "profileUrl":profileUrl]
                        NotificationCenter.default.post(name: Constants.profileUpdateNotif, object: nil, userInfo: ["url":profileUrl])
                    }
                    APIManager.updateUserProfile(dict) { [self] (success) in
                        self.collectionVu.isUserInteractionEnabled = true
                        saveUserToDefaults(userDetails!)
                        stopLoading()
                        print("Saved profile with url",dict)
                    }
                }
            }
        }
        
        collectionVu.reloadData()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: Selector Methods
    //==========================================
    
    @objc func lblTapped (sender: UITapGestureRecognizer){
        performSegue(withIdentifier: "addText", sender: self)
    }
    
    @objc func logoImgTapped(_ button:UIButton){
        
        resignFirstResponder()
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            isLogo = true
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @objc func imgVuTapped (sender: UITapGestureRecognizer){
        
        resignFirstResponder()
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            isLogo = false
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @objc func editFieldTapped (sender: UITapGestureRecognizer) {
        let header = collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: headerIndex) as? AddLinksReusableView
        guard let view = sender.view else {
            return
        }
        
        switch view {
        case header?.userNameVu:
            header?.nameTF.becomeFirstResponder()
        case header?.phoneVu:
            header?.phoneNumberTF.becomeFirstResponder()
        case header?.addressVu:
            header?.addressTF.becomeFirstResponder()
        default:
            print(sender.debugDescription)
        }
    }
    
    @objc final private func didEndEditing(textField: UITextField) {
        switch textField.tag {
        case 11:
            nameChanged = true
            newName = textField.text ?? "Name"
        case 22:
            phoneChanged = true
            newPhone = textField.text ?? "Phone"
        case 33:
            addressChanged = true
            newAddress = textField.text ?? "Address"
        default:
            break
        }
    }
    
    @objc final private func didBeginEditing(textField: UITextField) {
        switch textField.tag {
        case 11:
            newName = textField.text ?? ""
            textField.text = ""
        case 22:
            newPhone = textField.text ?? ""
            textField.text = ""
        case 33:
            newAddress = textField.text ?? ""
            textField.text = ""
        default:
            break
        }
    }
    
    @objc func editUsername(_ button:UIButton){
        
        resignFirstResponder()
        let header = self.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self.headerIndex)) as? AddLinksReusableView
        
        switch button.tag {
        case 1:
            header?.nameTF.becomeFirstResponder()
        case 2:
            header?.phoneNumberTF.becomeFirstResponder()
        case 3:
            header?.addressTF.becomeFirstResponder()
        case 4:
            performSegue(withIdentifier: "addText", sender: self)
        default:
            break
        }
        
    }
    
    @objc func showDateVu(_ button:UIButton){
        performSegue(withIdentifier: "showDate", sender: self)
    }
    
    // MARK: update Profile images
    
    func updateProfileImages() {
        //let image = UIImage(named: "user_black")
        profileChanged = true
        var dict = [String:String]()
        var storageRef = storage.reference()
        if isLogoRemoved {

            storageRef = storage.reference().child("logoImg:\(getUserId()).png")
            //imgChache.setObject(logoPic!, forKey: storageRef)
            logoUrl = ""
            imgChache.removeObject(forKey: storageRef)

            userDetails?.coverUrl = ""
            dict = ["id":getUserId(),
                    "logoUrl":""]
            
        } else {
            storageRef = storage.reference().child("profilePic:\(getUserId()).png")
            //imgChache.setObject(profilePicture!, forKey: storageRef)
            profileUrl = ""
            imgChache.removeObject(forKey: storageRef)

            userDetails?.profileUrl = ""
            dict = ["id":getUserId(),
                    "profileUrl":""]
            NotificationCenter.default.post(name: Constants.profileUpdateNotif, object: nil, userInfo: ["url":profileUrl])
        }
        startLoading()
        APIManager.updateUserProfile(dict) { [self] (success) in
            saveUserToDefaults(userDetails!)
            stopLoading()
            print("Saved profile with url",dict)
        }
    }
    
    
    // MARK: Button Actions
    //==========================================
    
    @objc func removeProfileTapped (_ button: UIButton) {
        profileUrl = ""
        profileChanged = true
        self.isLogoRemoved = false
        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.removeAlert) {
            self.profilePicture = UIImage(named: "user_black")
            
            let header = self.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self.headerIndex)) as? AddLinksReusableView
            header?.profileImg.image = UIImage(named: "user_black")
            header?.removeProfileImg.isHidden = true
            self.updateProfileImages()
        }
    }
    @objc func removeLogoTapped (_ button: UIButton) {
        logoUrl = ""
        profileChanged = true
        collectionVu.reloadData()
        self.isLogoRemoved = true
        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.removeAlert) {
            self.logoPic = UIImage(named: "img")
            let header = self.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self.headerIndex)) as? AddLinksReusableView
            header?.logoBtn.setImage(UIImage(named: "img"), for: UIControl.State.normal)
            header?.removeLogoBtn.isHidden = true
            self.updateProfileImages()

        }
    }
    

    @IBAction func saveBtnPressed(_ sender: Any) {
        
        //resignFirstResponder()
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        
        guard nameChanged || bioChanged || dobChanged || genderChanged || profileChanged || phoneChanged || addressChanged || linkChanged else {
            showAlert(title: AlertConstants.Alert, message: "Profile is already updated.")
            return
        }
        
        startLoading()
        if linkChanged {
            
            APIManager.updateUserLinks() { [self] (success) in
                self.stopLoading()
                if !success {
                    //collectionVu.reloadData()
                    self.showAlert(title: AlertConstants.Error, message: "User Link Could not be saved, please try again.")
                    return
                }
            }
        }
        
        
        guard let name = nameChanged ? newName : userDetails?.name, !name.isEmpty,
              let bio = bioChanged ? bioText : userDetails?.bio,
              let gender = genderChanged ? newGender : userDetails?.gender,
              let date = dobChanged ? newDob : userDetails?.dob,
              let phone = phoneChanged ? newPhone : userDetails?.phone,
              let address = addressChanged ? newAddress : userDetails?.address else {
                  self.stopLoading()
                  
                  showAlert(title: AlertConstants.Alert, message: AlertConstants.AllFieldNotFilled)
                  return
              }
        
        
        let param : [String:String?] = [
            "id" : getUserId(),
            "name": name,
            "bio": bio,
            "gender": gender,
            "dob": date,
            "phone": phone,
            "address": address,
            "profileUrl": profileUrl
        ]
        
        APIManager.updateUserProfile(param as APIManager.dictionaryParameter) { [self] (success) in
            self.stopLoading()
            if success {
                
                userDetails = User(id: getUserId(), name: name, email: getEmail(), fcmToken: getFCM(), profileUrl: profileUrl, bio: bio, username: getUsername(), phone: phone, gender: gender, dob: date, address: address,coverUrl: logoUrl)
                
                NotificationCenter.default.post(name: Constants.profileUpdateNotif, object: nil, userInfo: ["name": name,"url":profileUrl])
                
                self.saveUserToDefaults(userDetails!)
                
                profileChanged = false
                nameChanged = false
                bioChanged = false
                dobChanged = false
                genderChanged = false
                addressChanged = false
                phoneChanged = false
                
                self.showAlert(title: AlertConstants.Success, message: "Profile updated successfully") {
                    
                    NotificationCenter.default.post(name: Notification.Name("changeRootVC"),
                                    object: nil,
                                    userInfo: ["VCName":"DashBoardVC"])
                }
            } else {
                self.showAlert(title: AlertConstants.Error, message: "Profile could not be updated")
            }
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        profileChanged = false
        nameChanged = false
        bioChanged = false
        dobChanged = false
        genderChanged = false
        addressChanged = false
        phoneChanged = false
        linkChanged = false
        
        NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["VCName":"DashBoardVC"])
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Hide Remove buttons
    func setProfileRemoveButton(image: UIImage, header: AddLinksReusableView?) {
        if (image == UIImage(named: "user_black")) {
            header?.removeProfileImg.isHidden = true
        }
        else {
            header?.removeProfileImg.isHidden = false
        }
    }
    
    func setLogoRemoveButton(image: UIImage, header: AddLinksReusableView?) {
        if (image == UIImage(named: "img")) {
            header?.removeLogoBtn.isHidden = true
        }
        else {
            header?.removeLogoBtn.isHidden = false
        }
    }
    
    // MARK: Setters & Getters
    //==========================================
    
    func getAllLinks() {
        
        guard APIManager.isInternetAvailable() else {
            showAlert(title: AlertConstants.InternetNotReachable, message: "")
            return
        }
        
        startLoading()
        APIManager.getAllLinks() { [self] (data) in
            if data == nil {
                print("Nil Links are Returned")
            } else {
                var links = data
                
                links?.append(Links(linkID: 110, value: "", image: "", name: "Menu", shareable: false))
                links?.append(Links(linkID: 111, value: "", image: "", name: "Catalogo", shareable: false))
                links?.append(Links(linkID: 112, value: "", image: "", name: "My Pet", shareable: false))
                
                if let addIndex = links?.firstIndex(where: {$0.name == "Add New"}) {
                    links?.remove(at: addIndex)
                }
                
                print("Data Found in links",data as Any)
                for i in 0..<(links?.count ?? 0) {
                    for j in 0..<(userLinks?.count ?? 0) {
                        if links?[i].name == userLinks?[j].name{
                            links?[i].value = userLinks?[j].value
                            links?[i].name = userLinks?[j].name
                            links?[i].image = userLinks?[j].image
                        }
                    }
                }
                userAllLinks = links
                
            }
            
            DispatchQueue.main.async {
                collectionVu.reloadData()
                stopLoading()
            }
            
        } error: { [self] err in
            stopLoading()
            showAlert(title: AlertConstants.Error, message: err)
        }
    }
    
    func setProfile() {
        
        userDetails = readUserData()
        
        if let text = userDetails?.bio, text == "" || text.isEmpty {
            let height = "Bio".getStringHeight(constraintedWidth: collectionVu.frame.width , font: UIFont.systemFont(ofSize: 17.0))
            headerTotalHeight = headerHeight + (height)
            
        } else {
            let height = userDetails?.bio?.getStringHeight(constraintedWidth: collectionVu.frame.width , font: UIFont.systemFont(ofSize: 17.0))
            headerTotalHeight = headerHeight + (height ?? 20)
        }
        collectionVu.reloadData()
        profileUrl = userDetails?.profileUrl ?? ""
        logoUrl = userDetails?.coverUrl ?? ""
        
        //        var url = URL(string: profileUrl)
        
        DispatchQueue.main.async { [self] in
            var ref = storage.reference()
            let header = self.collectionVu.supplementaryView(forElementKind:  UICollectionView.elementKindSectionHeader, at: (self.headerIndex)) as? AddLinksReusableView
            
            if profileUrl != "" {
                ref = storage.reference(forURL: profileUrl)
                if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                    header?.profileImg.image = cacheImg
                    self.profilePicture = cacheImg
                    self.setProfileRemoveButton(image: cacheImg, header: header)
                } else {
                    ref.getData(maxSize: 1 * 1024 * 8024) { data, error in
                        if let error = error {
                            print(error.localizedDescription)
                            // Uh-oh, an error occurred!
                        } else {
                            if let img = UIImage(data: data!) {
                                imgChache.setObject(img, forKey: ref)
                                header?.profileImg.image = img
                                self.profilePicture = img
                                self.setProfileRemoveButton(image: img, header: header)
                            }
                        }
                    }
                }
            }
            
            if logoUrl != "" {
                
                ref = storage.reference(forURL: logoUrl)
                
                ref.getData(maxSize: 1 * 1024 * 8024) { data, error in
                    if let error = error {
                        print(error.localizedDescription)
                        // Uh-oh, an error occurred!
                    } else {
                        if let img = UIImage(data: data!) {
                            header?.logoBtn.setImage(img, for: .normal)
                            self.logoPic = img
                            self.setLogoRemoveButton(image: img, header: header)
                        }
                    }
                }
                
            }
        }
        
        //        profilePicture =  userProfileInDefault?["profileUrl"] as? String
    }
    
    func dropDownproperties(dropDown: DropDown, placeHolder: String, listArray: [String]) {
        dropDown.text = placeHolder
        dropDown.optionArray = listArray
        dropDown.arrowColor = .black
        dropDown.isSearchEnable = false
        dropDown.selectedRowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        dropDown.rowHeight = 50
        dropDown.listHeight = 200
    }
}


// MARK: Collection View Delegates
extension AddLinksVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditProfileCell", for: indexPath) as! EditProfileCell
        cell.contentView.frame = cell.bounds
        cell.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cell.outerVu.frame = cell.contentView.frame
        cell.imgVu.frame.size.width = cell.outerVu.frame.size.width
        cell.imgVu.frame.size.height = cell.outerVu.frame.size.height - 20
        cell.lbl.frame.origin.y = cell.imgVu.frame.size.height

        
        let link = userAllLinks?[indexPath.row]
        
        cell.lbl.text = ""
        cell.lbl.font = Constants.customFontRegular
        cell.imgVu.image = nil
        cell.imgVu.image = UIImage(named: link?.name?.lowercased() ?? "")
        cell.lbl.text = link?.name!.localize()
        if link?.value != "" && link?.value != nil {
            cell.checkMark.isHidden = false
        } else {
            cell.checkMark.isHidden = true
        }
        
        cell.imgVu.layer.masksToBounds = true
        cell.imgVu.layer.cornerRadius = cell.imgVu.frame.height/2
        
//        if link?.linkID == 4{
//            if link?.value != "" && link?.value != nil, let img = UIImage(contentsOfFile: self.customLogoImgUrl.path) {
//                cell.imgVu.contentMode = .scaleToFill
//                cell.imgVu.image = img
//                cell.lbl.text = link?.name
//            }
//            else {
//                cell.imgVu.loadGif(name: "1-Custom-Link")
//                
//                cell.lbl.text = "Custom Link"
//            }
//        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.bounds
        let width = (size.width / 3) - 30
        let height = width + 20
        return CGSize(width: width, height: height) // viewcollectionView
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        //performSegue(withIdentifier: "addNewLinks", sender: self)
        let addLinksVC = self.storyboard?.instantiateViewController(withIdentifier: "EditLinkVC") as! EditLinkVC
        addLinksVC.linkSelected = userAllLinks?[index]
        addLinksVC.delegate = self
        self.navigationController?.pushViewController(addLinksVC, animated: true)
        if #available(iOS 13.0, *) {
//            let vc = self.storyboard?.instantiateViewController(identifier: "InsertLinkVC") as! InsertLinkVC
//
//            let x = userAllLinks?[index].name?.capitalized
//            vc.text = userAllLinks?[index].value ?? ""
//            vc.img = userAllLinks?[index].image ?? ""
//            vc.selectedLink = userAllLinks?[index]
//            vc.indexPath = index
//            vc.platform = x ?? "No Name"
//
//            vc.deleteDelegate = self
//            vc.delegate = self
//            self.present(vc, animated: true, completion: nil)
            //            show(vc, sender: self)
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return userAllLinks?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.size.width, height: CGFloat(headerTotalHeight))
        return CGSize(width: 0  , height: 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddLinksReusableView", for: indexPath) as? AddLinksReusableView else {
                fatalError("Invalid view type")
            }
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            headerIndex = indexPath
            print(indexPath)
            let numberOfPostsViewSelector : Selector = #selector(self.imgVuTapped)
            let viewPostsViewGesture = UITapGestureRecognizer(target: self, action: numberOfPostsViewSelector)
            headerView.profileImg.isUserInteractionEnabled = true
            viewPostsViewGesture.numberOfTapsRequired = 1
            viewPostsViewGesture.delaysTouchesBegan = true
            headerView.profileImg.addGestureRecognizer(viewPostsViewGesture)
            headerView.logoBtn.layer.cornerRadius = headerView.logoBtn.bounds.height/2
            
            headerView.addressTF.addTarget(self, action: #selector(didEndEditing(textField:)), for: .editingDidEnd)
            headerView.addressTF.addTarget(self, action: #selector(didBeginEditing(textField:)), for: .editingDidBegin)
            
            headerView.nameTF.addTarget(self, action: #selector(didEndEditing(textField:)), for: .editingDidEnd)
            headerView.nameTF.addTarget(self, action: #selector(didBeginEditing(textField:)), for: .editingDidBegin)
            
            headerView.phoneNumberTF.addTarget(self, action: #selector(didEndEditing(textField:)), for: .editingDidEnd)
            headerView.phoneNumberTF.addTarget(self, action: #selector(didBeginEditing(textField:)), for: .editingDidBegin)
            
            headerView.addProfileImg.addTarget(self, action: #selector(imgVuTapped(sender:)), for: .touchUpInside)
            
            headerView.nameTF.tag = 11
            headerView.phoneNumberTF.tag = 22
            headerView.addressTF.tag = 33
            
            let userNameSelector : Selector = #selector(self.editFieldTapped)
            let userNameViewGesture = UITapGestureRecognizer(target: self, action: userNameSelector)
            let phoneViewGesture = UITapGestureRecognizer(target: self, action: userNameSelector)
            let addressVuGesture = UITapGestureRecognizer(target: self, action: userNameSelector)
            
            //            headerView.userNameVu.isUserInteractionEnabled = true
            //            userNameViewGesture.numberOfTapsRequired = 1
            //            userNameViewGesture.delaysTouchesBegan = true
            
            headerView.userNameVu.addGestureRecognizer(userNameViewGesture)
            headerView.phoneVu.addGestureRecognizer(phoneViewGesture)
            headerView.addressVu.addGestureRecognizer(addressVuGesture)
            
            
            headerView.dobBtn.addTarget(self, action: #selector(showDateVu(_:)), for: .touchUpInside)
            
            let lblSelector : Selector = #selector(self.lblTapped)
            let lblSelectorGesture = UITapGestureRecognizer(target: self, action: lblSelector)
            headerView.bioLbl.isUserInteractionEnabled = true
            
            //            headerView.dobBtn.setTitle(dob, for: .normal)
            lblSelectorGesture.numberOfTapsRequired = 1
            lblSelectorGesture.delaysTouchesBegan = true
            headerView.bioLbl.addGestureRecognizer(lblSelectorGesture)
            
            headerView.logoBtn.addTarget(self, action: #selector(logoImgTapped(_:)), for: .touchUpInside)
            headerView.addLogoBtn.addTarget(self, action: #selector(logoImgTapped(_:)), for: .touchUpInside)
            headerView.removeLogoBtn.addTarget(self, action: #selector(removeLogoTapped(_:)), for: .touchUpInside)
            headerView.removeProfileImg.addTarget(self, action: #selector(removeProfileTapped(_:)), for: .touchUpInside)
            
            headerView.editUsernameBtn.addTarget(self, action: #selector(editUsername(_:)), for: .touchUpInside)
            headerView.editPhoneBtn.addTarget(self, action: #selector(editUsername(_:)), for: .touchUpInside)
            
            headerView.editBioBtn.addTarget(self, action: #selector(editUsername(_:)), for: .touchUpInside)
            
            
            headerView.editAddressBtn.addTarget(self, action: #selector(editUsername(_:)), for: .touchUpInside)
            
            // The the Closure returns Selected Index and String of dropdown
            dropDownproperties(dropDown: headerView.genderDropDown, placeHolder: "Gender", listArray:  ["Male","Female","Undefined"])
            
            headerView.genderDropDown.didSelect{ [self] (selectedText , index ,id) in
                genderChanged = true
                newGender = selectedText
                headerView.genderDropDown.textColor = .black
                headerView.genderDropDown.checkMarkEnabled = true
                headerView.genderDropDown.text = selectedText
            }
            
            if nameChanged {
                headerView.nameTF.text = newName
            } else {
                headerView.nameTF.text = userDetails?.name ?? "Name"
            }
            
            if bioChanged {
                headerView.bioLbl.text = bioText
            } else {
                
                if let text = userDetails?.bio, text == "" || text.isEmpty {
                    headerView.bioLbl.text = "Bio"
                } else {
                    headerView.bioLbl.text = userDetails?.bio
                }
            }
            
            //            if profileChanged {
            //                headerView.profileImg.image = newProfile
            //            } else {
            headerView.profileImg.image = profilePicture
            self.setProfileRemoveButton(image: profilePicture!, header: headerView)
            
            //            }
            headerView.logoBtn.setImage(logoPic, for: .normal)
            self.setLogoRemoveButton(image: logoPic!, header: headerView)

            if genderChanged {
                headerView.genderDropDown.text = newGender
            } else {
                
                if userDetails?.gender == "" {
                    headerView.genderDropDown.text = "Gender"
                    userDetails?.gender = "Gender"
                } else {
                    headerView.genderDropDown.text = userDetails?.gender
                }
                
                print(userDetails?.gender as Any)
                switch userDetails?.gender {
                case "Male":
                    headerView.genderDropDown.selectedIndex = 0
                case "Female":
                    headerView.genderDropDown.selectedIndex = 1
                case "Undefined":
                    headerView.genderDropDown.selectedIndex = 2
                default:
                    headerView.genderDropDown.selectedIndex = nil
                }
            }
            
            if dobChanged {
                headerView.dobBtn.setTitle(newDob, for: .normal)
            } else {
                if let text = userDetails?.dob, text == "" || text.isEmpty {
                    headerView.dobBtn.setTitle("Date of Birth", for: .normal)
                } else {
                    headerView.dobBtn.setTitle(userDetails?.dob, for: .normal)
                }
            }
            
            if phoneChanged {
                headerView.phoneNumberTF.text = newPhone
            } else {
                if let text = userDetails?.phone, text == "" || text.isEmpty {
                    headerView.phoneNumberTF.text = "Phone"
                } else {
                    headerView.phoneNumberTF.text = userDetails?.phone
                }
            }
            
            if addressChanged {
                headerView.addressTF.text = newAddress
            } else {
                if let text = userDetails?.address, text == "" || text.isEmpty {
                    headerView.addressTF.text = "Address"
                } else {
                    headerView.addressTF.text = userDetails?.address
                }
            }
            
            
        default:
            assert(false, "Invalid element type")
        }
        
        return headerView
        
    }
    
}

