
import Foundation
import UIKit
import Foundation
import Firebase

class PushNotificationSender: BaseClass {
    
    var ref: DatabaseReference!
    var userId = ""
    
    //    func sendPushNotificationToTopic(topicName: String, title: String, body: String, projectId: String, isPost: Bool) {
    //        userId = getUserID()
    //        let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")
    //        let postParams: [String : Any] = ["apns": [
    //            "payload": [
    //                "aps": [
    //                    "mutable-content": 1],
    //            ],
    //            "fcm_options": []
    //        ] as AnyObject,"to": "/topics/\(topicName)" as AnyObject,
    //        "data": [ "type":"Notification",
    //                  "projectId": projectId,
    //                  "userId": userId,
    //                  "isPost": isPost
    //                  ] as AnyObject,
    //        "notification":
    //            [ "body": body,
    //              "title": title as String? ?? "New Notification",
    //              "sound" : "default",
    //            ] as AnyObject
    //        ]
    //        //, “badge” : totalBadgeCount
    //
    //        let request = NSMutableURLRequest(url: url! as URL)
    //        request.httpMethod = "POST"
    //        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    //        request.setValue("key=\(Constants.firebaseServerKey)", forHTTPHeaderField: "Authorization")
    //        do{
    //            request.httpBody = try JSONSerialization.data(withJSONObject: postParams, options: JSONSerialization.WritingOptions())
    //            print("My paramaters: \(postParams)")
    //        }
    //        catch{
    //            print("Caught an error: \(error)")
    //        }
    //        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
    //            if let realResponse = response as? HTTPURLResponse{
    //                if realResponse.statusCode == 200 {
    //                    print("Success")
    //                }
    //
    //            }
    //            if let postString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as String?{
    //                print("POST: \(postString)")
    //            }
    //        }
    //        task.resume()
    //
    //        if topicName == Constants.newCustomerPost {
    //            ref = Constants.refs.agentNotif
    //        } else {
    //            ref = Constants.refs.customerNotif
    //        }
    //
    //        guard let key = ref.childByAutoId().key else { return }
    //
    //        ref.child(key).setValue([
    //            "id" : key,
    //            "title" : title,
    //            "body" : body,
    //            "userId": userId,
    //            "projectId": projectId,
    //            "isPost": isPost,
    //            "timestamp": [".sv": "timestamp"]
    //        ]) {
    //            (error:Error?, ref:DatabaseReference) in
    //            if let error = error {
    //                print("Data could not be saved: \(error).")
    //            } else {
    //                print("Data saved successfully!")
    //            }
    //        }
    //
    //    }
    
    func sendPushNotification(fcmToken: String, title: String, body: String) {
        
        userId = getUserId()
        
        let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")
        let postParams: [String : Any] = ["apns": [
            "payload": [ "aps": [
                            "mutable-content": 1]
            ],
            "fcm_options": []
            
        ] as AnyObject,
        "to": fcmToken,
        "data": [   "type":"Notification",
                    "userId" : userId
        ] as AnyObject,
        "notification": ["body": body,
                         "title": title as String? ,
                         "sound" : "default"] as AnyObject]
        //, “badge” : totalBadgeCount
        
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(Constants.firebaseServerKey)", forHTTPHeaderField: "Authorization")
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: postParams, options: JSONSerialization.WritingOptions())
            print("My paramaters: \(postParams)")
        }
        catch{
            print("Caught an error: \(error)")
        }
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if let realResponse = response as? HTTPURLResponse {
                if realResponse.statusCode == 200 {
                    print("Success")
                }
            }
            if let postString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as String? {
                print("POST: \(postString)")
            }
        }
        task.resume()
    }
}

//"to": "\(fcmToken)",
//"data": [   "type":"Notification",
//            "senderId" : receiverId,
//            "isChat": isChat
//] as AnyObject,
//"notification": ["body": body,
//                 "title": title as String? ,
//                 "sound" : "default"] as AnyObject]
