
import UIKit
import LGSideMenuController

class HowToVC: BaseClass {

    @IBOutlet weak var collectionVu: UICollectionView!
    
//    @IBOutlet weak var usePodView: AnimationView!
//    @IBOutlet weak var qrCodeView: AnimationView!
//    @IBOutlet weak var profileView: AnimationView!
    
    var imgs = ["1","2","3","4"]
    var lbls = ["1. Create an account & get your Linkpod ready. You can purchase your Linkpod from www.linkpod.store","2. Start building your Linkpod profile! \nInclude a display picture, contact details and connect your desired social media accounts to your profile.","3. When you are done, you can go ahead and activate your Linkpod!","4. Share your profile instantly with just one tap! Alternatively, you can share your profile link or QR code with the other party. \n\nGet connected today."]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionVu.delegate = self
        collectionVu.dataSource = self
                
        // Do any additional setup after loading the view.
        
    }
}

extension HowToVC: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: (indexPath.row % 2 == 0) ? "TutorialCell0" : "TutorialCell1", for: indexPath) as! TutorialCell
        cell.imgVu.image = UIImage(named: imgs[indexPath.row])
        cell.lbl.text = lbls[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
        switch indexPath.row{
        
        case 0:
            if let url = URL(string: "http://linkpod.store") {
                UIApplication.shared.open(url)
            }
            self.hideLeftView(self)
            break
            
        case 1:
            self.pushController(controller: AddLinksVC.id, storyboard: Storyboards.Main.id)
            break
        
        case 2:
            self.pushController(controller: AddNewTagVC.id, storyboard: Storyboards.Main.id)
          break
            
        case 3:
            self.pushController(controller: MyQrCodeVC.id, storyboard: Storyboards.Main.id)
            break
        default:
            self.toggleLeftViewAnimated(self)
            print("nothing")
        }
    }
}


class TutorialCell : UICollectionViewCell
{
    @IBOutlet weak var imgVu: UIImageView!
    @IBOutlet weak var lbl: UILabel!
}
