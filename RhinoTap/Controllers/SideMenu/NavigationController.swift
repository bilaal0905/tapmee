

import UIKit

class NavigationController: UINavigationController {
    
    override func viewDidLoad() {
        
        //Tabbar
        let addLinksVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//        self.navigationController?.pushViewController(addLinksVC, animated: true)
        
//        let view = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
        self.setViewControllers([(addLinksVC)], animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeRootVC(_:)), name: NSNotification.Name(rawValue: "changeRootVC"), object: nil)
    }
    
    @objc func changeRootVC(_ notification: NSNotification) {
        if let name = notification.userInfo?["VCName"] as? String {
            if let tabbarIndex = notification.userInfo?["tabbarIndex"] as? String, name == "TabBarController" {
                let vc = (storyboard?.instantiateViewController(withIdentifier: name))! as!TabBarController
                vc.initialIndex = Int(tabbarIndex) ?? 0
                self.setViewControllers([vc], animated: false)
            }
            else {
                let vc = (storyboard?.instantiateViewController(withIdentifier: name))!
                self.setViewControllers([vc], animated: false)
            }
        }
    }
}




