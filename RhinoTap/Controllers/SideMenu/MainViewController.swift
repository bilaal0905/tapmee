

import UIKit
import LGSideMenuController

class MainViewController: LGSideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        leftViewWidth = 250.0
        leftViewPresentationStyle = .slideBelow
    }
    override func viewWillAppear(_ animated: Bool) {
        if openProfile {

            guard let window = UIApplication.shared.keyWindow else { return }

            openProfile.toggle()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ViewContactVC") as! ViewContactVC
            vc.uid = profileId
            vc.didTapNotification = true
            window.rootViewController = vc
            window.makeKeyAndVisible()
        }
    }
    deinit {
        print("MainViewController deinitialized")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
