

import UIKit
import NFCReaderWriter
import FirebaseStorage

class AddNewTagVC: BaseClass {
   
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var profileImgVu: UIImageView! {
        didSet {
            profileImgVu.layer.cornerRadius = profileImgVu.bounds.height/2
        }
    }
    
    let storage = Storage.storage()

    let readerWriter = NFCReaderWriter.sharedInstance()
    var userId = ""
    var idInPayload = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userId = getUserId()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        usernameLbl.text = getUsername()
        
        var ref = storage.reference()
        let link = getProfileUrl()
        if link != ""  {
            ref = storage.reference(forURL: link)

            if let cacheImg = imgChache.object(forKey: ref) as? UIImage {
                profileImgVu.image = cacheImg
            } else {
                ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    if error != nil {
                        // Uh-oh, an error occurred!
                    } else {
                        
                        if let img = UIImage(data: data!) {
                            imgChache.setObject(img, forKey: ref)
                            self.profileImgVu.image = img
                            
                        }
                    }
                }
            }
        }

    }
    
    @IBAction func activateBtnTapped(_ sender: Any) {
        readerWriter.newWriterSession(with: self, isLegacy: true, invalidateAfterFirstRead: true, alertMessage: "Ready to scan - hold the linkpod close to your smartphone to set up your linkpod")
        readerWriter.begin()
        self.readerWriter.detectedMessage = "Write data successfully"
        
    }
    @IBAction func readPodPressed(_ sender: Any) {
        
        guard NFCReaderSession.readingAvailable else {
            self.showAlert(title: AlertConstants.Error, message: "Hold your iPhone near to write the data")
            return
        }
        
        /*
         https://betterprogramming.pub/working-with-nfc-tags-in-ios-13-d08c7d183981
         Finally, invalidAfterFirstRead is a boolean flag to — you guessed it — invalid the session after the first tag is read. If you want to scan only one single tag you set it to true and if you want to keep scanning set it to false.
         */
        readerWriter.newWriterSession(with: self, isLegacy: false, invalidateAfterFirstRead: true, alertMessage: "Scan Linkpod to keep connected")
        readerWriter.begin()
        readerWriter.detectedMessage = "detected Tag info"
    }
    
    
    // MARK: - Utilities
    func contentsForMessages(_ messages: [NFCNDEFMessage]) -> String {
        var recordInfos = ""
        
        if let record = messages.first?.records.first {
            if let string = String(data: record.payload, encoding: .ascii) {
                recordInfos = "\(string)"
            }
        }
        //        for message in messages {
        
        //            for (_, record) in message.records.enumerated() {
        //                recordInfos += "Record(\(i + 1)):\n"
        //                recordInfos += "Type name format: \(record.typeNameFormat.rawValue)\n"
        //                recordInfos += "Type: \(record.type as NSData)\n"
        //                recordInfos += "Identifier: \(record.identifier)\n"
        //                recordInfos += "Length: \(message.length)\n"
        
        
        //                recordInfos += "Payload raw data: \(record.payload as NSData)\n\n"
        //            }
        //        }
        
        return recordInfos
    }
    
    func getTagInfos(_ tag: __NFCTag) -> [String: Any] {
        var infos: [String: Any] = [:]

        switch tag.type {
        case .miFare:
            if #available(iOS 13.0, *) {
                if let miFareTag = tag.asNFCMiFareTag() {
                    switch miFareTag.mifareFamily {
                    case .desfire:
                        infos["TagType"] = "MiFare DESFire"
                    case .ultralight:
                        infos["TagType"] = "MiFare Ultralight"
                    case .plus:
                        infos["TagType"] = "MiFare Plus"
                    case .unknown:
                        infos["TagType"] = "MiFare compatible ISO14443 Type A"
                    @unknown default:
                        infos["TagType"] = "MiFare unknown"
                    }
                    if let bytes = miFareTag.historicalBytes {
                        infos["HistoricalBytes"] = bytes.hexadecimal
                    }
                    infos["Identifier"] = miFareTag.identifier.hexadecimal
                }
            } else {
                // Fallback on earlier versions
            }
        case .iso7816Compatible:
            if #available(iOS 13.0, *) {
                if let compatibleTag = tag.asNFCISO7816Tag() {
                    infos["TagType"] = "ISO7816"
                    infos["InitialSelectedAID"] = compatibleTag.initialSelectedAID
                    infos["Identifier"] = compatibleTag.identifier.hexadecimal
                    if let bytes = compatibleTag.historicalBytes {
                        infos["HistoricalBytes"] = bytes.hexadecimal
                    }
                    if let data = compatibleTag.applicationData {
                        infos["ApplicationData"] = data.hexadecimal
                    }
                    infos["OroprietaryApplicationDataCoding"] = compatibleTag.proprietaryApplicationDataCoding
                }
            } else {
                // Fallback on earlier versions
            }
        case .ISO15693:
            if #available(iOS 13.0, *) {
                if let iso15693Tag = tag.asNFCISO15693Tag() {
                    infos["TagType"] = "ISO15693"
                    infos["Identifier"] = iso15693Tag.identifier
                    infos["ICSerialNumber"] = iso15693Tag.icSerialNumber.hexadecimal
                    infos["ICManufacturerCode"] = iso15693Tag.icManufacturerCode
                }
            } else {
                // Fallback on earlier versions
            }
            
        case .feliCa:
            if #available(iOS 13.0, *) {
                if let feliCaTag = tag.asNFCFeliCaTag() {
                    infos["TagType"] = "FeliCa"
                    infos["Identifier"] = feliCaTag.currentIDm
                    infos["SystemCode"] = feliCaTag.currentSystemCode.hexadecimal
                }
            } else {
                // Fallback on earlier versions
            }
        default:
            break
        }
        return infos
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewContact" {
            if let vc = segue.destination as? ViewContactVC {
                
                let fullString = idInPayload
                let stringArr = fullString.components(separatedBy: "/")
                let uid = stringArr[3]
                vc.uid = uid
                vc.tagScanned = true
            }
        }
    }
}

@available(iOS 11.0, *)
extension AddNewTagVC: NFCReaderDelegate {
    func readerDidBecomeActive(_ session: NFCReader) {
        print("Reader did become")
    }
    
    func reader(_ session: NFCReader, didInvalidateWithError error: Error) {
            print("ERROR:\(error)")
            readerWriter.end()
    }
    
    /// -----------------------------
    // MARK: 2. NFC Writer(iOS 13):
    /// -----------------------------
    @available(iOS 13.0, *)
    func reader(_ session: NFCReader, didDetect tags: [NFCNDEFTag]) {
        print("did detect tags")

        let uriPayloadFromURL = NFCNDEFPayload.wellKnownTypeURIPayload(
                url: URL(string: baseUrl+getUserId())!
            
            )!
        
//        var payloadData = Data([0x02])
//        let text = userId
//        payloadData.removeAll(keepingCapacity: false)
//        payloadData.append(text.data(using: .utf8)!)
//
//        let payload = NFCNDEFPayload.init(
//
//            format: NFCTypeNameFormat.nfcWellKnown,
//            type: "U".data(using: .utf8)!,
//            identifier: Data.init(count: 0),
//            payload: payloadData,
//            chunkSize: 0)

        let message = NFCNDEFMessage(records: [uriPayloadFromURL])

        readerWriter.write(message, to: tags.first!) { (error) in
            if let err = error {
                print("ERR:\(err)")
            } else {
                print("write success")
            }
            self.readerWriter.end()
        }
    }
    
    /// --------------------------------
    // MARK: 3. NFC Tag Reader(iOS 13)
    /// --------------------------------
    /*
     The important method for reading data from an NFC tag is readerSession(_:didDetectNDEFs:). The reader session calls this method once a tag with an NDEF message is found.
     */
    func reader(_ session: NFCReader, didDetect tag: __NFCTag, didDetectNDEF message: NFCNDEFMessage) {
        _ = readerWriter.tagIdentifier(with: tag)
        let payload = contentsForMessages([message])
        let tagInfos = getTagInfos(tag)
        var tagInfosDetail = ""
        tagInfos.forEach { (item) in
            tagInfosDetail = tagInfosDetail + "\(item.key): \(item.value)\n"
        }
        
        DispatchQueue.main.async {
            print("Tag Data:",payload)
            guard !payload.isEmpty else {
                self.showAlert(title: AlertConstants.Alert, message: "No data found in Pod.")
                return
            }
            
            self.idInPayload = payload
            self.performSegue(withIdentifier: "viewContact", sender: self)
            
        }
        self.readerWriter.alertMessage = "NFC Tag Info detected"
        self.readerWriter.end()
    }
    
    
    
}

extension Data {
    /// Hexadecimal string representation of `Data` object.
    var hexadecimal: String {
        return map { String(format: "%02x", $0) }
            .joined()
    }
}

