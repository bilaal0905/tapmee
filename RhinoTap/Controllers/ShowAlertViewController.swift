//
//  ShowAlertViewController.swift
//  RhinoTap
//
//  Created by Muhammad Abdullah on 22/02/2022.
//

import UIKit

class ShowAlertViewController: BaseClass {

    @IBOutlet weak var imageVu: UIImageView!
    @IBOutlet weak var statuslbl: UILabel!
    @IBOutlet weak var messagelbl: UILabel!
    
    var status = ""
    var isVerified: Bool = false
    var tagID : String = ""
    var notListed: Bool = false
    var delegate: verifyTag?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //check-mark.png
        //attention.png
        
        //Please visit our official website to get yoours: www.TapMee.com
        
        if notListed == true {
            statuslbl.text = "This TapMee product is not listed in our stock, please contact our customer support.".localize()
            imageVu.image = UIImage(named: "attention")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                // Do whatever you want
                self.dismiss(animated: true) {
                    return
                }
            }
            
        } else {

        messagelbl.isHidden = false
        
        if isVerified == true {
            messagelbl.isHidden = true
            status = "Your TapMee account is now active!".localize()
            imageVu.image = UIImage(named: "check-mark")
            verificationSuccessfull()
        } else {
            status = "This is not official TapMee Product!".localize()
            imageVu.image = UIImage(named: "attention")
        }
         
        statuslbl.text = status
        messagelbl.text = "Please visit our official website to get yours: www.TapMee.it".localize()
        
      //  messagelbl.attributedText = boldenParts(string: "Please visit our official website to get yours: www.TapMee.com", boldCharactersRanges: [[46,63]], regularFont: UIFont(name: "Montserrat-Regular", size: 17.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 17.0))
        }
    }
    
    func verificationSuccessfull() {
        
        //To verify multiple tags
//        var tagsArr = [[String:Any]]()
//
//        if userTags.count == 0 {
//            tagsArr.append(tagUid(id: tagID).asDictionary)
//        } else {
//            for tag in userTags {
//                if tag.id != tagID {
//                    userTags.append(tagUid(id: tagID))
//                }
//            }
//
//            for tag in userTags {
//                    tagsArr.append(tag.asDictionary)
//            }
//        }
 
        
        let dict = [
            "id":self.getUserId(),
            "tagUid":tagID
        ] as [String : Any]
        
        APIManager.updateUserProfile(dict) { [self] (success) in
            self.stopLoading()
            print("assigned uid to user",dict)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                // Do whatever you want
                
                self.dismiss(animated: true) {
                    self.delegate?.tagVerification(true)
                }
            }
        }
    }
    
    func boldenParts(string: String, boldCharactersRanges: [[Int]], regularFont: UIFont?, boldFont: UIFont?) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 17)])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont ?? UIFont.boldSystemFont(ofSize: regularFont?.pointSize ?? UIFont.systemFontSize)]
        for range in boldCharactersRanges {
            let currentRange = NSRange(location: range[0], length: range[1]-range[0]+1)
            attributedString.addAttributes(boldFontAttribute, range: currentRange)
            }
        return attributedString
    }

}
