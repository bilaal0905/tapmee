//
//  AnimatedLaunchScreenVC.swift
//
//  Created by Apple on 31/03/2022.
//

import UIKit

class AnimatedLaunchScreenVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let status = UserDefaults.standard.bool(forKey: Constants.status)
        print(status)
        //imageView.addAnimatedGifwith(name: "LottieAnimation")
        if (status == true) {
//            let addLinksVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//            self.navigationController?.pushViewController(addLinksVC, animated: true)
            self.performSegue(withIdentifier: "homeSegue", sender: self)
        } else {
            self.performSegue(withIdentifier: "signinSegue", sender: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.addBackground()
    }
}

extension UIImageView {
    
    func addAnimatedGifwith(name:String)
    {
        guard let path = Bundle.main.path(forResource: name, ofType: "gif") else {
            print("Gif does not exist at that path")
            return
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
              let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        self.animationImages = images
        self.startAnimating()
    }
}
