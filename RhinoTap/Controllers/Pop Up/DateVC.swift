

import UIKit

class DateVC: UIViewController {
    
    var delegate: getDate?
    var oldDate = ""
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if oldDate != "" && oldDate != "Date of Birth" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd,yyyy"
            let date = dateFormatter.date(from: oldDate)
            let optional = dateFormatter.string(from: Date())
            datePicker.date = date ?? dateFormatter.date(from: optional)!
        }
    }
    
    @IBAction func donePressed(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd,yyyy"
        
        delegate?.getSelectedDate(formatter.string(from: datePicker.date))
        dismiss(animated: true)
        
    }
    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
