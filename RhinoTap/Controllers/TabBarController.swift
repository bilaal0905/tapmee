//
//  TabBarController.swift
//  Tapmee
//
//  Created by Apple on 11/04/2022.
//

import UIKit
import FloatingTabBarController

class TabBarController: FloatingTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let selected = UIImage(named: "user_")!
        let normal = UIImage(named: "user_")!
        let controller1 = storyboard!.instantiateViewController(withIdentifier: "DashBoardVC")
        controller1.title = ""
        controller1.view.backgroundColor = .yellow
        controller1.floatingTabItem = FloatingTabItem(selectedImage: selected, normalImage: normal)
        viewControllers.append(controller1)
        
        let selected2 = UIImage(named: "checked")!
        let normal2 = UIImage(named: "checked")!
        let controller2 = storyboard!.instantiateViewController(withIdentifier: "TagVerificationViewController")
        controller2.title = ""
        controller2.floatingTabItem = FloatingTabItem(selectedImage: selected2, normalImage: normal2)
        viewControllers.append(controller2)
        
        let selected3 = UIImage(named: "qr-code (4)")!
        let normal3 = UIImage(named: "qr-code (4)")!
        let controller3 = storyboard!.instantiateViewController(withIdentifier: "MyQrCodeVC")
        controller3.title = ""
        controller3.floatingTabItem = FloatingTabItem(selectedImage: selected3, normalImage: normal3)
        viewControllers.append(controller3)
        
    }

}
