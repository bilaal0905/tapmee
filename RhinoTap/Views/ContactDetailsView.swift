

import UIKit

class ContactDetailsView: UICollectionReusableView {
    
    @IBOutlet weak var phoneVu: UIView!
    @IBOutlet weak var emailVu: UIView!
    
    @IBOutlet weak var dismissBtn: UIButton!
    @IBOutlet weak var profileImg: UIImageView! {
        didSet {
            profileImg.layer.cornerRadius = profileImg.bounds.height/2
        }
    }
    @IBOutlet weak var logoImg: UIImageView! {
        didSet {
            logoImg.layer.cornerRadius = logoImg.bounds.height/2
        }
    }
    
    @IBOutlet weak var addToContactsBtn: UIButton!
    
    @IBOutlet weak var connectBackBtn: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var outerVu: UIView!
    {
        didSet {
            outerVu.layer.cornerRadius = 25
            layer.backgroundColor = UIColor.clear.cgColor
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOffset = CGSize(width: 2.0, height: 0.0)
            layer.shadowOpacity = 0.2
            layer.shadowRadius = 2.0
            
            outerVu.layer.masksToBounds = true
            addSubview(outerVu)
            outerVu.translatesAutoresizingMaskIntoConstraints = false
            
            // pin the containerView to the edges to the view
            outerVu.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            outerVu.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            outerVu.topAnchor.constraint(equalTo: topAnchor).isActive = true
            outerVu.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
    }
}
