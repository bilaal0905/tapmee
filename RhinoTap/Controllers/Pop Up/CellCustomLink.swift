//
//  CellCustomLink.swift
//
//  Created by MuhammadAnsaf on 22/03/2022.
//

import UIKit

class CellCustomLink: UICollectionViewCell {
    @IBOutlet weak var outerVu: UIView!
    @IBOutlet weak var imgVu: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var checkMark: UIImageView!
}
