
import UIKit
import FirebaseAuth

class ForgotPwVC: BaseClass {
    
    @IBOutlet weak var emailTF: UITextField!{
        didSet {
            let blackPlaceholderText = NSAttributedString(string: "Email",
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            emailTF.attributedPlaceholder = blackPlaceholderText
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: emailTF.frame.height))
            emailTF.leftView = paddingView
            emailTF.leftViewMode = .always
        }
    }
    var link: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let link = UserDefaults.standard.value(forKey: "Link") as? String{
            self.link = link
            //sendBtn.isEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.addBackground()
    }
    
    @IBAction func sendBtnPressed(_ sender: Any) {
        guard let email = emailTF.text, !email.isEmpty else {
            showAlert(title: AlertConstants.Alert, message: AlertConstants.EmailEmpty)
            return
        }
        startLoading()
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if error == nil {
                self.showAlert(title: AlertConstants.Alert, message: "Check your email to reset password.") {
                    self.navigationController?.popViewController(animated: true)
                }
                //                pushController(controller: PasscodeVerificationVC.id, storyboard: Storyboards.Main.id)
            } else {
                self.stopLoading()
                self.showAlert(title: AlertConstants.Error, message: error?.localizedDescription ?? AlertConstants.SomeThingWrong)
            }
        }
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
    
    func didTapSendSignInLink(){
        if let email = self.emailTF.text {
            let actionCodeSettings = ActionCodeSettings.init()
            actionCodeSettings.handleCodeInApp = true
            actionCodeSettings.url = URL(string: String(format:"https://myapp-4e836.firebaseapp.com/?email=%@",email))
            actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
            
            Auth.auth().sendSignInLink(toEmail:email,actionCodeSettings: actionCodeSettings) { error in
                // ...
                if let error = error {
                    print("error in send link :",error)
                    //self.showMessagePrompt(error.localizedDescription)
                    return
                }
                // The link was successfully sent. Inform the user.
                // Save the email locally so you don't need to ask the user for it again
                // if they open the link on the same device.
                UserDefaults.standard.set(email, forKey: "Email")
                //self.showMessagePrompt("Check your email for link")
                // ...
            }
            
        }
    }
    
    func didTapSignInWithEmailLink() {
        Auth.auth().currentUser?.reload(completion: { (error) in
            if error == nil{
                if let email = self.emailTF.text {
                    Auth.auth().signIn(withEmail: email, link: self.link) { (user, error) in
                        if let error = error {
                            print("\(error.localizedDescription)")
                            return
                        }
                    }
                } else {
                    print("Email can't be empty")
                }
                
                print(Auth.auth().currentUser?.email as Any)
                if Auth.auth().currentUser?.isEmailVerified == true {
                    print("User verified")
                } else {
                    print("User not verified")
                }
            } else {
                print(error?.localizedDescription as Any)
            }
        })
    }
    
}
