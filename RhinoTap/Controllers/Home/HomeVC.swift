

import UIKit
import GoogleSignIn
import Firebase
import FBSDKLoginKit
import AuthenticationServices
import CryptoKit
import FirebaseAuth
import Localize

class HomeVC: BaseClass, ASAuthorizationControllerPresentationContextProviding {
    
    @IBOutlet weak var signBtn: UIButton!
    @IBOutlet weak var loginInWithAppleVu: UIView!
    @IBOutlet weak var signInGooglevu: UIView!
    @IBOutlet weak var signFacebookVu: UIView!
    
    @IBOutlet weak var shareYourLbl: UILabel!
    @IBOutlet weak var youWontLbl: UILabel!
    //==========================================
    
    var email = ""
    var name = ""
    var uid = ""
    let appleSignIn = HSAppleSignIn()
    let storage = Storage.storage()

    
    //==========================================
    
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0,weight: .medium),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    var attributedString = NSMutableAttributedString(string:"")
    
    override func viewDidLoad() {
        super.viewDidLoad()        

        let buttonTitleStr = NSMutableAttributedString(string:"Sign In".localize(), attributes:attrs)
        
        if #available(iOS 13, *) {
            let appleTap = UITapGestureRecognizer(target: self, action: #selector(self.loginWithApplePressed(_:)))
            loginInWithAppleVu.addGestureRecognizer(appleTap)
        } else {
            loginInWithAppleVu.isHidden = true
        }
        
        attributedString.append(buttonTitleStr)
        signBtn.setAttributedTitle(attributedString, for: .normal)
        
        let fbTap = UITapGestureRecognizer(target: self, action: #selector(self.loginWithFacebookPressed(_:)))
        signFacebookVu.addGestureRecognizer(fbTap)
        
        let googleTap = UITapGestureRecognizer(target: self, action: #selector(self.loginWithGooglePressed(_:)))
        signInGooglevu.addGestureRecognizer(googleTap)
                    
        shareYourLbl.attributedText = boldenParts(string: "Share your Social Media Profiles, Websites & Custom Links with Only One Tap!".localize(), boldCharactersRanges: [[0,73]], regularFont: UIFont(name: "Montserrat-Regular", size: 17.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 17.0))
        
        youWontLbl.attributedText = boldenParts(string: "You won't need a Business card anymore, This is the Future Of Networking!".localize(), boldCharactersRanges: [[0,61]], regularFont: UIFont(name: "Montserrat-Regular", size: 17.0), boldFont: UIFont(name: "Montserrat-SemiBold", size: 17.0))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.view.addBackground()
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    func boldenParts(string: String, boldCharactersRanges: [[Int]], regularFont: UIFont?, boldFont: UIFont?) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 17)])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont ?? UIFont.boldSystemFont(ofSize: regularFont?.pointSize ?? UIFont.systemFontSize)]
        for range in boldCharactersRanges {
            let currentRange = NSRange(location: range[0], length: range[1]-range[0]+1)
            attributedString.addAttributes(boldFontAttribute, range: currentRange)
            }
        return attributedString
    }

    @available(iOS 13, *)
    @objc func loginWithApplePressed(_ sender: UITapGestureRecognizer?) {
        startSignInWithAppleFlow()
    }

    @objc func loginWithFacebookPressed(_ sender: UITapGestureRecognizer?) {
        
        startLoading()
        let manager = LoginManager()
        
        manager.logIn(permissions: [.publicProfile,.email], viewController: self) { [self] (result) in
            switch result {
            case .success(_,_,_):
                let credential = FacebookAuthProvider
                    .credential(withAccessToken: AccessToken.current!.tokenString)
                
                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, relationship_status, picture.type(large)"]) .start { (connection, result, error) in
                    if (error == nil)
                    {
                        
                        
                        let fbDetails = result as! NSDictionary
                        print("fbDetails",fbDetails)
                        self.email = fbDetails["email"] as! String
                        self.name = fbDetails["name"] as! String
                        self.uid = fbDetails["id"] as! String
//
                        
                        Auth.auth().signIn(with: credential) { authResult, error in
                            
                            if let error = error {
                                self.stopLoading()
                                self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                                return
                            }
                            
                            guard let uid = authResult?.user.uid else {return}
                            
                            APIManager.getUserData(id: uid) { (userDetails) in
                                if userDetails != nil {
                                    // User data already found on database
                                    
                                    //TODO: apply check on user status
                                    if userDetails?.isDeleted == true {
                                        self.stopLoading()
                                        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.accountLockedAlert)
                                    } else {
                                        self.saveUserToDefaults(userDetails!)
                                        self.stopLoading()
                                        print("Saved Data to user defaults",userDetails as Any)
                                        self.performSegue(withIdentifier: "Main", sender: self)

                                    }
                                    
                                } else { // User data is not found on database†
                                    
                                    let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = sb.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                                    
                                    vc.uid = uid
                                    vc.email = self.email
                                    vc.name = self.name
                                    
                                    if let picture = fbDetails["picture"] as? [String:Any] ,
                                       let imgData = picture["data"] as? [String:Any] ,
                                       let imgUrl = imgData["url"] as? String {
                                        print(imgUrl)
                                        vc.profileImgUrl = "\(imgUrl)"
                                    }
                                    
                                    self.stopLoading()
                                    vc.isSocialLogin = true
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }

                            } error: { (desc) in
                                self.stopLoading()
                                self.showAlert(title: AlertConstants.Error, message: desc)
                            }
                        }
                        
                    } else {
                        showAlert(title: AlertConstants.Alert, message: error?.localizedDescription ?? AlertConstants.SomeThingWrong)
                        print("error occured.",error?.localizedDescription as Any)
                    }
                }
                
                break
                
            case .cancelled:
                stopLoading()
                showAlert(title: AlertConstants.Alert, message: AlertConstants.userCancelLoginAlert)
                print("user cancelled the login Flow")
                break
            case .failed(let error):
                stopLoading()
                self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                print("Error occured \(error.localizedDescription)")
            }
        }
        
    }
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    // Unhashed nonce.
    fileprivate var currentNonce: String?
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    @objc func loginWithGooglePressed(_ sender: UITapGestureRecognizer? = nil) {
        
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        
        // Create Google Sign In configuration object.
        let config = GIDConfiguration(clientID: clientID)
        startLoading()
        // Start the sign in flow!
        GIDSignIn.sharedInstance.signIn(with: config, presenting: self) { user, error in
            
            if let error = error {
                self.stopLoading()
                self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                return
            }
            guard let authentication = user?.authentication,
                  let idToken = authentication.idToken
            else {
                self.stopLoading()
                return
            }
            
            let credential = GoogleAuthProvider.credential(withIDToken: idToken,
                                                           accessToken: authentication.accessToken)
            self.autheticateWithFirebaseGoogle(credential: credential,user: user!)
        }
    }
    
    func autheticateWithFirebaseGoogle(credential: AuthCredential,user: GIDGoogleUser) {
        
        Auth.auth().signIn(with: credential) { authResult, error in
            
            if let error = error {
                self.stopLoading()
                self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                return
            }
            
            guard let uid = authResult?.user.uid else {return}
            
            APIManager.getUserData(id: uid) { (userDetails) in
                
                if userDetails != nil { // User data already found on database
                    if userDetails?.isDeleted == true {
                        self.stopLoading()
                        self.showAlert(title: AlertConstants.Alert, message: AlertConstants.accountLockedAlert)
                    } else {
                        
                        self.saveUserToDefaults(userDetails!)
                        self.stopLoading()

                        print("Saved Data to user defaults",user as Any)
                        
                        self.performSegue(withIdentifier: "Main", sender: self)

                    }
                    
                } else { // User data is not found on database
                    
                    let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                    vc.uid = uid
                    vc.email = user.profile?.email ?? ""
                    vc.name = user.profile?.name ?? ""
                    
                    if let url = user.profile?.imageURL(withDimension: 150) {
                        vc.profileImgUrl = "\(url)"
                    }
                    
                    self.stopLoading()
                    vc.isSocialLogin = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            } error: { (desc) in
                self.stopLoading()
                self.showAlert(title: AlertConstants.Error, message: desc)
            }
        }
    }

    func authenticateWithFirebaseFacebook() {
        
        
    }
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        pushController(controller: SignUpVC.id, storyboard: "Main")
    }

    @IBAction func SignInPressed(_ sender: Any) {
        pushController(controller: SignInVC.id, storyboard: "Main")
    }
    
    @IBAction func BuyYourTagPressed(_ sender: Any) {
      //  pushController(controller: TagVerificationViewController.id, storyboard: "Main")
    }
    
    override func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
}

@available(iOS 13.0, *)
extension HomeVC: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            startLoading()
            guard let nonce = currentNonce else {
                self.stopLoading()

                fatalError("Invalid state: A login callback was received, but no login request was sent.")
                
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                self.stopLoading()
                showAlert(title: AlertConstants.Error, message: "Unable to fetch identity token".localize())
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                self.stopLoading()
                showAlert(title: AlertConstants.Error, message: "Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            
            Auth.auth().signIn(with: credential) { authResult, error in
                
                if let error = error {
                    self.stopLoading()
                    self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                    return
                }
                
                guard let uid = authResult?.user.uid else {return}
                
                APIManager.getUserData(id: uid) { [self] (userDetails) in
                    if userDetails != nil {
                        
                        if userDetails?.isDeleted == true {
                            self.stopLoading()
                            self.showAlert(title: AlertConstants.Alert, message: AlertConstants.accountLockedAlert)
                        } else {
                            self.saveUserToDefaults(userDetails!)
                            self.stopLoading()
                            print("Data saved successfully!")
                            
                            self.performSegue(withIdentifier: "Main", sender: self)
                            
                        /*    if userDetails?.tagUid != "" && userDetails?.tagUid != nil {
                                self.performSegue(withIdentifier: "Main", sender: self)
                            } else {
                                let controller = self.getControllerRef(controller: TagVerificationViewController.id, storyboard: Storyboards.Main.id) as! TagVerificationViewController
                                self.navigationController?.pushViewController(controller, animated: true)
                            } */
                        }
                        
                    } else { // User data is not found on database
                        
                     /*   let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = sb.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                        
                        vc.uid = uid
                        vc.isFromAppleLogin = true
                        vc.isSocialLogin = true
                        vc.email = appleIDCredential.email ?? ""
                        vc.name = (appleIDCredential.fullName?.givenName ?? "") + " " + (appleIDCredential.fullName?.familyName ?? "")
                        
                        self.stopLoading()
                        self.navigationController?.pushViewController(vc, animated: true)
                        */
                        
                        var param : [String:Any] = [:]
                        
                        let user = User(id: uid,
                                        name:(appleIDCredential.fullName?.givenName ?? "") + " " + (appleIDCredential.fullName?.familyName ?? ""),
                                        email: appleIDCredential.email ?? "",
                                        fcmToken: self.getFCM(),
                                        profileUrl: "",
                                        bio: "",
                                        username: name,
                                        phone: "",
                                        gender: "",
                                        dob: "",
                                        address: "",
                                        coverUrl:"",
                                        profileOn: 1,
                                        isDeleted: false,
                                        platform: "ios")
                        
                        
                        param.removeAll()
                        param = user.asDictionary
                        
                        UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey: Constants.customer)
                        UserDefaults.standard.synchronize()
                        
                        Constants.refs.databaseUser.child(uid).setValue(param) {
                            (error:Error?, ref:DatabaseReference) in
                            self.stopLoading()
                            if let error = error {
                                self.showAlert(title: AlertConstants.Error, message: error.localizedDescription)
                                return
                            } else {
                                print("Data saved successfully!")
                                UserDefaults.standard.setValue(true, forKey: Constants.status)
                                
                                self.performSegue(withIdentifier: "Main", sender: self)
                                
                            }
                        }
                    }
                    
                } error: { (desc) in
                    self.stopLoading()
                    self.showAlert(title: AlertConstants.Error, message: desc)
                }
            }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
        
    }
    
}


